//
//  AYSUITests.swift
//  AYSUITests
//
//  Created by Max Ward on 15/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import XCTest

class AYSUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    override func tearDown() {}

//    func testLoginSuccess(){
//        
//        let app = XCUIApplication()
//        let txtUsername = "testapp@asyoustay.com"
//        let txtPassword  = "testmeup!"
//        
//        let userTextField = app.textFields["User name"]
//        XCTAssertTrue(userTextField.exists)
//        userTextField.tap()
//        userTextField.typeText(txtUsername)
//        
//        //let passwordTextField = app.secureTextFields["Password"]
//        let passwordTextField = app.textFields["Password"]
//        XCTAssertTrue(passwordTextField.exists)
//        passwordTextField.tap()
//        passwordTextField.typeText(txtPassword)
//        
//        let loginButton = app.buttons["Sign in"]
//        loginButton.tap()
//        
//        let alert = app.alerts.firstMatch
//        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: alert, handler: nil)
//        waitForExpectations(timeout: 30, handler: nil)
//        alert.buttons["OK"].tap()
//    }
//    
//    func testLoginBadMail(){
//        let app = XCUIApplication()
//        let txtUsername = "esto no es un mail"
//        let txtPassword  = "testmeup!"
//        
//        let userTextField = app.textFields["User name"]
//        XCTAssertTrue(userTextField.exists)
//        userTextField.tap()
//        userTextField.typeText(txtUsername)
//        
//        let passwordTextField = app.textFields["Password"]
//        XCTAssertTrue(passwordTextField.exists)
//        passwordTextField.tap()
//        passwordTextField.typeText(txtPassword)
//        
//        let loginButton = app.buttons["Sign in"]
//        loginButton.tap()
//        
//        let alert = app.alerts.firstMatch
//        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: alert, handler: nil)
//        waitForExpectations(timeout: 20, handler: nil)
//        alert.buttons["OK"].tap()
//    }
//    func testLoginBadCredentials() {
//        let app = XCUIApplication()
//        let txtUsername = "no_es_un_usuario@zibra.co"
//        let txtPassword  = "y esto no es el password"
//        
//        let userTextField = app.textFields["User name"]
//        XCTAssertTrue(userTextField.exists)
//        userTextField.tap()
//        userTextField.typeText(txtUsername)
//        
//        let passwordTextField = app.textFields["Password"]
//        XCTAssertTrue(passwordTextField.exists)
//        passwordTextField.tap()
//        passwordTextField.typeText(txtPassword)
//        
//        let loginButton = app.buttons["Sign in"]
//        loginButton.tap()
//        
//        let alert = app.alerts.firstMatch
//        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: alert, handler: nil)
//        waitForExpectations(timeout: 20, handler: nil)
//        alert.buttons["OK"].tap()
//    }
//    
//    func testLoginEmptyPassword () {
//        //
//        let app = XCUIApplication()
//        let txtUsername = "no_es_un_usuario@zibra.co"
//        
//        let userTextField = app.textFields["User name"]
//        XCTAssertTrue(userTextField.exists)
//        userTextField.tap()
//        userTextField.typeText(txtUsername)
//        
//        let passwordTextField = app.textFields["Password"]
//        XCTAssertTrue(passwordTextField.exists)
//        passwordTextField.tap()
//        
//        let loginButton = app.buttons["Sign in"]
//        loginButton.tap()
//        
//        let alert = app.alerts.firstMatch
//        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: alert, handler: nil)
//        waitForExpectations(timeout: 20, handler: nil)
//        alert.buttons["OK"].tap()
//    }
}
