//
//  BasicAnimator.swift
//  As You Stay
//
//  Created by Max Ward on 7/15/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit

class BasicAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    //let animationDuration: Double = 0.0
    var isPresenting: Bool = true
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.8//TimeInterval(exactly: animationDuration) ?? 0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toViewController = transitionContext.viewController(forKey: .to), let fromViewController = transitionContext.viewController(forKey: .from) else {
            transitionContext.completeTransition(false)
            return
        }
        if isPresenting {
            transitionContext.containerView.addSubview(toViewController.view)
            presentAnimation(with: transitionContext, viewToAnimate: toViewController.view)
        } else {
            transitionContext.containerView.addSubview(fromViewController.view)
            dismissAnimation(with: transitionContext, viewToAnimate: fromViewController.view)
        }
    }
    
    func presentAnimation(with transitionContext: UIViewControllerContextTransitioning, viewToAnimate: UIView){
        viewToAnimate.clipsToBounds = true
        viewToAnimate.alpha = 0
        UIView.animate(withDuration: 0.8, animations: {
            viewToAnimate.alpha = 1
        }, completion: { _ in
            transitionContext.completeTransition(true)
        })
    }
    func dismissAnimation(with transitionContext: UIViewControllerContextTransitioning, viewToAnimate: UIView){
        viewToAnimate.clipsToBounds = true
        UIView.animate(withDuration: 0.8, animations: {
            viewToAnimate.alpha = 0
        }, completion: { _ in
            transitionContext.completeTransition(true)
        })
    }
}

extension BasicAnimator: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BasicAnimator()
    }
    
    func animationController(forDismissed dismissed: UIViewController)-> UIViewControllerAnimatedTransitioning? {
        let ba = BasicAnimator()
        ba.isPresenting = false
        return ba
    }
}
