//
//  GMSMarkerExtension.swift
//  As You Stay
//
//  Created by Max Ward on 12/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import GoogleMaps

extension GMSMarker{

    fileprivate struct AssociatedKeys{
        static var hotel : HotelResponse?
        static var index : Int?
    }
    
    var hotel: HotelResponse? {
        get { return objc_getAssociatedObject(self, &AssociatedKeys.hotel) as? HotelResponse }
        set (newValue){
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.hotel,
                    newValue as HotelResponse?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    var index: Int?{
        get { return objc_getAssociatedObject(self, &AssociatedKeys.index) as? Int }
        set (newValue){
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.index,
                    newValue as Int?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
}
