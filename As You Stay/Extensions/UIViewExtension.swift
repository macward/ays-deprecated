//
//  UIViewExtension.swift
//  As You Stay
//
//  Created by Max Ward on 21/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    class func initFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)?[0] as! T
    }
}
