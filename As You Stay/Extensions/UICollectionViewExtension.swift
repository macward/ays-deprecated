//
//  UICollectionViewExtension.swift
//  As You Stay
//
//  Created by Max Ward on 11/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import AssistantKit
import AudioToolbox

extension UICollectionView {

    func scrollToNearestVisibleCollectionViewCell(completion: ((IndexPath) -> Void)? = nil) {
        self.decelerationRate = UIScrollView.DecelerationRate.fast
        let visibleCenterPositionOfScrollView = Float(self.contentOffset.x + (self.bounds.size.width / 2))
        var closestCellIndex = -1
        var closestDistance: Float = .greatestFiniteMagnitude
        for i in 0..<self.visibleCells.count {
            let cell = self.visibleCells[i]
            let cellWidth = cell.bounds.size.width
            let cellCenter = Float(cell.frame.origin.x + cellWidth / 2)

            // Now calculate closest cell
            let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
            if distance < closestDistance {
                closestDistance = distance
                closestCellIndex = self.indexPath(for: cell)!.row
            }
        }
        if closestCellIndex != -1 {
            self.scrollToItem(at: IndexPath(row: closestCellIndex, section: 0), at: .centeredHorizontally, animated: true)
////            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            let generator = UIImpactFeedbackGenerator(style: .light)
            completion?(IndexPath(row: closestCellIndex, section: 0))
            //generator.impactOccurred()
//
//            let pop = SystemSoundID(1520)
//            AudioServicesPlaySystemSound(pop)
//
//            let peek = SystemSoundID(1519)
//            AudioServicesPlaySystemSound(peek)
        }
    }
}
