//
//  UITextFieldExtension.swift
//  As You Stay
//
//  Created by Max Ward on 7/15/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit
extension UITextField {

    private func underlined() {
    }
    
    private func setIcon(image: UIImage) {
        let containerWidth = 42
        
        let iconContainerView: UIView = UIView(frame:CGRect(x: 0, y: 0, width: containerWidth, height: containerWidth))
        
        iconContainerView.backgroundColor = UIColor(hexString: "F7F8FA")
        iconContainerView.layer.cornerRadius = iconContainerView.bounds.width / 2
        
        let iconView = UIImageView(frame:CGRect(x: (iconContainerView.frame.maxX / 2) - 10, y: (iconContainerView.frame.maxY / 2) - 10, width: 20, height: 20))
        iconView.contentMode = .scaleAspectFit
        iconView.image = image
        
        iconContainerView.addSubview(iconView)
        
        leftView = iconContainerView
        leftViewMode = .always
    }
    
    /**
     Setting the custom text field
     
     - Parameter border: 0 without border, 1 bottom border, default roundedRect
     - Parameter image: icon (width: 40, height: 40)
     - Parameter placeholder: quotes
     - Version: 1
     - Author: Zibra
     */
    func setCustomTextField( border: Int, image: UIImage?, placeholder: String? ){
        switch border {
        case 0:
             borderStyle = .none
        case 1:
            underlined()
        default:
            borderStyle = .roundedRect
        }
        if let img = image {
            setIcon(image: img)
        }
        if let text = placeholder {
            self.placeholder = text
        }
    }
}
