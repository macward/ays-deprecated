//
//  UINavigationController.swift
//  As You Stay
//
//  Created by Max Ward on 30/10/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func clearBackground () {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
}
