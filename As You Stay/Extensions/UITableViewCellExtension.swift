//
//  UITableViewCellExtension.swift
//  As You Stay
//
//  Created by Max Ward on 20/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    func setDisclosure(toColour: UIColor) -> () {
        for view in self.subviews {
            if let disclosure = view as? UIButton {
                if let image = disclosure.backgroundImage(for: .normal) {
                    let colouredImage = image.withRenderingMode(.alwaysTemplate)
                    disclosure.setImage(colouredImage, for: .normal)
                    disclosure.tintColor = toColour
                }
            }
        }
    }
}
