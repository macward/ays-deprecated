//
//  AppExtensions.swift
//  As You Stay
//
//  Created by Max Ward on 25/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

extension UIView {
    func addShadow() {
        self.clipsToBounds = false
        self.layer.shadowOffset = CGSize(width: 2, height: 2);
        self.layer.shadowRadius = 4;
        self.layer.shadowOpacity = 0.5;
    }
    /**
     Change one or more corners specifically
     
     - Parameter corners: [.topLeft, .topRight, .bottomLeft, .bottomRight]
     - Parameter radius: example 10.0
     */
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
extension UISearchBar {
    /**
     Change the text field background color, if set searchBarStyle = .minimal dont work
     */
    func setTextFieldBackgroundColor(color: UIColor) {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.backgroundColor = color
    }
    /**
     Change the text field text color, fontSize and if it's bold or not
     */
    func setTextFieldText(color: UIColor, fontSize: CGFloat, bold: Bool) {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.textColor = color
        if bold {
            tf.font = UIFont.Text.big()//UIFont.boldSystemFont(ofSize: fontSize)
        } else {
             tf.font = UIFont.Text.big()//UIFont.systemFont(ofSize: fontSize)
        }
        
    }
    
    /**
    Change the icon color
    */
    func setMagnifyingGlassColorTo(color: UIColor)
    {
        // Search Icon
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = color
    }
    
    func removeTextFieldBorder() {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.borderStyle = .none
    }
    
    func setIcon(image: UIImage) {
        let containerWidth = 42
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        
        let iconContainerView: UIView = UIView(frame:CGRect(x: 0, y: 0, width: containerWidth, height: containerWidth))
        let iconView = UIImageView(frame:CGRect(x: (iconContainerView.frame.maxX / 2) - 10, y: (iconContainerView.frame.maxY / 2) - 10, width: 20, height: 20))
        iconView.contentMode = .scaleAspectFit
        iconView.image = image
        
        iconContainerView.addSubview(iconView)
        
        tf.leftView = iconContainerView
        tf.leftViewMode = .always
    }
}
extension Double {
    /**
     take zero from the decimals
     */
    var noZero: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
}

enum AppFontName: String{
    case regular = "Montserrat-Regular"
    case bold = "Montserrat-Bold"
}

extension UIFont {
    
    struct App {
        static func regular( size:CGFloat ) -> UIFont{
            return  UIFont(name: AppFontName.regular.rawValue, size: size)!
        }
        
        static func bold( size:CGFloat ) -> UIFont {
            return  UIFont(name: AppFontName.bold.rawValue, size: size)!
        }
    }
    
    struct Header {
        static func h1() -> UIFont {
            return  UIFont(name: AppFontName.bold.rawValue, size: 20)!
        }
        
        static func h2() -> UIFont {
            return  UIFont(name: AppFontName.bold.rawValue, size: 16)!
        }
        
        static func h3() -> UIFont {
            return  UIFont(name: AppFontName.regular.rawValue, size: 14)!
        }
    }
    
    struct Text {
        
        static func small() -> UIFont {
            return  UIFont(name: AppFontName.regular.rawValue, size: 14)!
        }
        
        static func normal() -> UIFont {
            return  UIFont(name: AppFontName.regular.rawValue, size: 16)!
        }
        
        static func big() -> UIFont {
            return  UIFont(name: AppFontName.regular.rawValue, size: 17)!
        }
    }
}
extension Date {
    
    func addingDaysToDate(days: Int, currentDate: Date)-> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: currentDate)!
    }
    func getMapFormatDate () -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "eee, MMM dd - hh a"
        let fd = formatter.string(from: self)
        return "\(fd)"
    }
    
    func getFormatHours(hour: Int) -> String {
        var formatedHour: Int = 0
        var indicator: String = "AM"
        if hour < 12 {
            if hour == 0 {
                formatedHour = 12
            }
            formatedHour = hour
        } else {
            if hour == 12 {
                formatedHour = hour
            } else {
                formatedHour = hour - 12
            }
            indicator = "PM"
        }
        return ", \(formatedHour)" + indicator
    }
    
    func combineDateWithHours(hours: Int) -> Date? {
        let calendar = Calendar.current
        var combineDate = DateComponents()
        combineDate.year = self.year
        combineDate.month = self.month
        combineDate.day = self.day
        combineDate.hour = hours
        return calendar.date(from: combineDate)
    }
    
    func getCalendarFormatDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd"
        let fd = formatter.string(from: self)
        return "\(fd)".uppercased()
    }
    
    func getFormatToRequest() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: self)
    }
    
}
