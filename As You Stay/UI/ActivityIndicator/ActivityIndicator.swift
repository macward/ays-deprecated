//
//  ActivityIndicator.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import Lottie

class ActivityIndicator: UIView {
    
    var animationView = AnimationView(name: "AsYouStayIcon")
    weak var view: UIView?
    var backgroundView = UIView()
    
    init(view: UIView) {
        super.init(frame: view.bounds)
        
        animationView.frame = CGRect(x: 0, y: 0, width: 75, height: 75)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.isHidden = true
    
        self.view = view
        animationView.center = view.center
        
        backgroundView.frame = view.bounds
        backgroundView.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.137254902, blue: 0.1647058824, alpha: 0.5)
        backgroundView.isHidden = true
        view.addSubview(backgroundView)
        view.addSubview(animationView)
        view.bringSubviewToFront(backgroundView)
        view.bringSubviewToFront(animationView)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startAnimating() {
        self.view?.bringSubviewToFront(backgroundView)
        self.view?.bringSubviewToFront(animationView)
        animationView.isHidden = false
        backgroundView.isHidden = false
        animationView.play()
    }
    
    func stopAnimating() {
        animationView.isHidden = true
        backgroundView.isHidden = true
        animationView.stop()
    }
    
    func isActive() -> Bool {
        return !animationView.isHidden
    }
}
