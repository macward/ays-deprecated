//
//  CreditCardsTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 07/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class CreditCardsTableViewCell: UITableViewCell {

    @IBOutlet weak var holderLabel: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var last4Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configure(with creditCard: CreditCardObject) {
        configurePrimaryCardLabel(isPrimary: creditCard.isPrimary)
        let viewModel = CreditCardViewModel()
        
        holderLabel.text = creditCard.label
        last4Label.text = creditCard.last4
        cardImage.image = viewModel.setImage(with: creditCard.type)
    }
    
    func configurePrimaryCardLabel( isPrimary: Bool) {
        if isPrimary {
            holderLabel.textColor = UIColor(red: 12/255, green: 194/255, blue: 235/255, alpha: 1)
            holderLabel.font = .boldSystemFont(ofSize: 20)
        } else {
            holderLabel.textColor = .white
            holderLabel.font = .systemFont(ofSize: 17)
        }
    }
    
    
}
