//
//  HotelTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 25/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class HotelTableViewCell: UITableViewCell {

    @IBOutlet weak var hotelImg: UIView!
    @IBOutlet weak var hotelNameLbl: UIView!
    @IBOutlet weak var hotelDistLbl: UILabel!
    @IBOutlet weak var dealView: UIView!
    @IBOutlet weak var currPriceLbl: UILabel!
    @IBOutlet weak var oldPriceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
