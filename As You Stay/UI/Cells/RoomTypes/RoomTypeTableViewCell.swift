//
//  RoomTypeTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 20/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class RoomTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var avgNightLabel: UILabel!
    @IBOutlet weak var memberSaving: UILabel!
    @IBOutlet weak var roomTypeLabel: UILabel!
    @IBOutlet weak var refundableLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with roomType: PricingV3RoomType) {
        
        avgNightLabel.text = "$\(roomType.avgNightPrice)"
        memberSaving.text = "-$\(roomType.avgMemberSavings)"
        roomTypeLabel.text = roomType.roomType
        if roomType.refundable {
            refundableLabel.text = "REFUNDABLE"
        } else {
            refundableLabel.text = "NO-REFUNDABLE"
            
        }
        
        self.setDisclosure(toColour: #colorLiteral(red: 0.04705882353, green: 0.7607843137, blue: 0.9215686275, alpha: 1))
    }
}
