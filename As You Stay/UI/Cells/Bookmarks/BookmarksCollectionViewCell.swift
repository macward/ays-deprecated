//
//  BookmarksCollectionViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 13/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class BookmarksCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var hotelLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    var hotelID: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 5.0
        addShadow()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.hotelLabel.text = ""
        self.addressLabel.text = ""
        self.hotelID = ""
        self.image.image = nil
        self.image.layer.sublayers?.removeAll()
    }
    
    func configure(with hotel: HotelObject) {
        //if hotel.availability {
        hotelLabel.text = hotel.name
        hotelLabel.numberOfLines = 2
        hotelLabel.lineBreakMode = .byWordWrapping
        addressLabel.text = hotel.full_address
        addressLabel.sizeToFit()
        image.contentMode = .scaleToFill
        hotelID = hotel.id
        
        let hotelService = HotelService()
        hotelService.getImageHotel(url: hotel.photo) { (photo) in
            self.image.image = photo
            self.image.cornerRadius = 5.0
            self.image.contentMode = .scaleAspectFill
            
            let gradient = CAGradientLayer()
            gradient.frame = self.image.bounds
            gradient.locations = [0,1]
            gradient.colors = [UIColor.clear.cgColor, UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 0.8).cgColor]
            gradient.startPoint = CGPoint(x:0.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 0.0, y:1.0)
            
            self.image.layer.addSublayer(gradient)
            
        }
        //} else {
        //    hotelLabel.text = "\(hotel.name) not available"
        //}
        
    }

}
