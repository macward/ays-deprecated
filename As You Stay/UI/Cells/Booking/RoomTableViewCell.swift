//
//  RoomTypeTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class RoomTableViewCell: UITableViewCell {

    @IBOutlet weak var roomTypeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configure(with booking: BookingDRO) {
        if let room = booking.selectedRoom {
            let refundable = room.refundable ? "Refundable" : "Non-refundable"
            roomTypeLabel.text = "\(room.roomType)\n \(refundable) "
        } else {
            roomTypeLabel.text = ""
        }
        
    }
}
