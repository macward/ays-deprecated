//
//  SavingsTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class SavingsTableViewCell: UITableViewCell {

    @IBOutlet weak var savingsAmmountLabel: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var savingsHoursLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with booking: BookingDRO) {
        savingsAmmountLabel.text = "-$\(booking.selectedRoom?.memberSavings ?? 0)"
        subtotalLabel.text = "$\(booking.selectedRoom?.bookingSubtotal ?? 0)"
        let savingHours = booking.selectedRoom?.savingsDeviation ?? 0
        let savingText = savingHours == 0 ? "" : "(\(savingHours)Hr)"
        savingsHoursLabel.text = savingText
    }
}
