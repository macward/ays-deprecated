//
//  TotalTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit


class TotalTableViewCell: UITableViewCell, BookingTotalDiscountsProtocol{
    
    var viewModel = bookingTotalDiscountsViewModel()
    var delegate: BookingTotalDiscountsDelegate?

    @IBOutlet weak var taxesFeesLabel: UILabel!
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var totalChargesabel: UILabel!
    @IBOutlet weak var loyaltyCreditLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with booking: BookingDRO) {
        taxesFeesLabel.text = "+$\(booking.selectedRoom?.taxesTotal ?? 0)"
        promoCodeLabel.text = "-$\(booking.selectedRoom?.promoDiscounts ?? 0)"
        loyaltyCreditLabel.text = "-$\(booking.loyaltyCredit)"
        totalChargesabel.text = "$\((booking.selectedRoom?.bookingTotal ?? 0) - booking.loyaltyCredit)"
        addTapGestures()
    }
}
   
// MARK: - Add tap gestures
extension TotalTableViewCell {
    
    fileprivate func addTapGestures(){
        let loyaltyTap = UITapGestureRecognizer(target: self, action: #selector(self.handleLoyaltyTap(_:)))
        self.loyaltyCreditLabel.isUserInteractionEnabled = true
        self.loyaltyCreditLabel.addGestureRecognizer(loyaltyTap)
        
        let promoCodeTap = UITapGestureRecognizer(target: self, action: #selector(self.handlePromoCodeTap(_:)))
        self.promoCodeLabel.isUserInteractionEnabled = true
        promoCodeLabel.addGestureRecognizer(promoCodeTap)
    }
    
    @objc private func handleLoyaltyTap(_ sender: UITapGestureRecognizer? = nil){
        delegate?.setLoyaltyAmmount()
    }
    
    @objc private func handlePromoCodeTap(_ sender: UITapGestureRecognizer? = nil){
        delegate?.setPromocode()
    }
}
