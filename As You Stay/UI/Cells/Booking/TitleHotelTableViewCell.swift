//
//  TitleHotelTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class TitleHotelTableViewCell: UITableViewCell {

    @IBOutlet weak var nameHotelLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func configure(with booking: BookingDRO) {
        nameHotelLabel.text = "\(booking.hotelName)"
    }
    
}
