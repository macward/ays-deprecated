//
//  CheckinCheckoutTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class CheckinCheckoutTableViewCell: UITableViewCell {

    @IBOutlet weak var checkinLabel: UILabel!
    @IBOutlet weak var checkoutLabel: UILabel!
    @IBOutlet weak var nightsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with booking: BookingDRO){
        
        // deberia pasarle tambien las fechas al dro
        if let checkin  = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)?.getMapFormatDate(),
           let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)?.getMapFormatDate(){
            
            checkinLabel.text = "\(checkin)"
            checkoutLabel.text = "\(checkout)"
        }
        nightsLabel.text = String(format: "%.1f", booking.nightAmount)
    }
}
