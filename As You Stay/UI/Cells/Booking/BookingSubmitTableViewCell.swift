//
//  BookingSubmitTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 08/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class BookingSubmitTableViewCell: UITableViewCell {

    @IBOutlet weak var submitButton: UIButton!
    
    var delegate: SubmitBookingDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        submitButton.titleEdgeInsets = UIEdgeInsets(top: 4,left: 4,bottom: 4,right: 4)
    }

    @IBAction func didTapSubmitButton(_ sender: Any) {
        self.delegate?.didTapSubmitButton()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with dro: BookingDRO){
        if let total = dro.selectedRoom?.bookingTotal {
            submitButton.titleLabel?.text = "BOOK YOUR STAY $\(total - dro.loyaltyCredit)"
        } else {
            submitButton.titleLabel?.text = "BOOK YOUR STAY"
        }
    }
}
