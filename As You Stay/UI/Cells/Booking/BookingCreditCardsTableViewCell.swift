//
//  CreditCardsTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 07/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class BookingCreditCardsTableViewCell: UITableViewCell {

    @IBOutlet weak var creditCardsContentView: UIView!
    @IBOutlet weak var last4Label: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        creditCardsContentView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with creditCard: CreditCardObject) {
        let viewModel = CreditCardViewModel()
        last4Label.text = creditCard.last4
        cardImage.image = viewModel.setImage(with: creditCard.type)
        
    }
}
