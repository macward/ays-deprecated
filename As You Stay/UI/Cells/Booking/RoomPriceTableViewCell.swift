//
//  RoomPriceTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class RoomPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var roomPriceLabel: UILabel!
    @IBOutlet weak var pricePerNight: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configure(with booking: BookingDRO) {
        pricePerNight.text = "Market price ($\(booking.selectedRoom?.avgMarketPrice ?? 0) per night)"
        roomPriceLabel.text = "$\(booking.selectedRoom?.marketPrice ?? 0)"
    }
}
