//
//  AboutThisHotelTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 8/13/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class AboutThisHotelTableViewCell: UITableViewCell {
    @IBOutlet weak var aboutTitleLabel: UILabel!
    @IBOutlet weak var aboutThisLabel: UILabel!
    
    let grayColorLabel = UIColor(red:0.74, green:0.78, blue:0.86, alpha:1)
    fileprivate var showTrim: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        aboutTitleLabel.text = "ABOUT THIS HOTEL"
        aboutTitleLabel.textColor = UIColor.white
        aboutTitleLabel.font = UIFont.Text.big()
        aboutTitleLabel.numberOfLines = 1
        aboutTitleLabel.adjustsFontSizeToFitWidth = true
        aboutTitleLabel.textAlignment = .left
        
        aboutThisLabel.text = ""
        aboutThisLabel.textColor = UIColor.white
        aboutThisLabel.font = UIFont.Text.normal()
        aboutThisLabel.numberOfLines = 0
        aboutThisLabel.textAlignment = .justified
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setupRow(detail: HotelDetails) {
        if self.showTrim {
            self.setupTrim(detail: detail)
        } else {
            self.showFull(detail: detail)
        }
    }
    
    func makeToggle(){
        showTrim.toggle()
    }
    
    fileprivate func showFull(detail: HotelDetails) {
        aboutThisLabel.text = "\(detail.description)"
        showLessAppend()
    }
    
    fileprivate func setupTrim(detail: HotelDetails) {
        aboutThisLabel.text = "\(detail.description.prefix(100) + "...")"
        showMoreAppend()
    }
    
    func showMoreAppend() {
        let readMoreText = " View More"
        let stringColor: UIColor = UIColor(hexString: "#0CC2EB")
        let attributes = [NSAttributedString.Key.foregroundColor: stringColor]
        let answerAttributed = NSMutableAttributedString(string: self.aboutThisLabel.text!, attributes: [NSAttributedString.Key.font: self.aboutThisLabel.font!])
        let readMoreAttributed = NSMutableAttributedString(string: readMoreText, attributes: attributes)
        answerAttributed.append(readMoreAttributed)
        self.aboutThisLabel.attributedText = answerAttributed
    }
    
    func showLessAppend() {
        let readMoreText = " View Less"
        let stringColor: UIColor = UIColor(hexString: "#0CC2EB")
        let attributes = [NSAttributedString.Key.foregroundColor: stringColor]
        let answerAttributed = NSMutableAttributedString(string: self.aboutThisLabel.text!, attributes: [NSAttributedString.Key.font: self.aboutThisLabel.font!])
        let readMoreAttributed = NSMutableAttributedString(string: readMoreText, attributes: attributes)
        answerAttributed.append(readMoreAttributed)
        self.aboutThisLabel.attributedText = answerAttributed
    }
}


extension UILabel {

    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText

        let lengthForVisibleString: Int = 100
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: 100, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }
}
