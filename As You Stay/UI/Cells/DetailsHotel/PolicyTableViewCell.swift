//
//  PolicyTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 8/13/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import PinLayout

class PolicyTableViewCell: UITableViewCell {

    @IBOutlet weak var policyLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var containtView: UIView!
    
    @IBOutlet var textViewHeightConstraint: NSLayoutConstraint!
    var expanded: Bool = false
    
    @IBOutlet weak var descriptionPolicyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        policyLabel.text = "POLICY"
        policyLabel.textColor = UIColor.white
        policyLabel.font = UIFont.Text.big()
        policyLabel.numberOfLines = 1
        policyLabel.adjustsFontSizeToFitWidth = true
        policyLabel.textAlignment = .left
        
        descriptionPolicyLabel.text = ""
        descriptionPolicyLabel.textColor = UIColor.white
        descriptionPolicyLabel.font = UIFont.Text.big()
        descriptionPolicyLabel.numberOfLines = 0
        descriptionPolicyLabel.lineBreakMode = .byWordWrapping
        descriptionPolicyLabel.sizeToFit()
        
        textViewHeightConstraint.constant = 0
        containtView.backgroundColor = UIColor(red: 43/256, green: 47/256, blue: 58/256, alpha: 1)
        layoutIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setRow(policy: HotelDetails){
        
        for labels in policy.policy {
            descriptionPolicyLabel.text! += "\(labels) \n"
        }
        
        if expanded {
            textViewHeightConstraint.isActive = true
            descriptionPolicyLabel.text = ""
            UIView.animate(withDuration: 0.1, animations: {
//                self.icon.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                        self.icon.transform  = CGAffineTransform.identity
            })
        } else {
            textViewHeightConstraint.isActive = false
            UIView.animate(withDuration: 0.1, animations: {
                self.icon.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            })
        }
        
        UIView.animate(withDuration: 0.5) {
            self.layoutIfNeeded()
            self.expanded = !self.expanded
        }
    }
}
