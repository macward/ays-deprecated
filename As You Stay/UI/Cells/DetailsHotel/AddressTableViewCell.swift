//
//  AddressTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 8/13/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import GoogleMaps
import PinLayout

class AddressTableViewCell: UITableViewCell {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressDescriptionLabel: UILabel!
    @IBOutlet weak var mapView: UIView!
    var map = GMSMapView()
    let marker = GMSMarker()
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addressLabel.text = "ADDRESS"
        addressLabel.textColor = UIColor.white
        addressLabel.font = UIFont.Text.big()
        addressLabel.numberOfLines = 1
        addressLabel.adjustsFontSizeToFitWidth = true
        addressLabel.textAlignment = .left
        
        addressDescriptionLabel.text = "300 West 22nd Street, Chelsea, New York City, NY 10011"
        addressDescriptionLabel.textColor = UIColor.white
        addressDescriptionLabel.font = UIFont.Text.normal()
        addressDescriptionLabel.numberOfLines = 1
        addressDescriptionLabel.adjustsFontSizeToFitWidth = true
        addressDescriptionLabel.textAlignment = .justified
        
        mapView.isUserInteractionEnabled = false
        
        let camera = GMSCameraPosition.camera(withLatitude: 40.73, longitude: -73.93, zoom: 14)
        map = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        map.setMinZoom(12, maxZoom: 20)
        mapView.addSubview(map)
        
        map.translatesAutoresizingMaskIntoConstraints = false
        map.leadingAnchor.constraint(equalTo: mapView.leadingAnchor).isActive = true
        map.trailingAnchor.constraint(equalTo: mapView.trailingAnchor).isActive = true
        map.bottomAnchor.constraint(equalTo: mapView.bottomAnchor).isActive = true
        map.topAnchor.constraint(equalTo: mapView.topAnchor).isActive = true
        
        marker.position = CLLocationCoordinate2D(latitude: 40.73, longitude: -73.93)
        marker.map = map
        map.animate(to: camera)
        
        layoutIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

    }
    override func layoutSubviews() {
    
    }
    
    func configure(with hotel: HotelDetails) {
        addressDescriptionLabel.text = hotel.full_address
        map.clear()
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(hotel.lat)!,
                                              longitude: CLLocationDegrees(hotel.lng)!, zoom: 14)
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(hotel.lat)!,
                                                 longitude: CLLocationDegrees(hotel.lng)!)
        marker.icon = UIImage(named: "locationPinSelected")
        marker.map = map
        map.animate(to: camera)
    }
}
