//
//  ImageHotelTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 8/13/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import ImageSlideshow

class ImageHotelTableViewCell: UITableViewCell {
   
    fileprivate var slideshow: ImageSlideshow = ImageSlideshow(frame: .zero)
    fileprivate var parentViewController: UIViewController!
    
    @IBOutlet weak var hotelImage: UIImageView!
    
    let gradient = CAGradientLayer()
    let ImageHotelViewModel = HotelResultsViewModel()
    let sView: UIView = {
        let sView = UIView(frame: .zero)
        sView.backgroundColor = .clear
        return sView
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addSubview(sView)
        
        sView.translatesAutoresizingMaskIntoConstraints = false
        sView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        sView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        sView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        sView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        sView.layer.addSublayer(gradient)
        
        let imageViewRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        hotelImage.addGestureRecognizer(imageViewRecognizer)
        
        let shadowViewRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        sView.addGestureRecognizer(shadowViewRecognizer)
        
        layoutIfNeeded()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradient.frame = sView.bounds
        gradient.locations = [0,1]
        gradient.colors = [UIColor.clear.cgColor, UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x:0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y:1.0)
        
    }
    

    func configure(with hotel: HotelDetails, parent: UIViewController) {
        ImageHotelViewModel.getImageHotel(url: hotel.album[0].url, completion: { (image) in
            self.hotelImage.image = image
        })
        parentViewController = parent
        configureSlideshow(with: hotel.album)
    }
    
    fileprivate func configureSlideshow(with album: [AlbumElement]) {
        
        var slideshowSource: [AlamofireSource] = []
        for item in album {
            slideshowSource.append(AlamofireSource(urlString: item.url)!)
        }
        slideshow.setImageInputs(slideshowSource)
    }
    
    @objc func didTap() {
        let fullScreenController = slideshow.presentFullScreenController(from: self.parentViewController)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
}
