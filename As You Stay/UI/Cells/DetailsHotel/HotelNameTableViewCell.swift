//
//  HotelNameTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 8/13/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class HotelNameTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameHotelLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    let grayColorLabel = UIColor(red:0.74, green:0.78, blue:0.86, alpha:1)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameHotelLabel.text = "HOTEL NAME"
        nameHotelLabel.textColor = UIColor.white
        nameHotelLabel.font = UIFont.App.bold(size: 20)
        nameHotelLabel.numberOfLines = 2
        nameHotelLabel.adjustsFontSizeToFitWidth = true
        
        distanceLabel.text = "DISTANCE 0.7"
        distanceLabel.textColor = grayColorLabel
        distanceLabel.font = UIFont.Text.normal()
        distanceLabel.numberOfLines = 1
        distanceLabel.adjustsFontSizeToFitWidth = true
        distanceLabel.textAlignment = .left
        
        ratingLabel.text = " 0.7"
        ratingLabel.textColor = grayColorLabel
        ratingLabel.font = UIFont.Text.normal()
        ratingLabel.numberOfLines = 1
        ratingLabel.adjustsFontSizeToFitWidth = true
        ratingLabel.textAlignment = .left
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    

    
    func setupRow(hotel: HotelResponse){
        nameHotelLabel.text = hotel.name
        
        let distance = String(format: "%.01f", hotel.distance)
        distanceLabel.text = "\(distance)m - New York, NY"

        let rating = String(format: "%.01f", hotel.review_rating)
        ratingLabel.text = "\(rating)%"
    }
    
    func configure(with hotel: HotelDetails) {
        
        nameHotelLabel.text = "\(hotel.name)"
        distanceLabel.text = "\(hotel.full_address)"
        ratingLabel.text = "\(hotel.star_rating)%"
    }
}
