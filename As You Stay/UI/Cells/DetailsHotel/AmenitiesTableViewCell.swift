//
//  AmenitiesTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 8/13/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class AmenitiesTableViewCell: UITableViewCell {

    @IBOutlet weak var amenitiesLabel: UILabel!
    let icon_1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
    let icon_2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
    let icon_3 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
    let icon_4 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
    let icon_5 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        amenitiesLabel.text = "AMENITIES"
        amenitiesLabel.textColor = UIColor.white
        amenitiesLabel.font = UIFont.Text.big()
        amenitiesLabel.numberOfLines = 1
        amenitiesLabel.adjustsFontSizeToFitWidth = true
        amenitiesLabel.textAlignment = .left
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
