//
//  HotelsCollectionViewCellNew.swift
//  As You Stay
//
//  Created by Max Ward on 8/1/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import PinLayout
import Kingfisher

class HotelsCollectionViewCellNew: UICollectionViewCell {
    
    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var nameHotelLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var iconRatingImage: UIImageView!
    
    @IBOutlet weak var wasLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var wasPriceLabel: UILabel!
    
    let backgroundCellColor = UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1)
    let grayColorLabel = UIColor(red:0.74, green:0.78, blue:0.86, alpha:1)
    let priceColor = UIColor(red:0.2, green:0.72, blue:0.84, alpha:1)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = backgroundCellColor
        self.layer.cornerRadius = 5.0
        clipsToBounds = false
        addShadow()
    }

    func setupRowHotel(hotel: HotelResponse) {
        nameHotelLabel.text = hotel.name
        nameHotelLabel.textColor = UIColor.white
        nameHotelLabel.font = UIFont.Header.h1()
        nameHotelLabel.numberOfLines = 2
//        nameHotelLabel.adjustsFontSizeToFitWidth = true
        
        let distance = String(format: "%.01f", hotel.distance)
        //full_address : "455 Washington Boulevard, Jersey City, New Jersey"
        let cityTxt = hotel.full_address
        
        let cityTxtArray = cityTxt.components(separatedBy: ",")
        if cityTxtArray.count > 1 {
            distanceLabel.text = "\(distance)m - \(cityTxtArray[1])"
        } else {
            distanceLabel.text = "\(distance)m - \(cityTxtArray[0])"
        }
        
        distanceLabel.textColor = grayColorLabel
        distanceLabel.font = UIFont.Text.big()
        distanceLabel.numberOfLines = 1
        distanceLabel.adjustsFontSizeToFitWidth = true
        distanceLabel.textAlignment = .left
        
        hotelImage.contentMode = .scaleAspectFill
        hotelImage.roundCorners(corners: [.topLeft, .bottomLeft], radius: 5)
        iconRatingImage.contentMode = .scaleToFill
        
        let rating = String(format: "%.01f", hotel.review_rating)
        ratingLabel.text = "\(rating)%"
        ratingLabel.textColor = grayColorLabel
        ratingLabel.font = UIFont.Text.big()
        ratingLabel.numberOfLines = 1
        ratingLabel.adjustsFontSizeToFitWidth = true
        ratingLabel.textAlignment = .left
        
        priceLabel.text = "$\(hotel.avg_night_price.noZero)"
        priceLabel.textColor = priceColor
        priceLabel.font = UIFont.App.bold(size: 24)
        priceLabel.sizeToFit()
        
        wasPriceLabel.attributedText = "$\(hotel.avg_market_price.noZero)".strikeThrough()
        wasPriceLabel.textColor = grayColorLabel
        wasPriceLabel.font = UIFont.Text.big()
        wasPriceLabel.sizeToFit()
        
        wasLabel.textColor = grayColorLabel
        wasLabel.font = UIFont.Text.normal()
        wasLabel.sizeToFit()
        if let url = URL(string: hotel.photo.url) {
            hotelImage.kf.setImage(with: .network(url))
        } else {
            hotelImage.backgroundColor = .lightGray
        }
        
        
    }
}
extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}
