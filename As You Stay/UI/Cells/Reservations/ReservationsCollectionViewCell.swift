//
//  ReservationsCollectionViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 30/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class ReservationsCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var bodyView: BodyView!
    @IBOutlet weak var barCodeView: BarCodeView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(using receipt: ReceiptDRO) {
        setupBody(with: receipt)
        setupBanner(with: receipt)
        setupBarcode(with: receipt)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bannerView.subviews.forEach({ $0.removeFromSuperview() })
        bodyView.subviews.forEach({ $0.removeFromSuperview() })
        barCodeView.subviews.forEach({ $0.removeFromSuperview() })
    }
    
    fileprivate func setupBanner(with item: ReceiptDRO) {
        let view = BannerView.configure(with: item)
        bannerView.addSubview(view)
        view.frame = bannerView.bounds
    }
    
    fileprivate func setupBody(with item: ReceiptDRO) {
        let view = BodyView.configure(using: item)
        bodyView.addSubview(view)
        view.frame = bodyView.bounds
    }
    
    fileprivate func setupBarcode(with item: ReceiptDRO){
        let view = BarCodeView.configure(code: item.bookingID)
        barCodeView.addSubview(view)
        view.frame = barCodeView.bounds
    }
}
