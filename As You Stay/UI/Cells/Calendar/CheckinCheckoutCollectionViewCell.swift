//
//  CheckinCheckoutCollectionViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 8/21/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class CheckinCheckoutCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var amPmLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.frame.size = CGSize(width: 34, height: 34)
        
        hoursLabel.font = UIFont.App.regular(size: 12)
        hoursLabel.textAlignment = .center
        
        amPmLabel.font = UIFont.App.regular(size: 12)
        amPmLabel.textAlignment = .center
        
    }
    
    func setRow(hour: Int) {
        var formatedHour: Int = 0
        var indicator: String = "AM"
        if hour < 12 {
            if hour == 0 {
                formatedHour = 12
            }
            formatedHour = hour
        } else {
            if hour == 12 {
                formatedHour = hour
            } else {
                formatedHour = hour - 12
            }
            indicator = "PM"
        }
        hoursLabel.text = "\(formatedHour):00"
        amPmLabel.text = indicator
    }
    
    func selectedRow(){
        borderWidth = 1.0
        cornerRadius = 5.0
        borderColor = UIColor(red:0.12, green:0.62, blue:0.8, alpha:1)
        
        hoursLabel.textColor = UIColor(red:0.12, green:0.62, blue:0.8, alpha:1)
        hoursLabel.font = UIFont.App.regular(size: 15)
        
        amPmLabel.font = UIFont.App.regular(size: 15)
        amPmLabel.textColor = UIColor(red:0.12, green:0.62, blue:0.8, alpha:1)
        //self.frame.size  =  CGSize(width: 66, height: 55)
        //self.frame = CGRect(x: -self.center.x, y: -self.center.y, width: 66, height: 55)
    }
    
    func deselectRow(){
        borderColor = .clear
        hoursLabel.textColor = .white
        amPmLabel.textColor = .white
        
        hoursLabel.font = UIFont.App.regular(size: 12)
        amPmLabel.font = UIFont.App.regular(size: 12)

        //self.frame.size = CGSize(width: 34, height: 34)
       // self.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
    }
}
