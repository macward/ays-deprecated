//
//  DetailAmenitiesTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 9/2/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class DetailAmenitiesTableViewCell: UITableViewCell {

    @IBOutlet weak var amenityLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        
        amenityLabel.textColor = UIColor.white
        amenityLabel.font = UIFont.App.regular(size: 18)
        amenityLabel.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
  
    func setRow(amenity: String ){
        amenityLabel.text = amenity
    }
}
