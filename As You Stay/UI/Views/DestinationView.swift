//
//  DestinationView.swift
//  As You Stay
//
//  Created by Max Ward on 7/29/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit
import PinLayout
import GoogleMaps

class DestinationView: UIView {
    
    var containerView = UIView()
    let containerDestinationView = UIView()
    let destinationView = UIView()
    let sinceDate = UIView()
    let toDate = UIView()
    let firstSeparator = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 1))
    let verticalSeparator = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: 0))
    let secondSeparator = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 1))
    let destinationLabel = UILabel()
    let chooseDateLabel = UILabel()
    let numberOfRoomsLabel = UILabel()
    let dateLabel = UILabel()
    let roomsLabel = UILabel()
    let titleColor = UIColor(red: 152/256, green: 152/256, blue: 152/256, alpha: 1)
    let separatorColor = UIColor(red: 216/256, green: 216/256, blue: 216/256, alpha: 1)
    let searchTextField = UITextField()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        containerDestinationView.addShadow()
        let camera = GMSCameraPosition.camera(withLatitude: 40.73, longitude: -73.93, zoom: 14)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        do {
            if let styleURL = Bundle.main.url(forResource: "kmapstyle", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        containerView = mapView
        addSubview(containerView)
        containerDestinationView.backgroundColor = UIColor.white
        containerDestinationView.layer.cornerRadius = 15.0
        
        firstSeparator.backgroundColor = separatorColor
        verticalSeparator.backgroundColor = separatorColor
        secondSeparator.backgroundColor = separatorColor
        
        destinationLabel.text = "DESTINATION"
        destinationLabel.textColor = titleColor
        destinationLabel.font = UIFont.systemFont(ofSize: 10)
        destinationLabel.textAlignment = .left
       
        chooseDateLabel.text = "CHOOSE DATE"
        chooseDateLabel.textColor = titleColor
        chooseDateLabel.font = UIFont.systemFont(ofSize: 10)
        chooseDateLabel.textAlignment = .left
        
        numberOfRoomsLabel.text = "NUMBER OF ROOMS"
        numberOfRoomsLabel.textColor = titleColor
        numberOfRoomsLabel.font = UIFont.systemFont(ofSize: 10)
        numberOfRoomsLabel.textAlignment = .left
        
        dateLabel.text = "19 Sep - 21 Sep"
        dateLabel.textColor = .black
        dateLabel.font = UIFont.systemFont(ofSize: 14)
        dateLabel.textAlignment = .left
        
        roomsLabel.text = "1 Room - 2 Adults"
        roomsLabel.textColor = .black
        roomsLabel.font = UIFont.systemFont(ofSize: 14)
        roomsLabel.textAlignment = .left
        
        searchTextField.backgroundColor = UIColor(red: 248/256, green: 248/256, blue: 248/256, alpha: 1)
        searchTextField.setCustomTextField(border: 0, image: UIImage(named: "icon-search-gray"), placeholder: "")
        searchTextField.text = "Toronto"
        searchTextField.layer.cornerRadius = 10

        destinationView.addSubview(searchTextField)
        destinationView.addSubview(destinationLabel)
        sinceDate.addSubview(chooseDateLabel)
        sinceDate.addSubview(dateLabel)
        toDate.addSubview(numberOfRoomsLabel)
        toDate.addSubview(roomsLabel)
        containerView.addSubview(containerDestinationView)
        containerDestinationView.addSubview(destinationView)
        containerDestinationView.addSubview(firstSeparator)
        containerDestinationView.addSubview(sinceDate)
        containerDestinationView.addSubview(verticalSeparator)
        containerDestinationView.addSubview(toDate)
        containerDestinationView.addSubview(secondSeparator)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.pin.all(0)
        
        containerDestinationView.pin.left(10).top(106).right(10).width(355).height(166)
    
        destinationView.pin.left(10).top(20).right(10).height(61)
        
        destinationLabel.pin.left(10).width(70).height(12)
        searchTextField.pin.left(0).right(0).bottom(0).height(40)
        
        firstSeparator.pin.left(10).below(of: destinationView).marginTop(12).right(10)
        
        sinceDate.pin.left(10).below(of: firstSeparator).width(166).height(37).marginTop(12)
       
        chooseDateLabel.pin.left(10).width(74).height(12)
        dateLabel.pin.left(10).below(of: chooseDateLabel).marginTop(9).width(176).height(16)
        
        verticalSeparator.pin.after(of: sinceDate).marginLeft(1.5).marginRight(1.5).below(of: firstSeparator).marginTop(12).height(37)
        
        toDate.pin.after(of: verticalSeparator).below(of: firstSeparator).marginTop(12).width(166).height(37)
        
        numberOfRoomsLabel.pin.left(15).width(176).height(12)
        roomsLabel.pin.left(15).below(of: numberOfRoomsLabel).marginTop(9).right(10).height(16)
        secondSeparator.pin.left(10).below(of: toDate).right(10).marginTop(9)
    }
}
