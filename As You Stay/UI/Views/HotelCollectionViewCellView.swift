//
//  HotelCollectionViewCellView.swift
//  As You Stay
//
//  Created by Max Ward on 8/1/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit
import PinLayout

class HotelCollectionViewCellView: UIView{
    var containerView = UIView()
    var leftView = UIView()
    var topRightView = UIView()
    var bottomRightView = UIView()
    
    let imageView = UIImageView(image: UIImage(named: "hotel-room"))
    let hotelNameLabel = UILabel()
    let distanceLabel = UILabel()
    let priceLabel = UILabel()
    let wasLabel = UILabel()
    let wasPriceLabel = UILabel()
    let tripAdvisorView = UIView()
    let tripAdvisorIconView = UIImageView(image: UIImage(named: "Owl"))
    let tripAdvisorRatingView = UIImageView(image: UIImage(named: "FullCircle"))
    
    let backgroundCellColor = UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1)
    let grayColorLabel = UIColor(red:0.74, green:0.78, blue:0.86, alpha:1)
    let priceColor = UIColor(red:0.2, green:0.72, blue:0.84, alpha:1)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        containerView.backgroundColor = backgroundCellColor
        
        tripAdvisorView.addSubview(tripAdvisorIconView)
        tripAdvisorView.addSubview(tripAdvisorRatingView)
        topRightView.addSubview(tripAdvisorView)
        
        hotelNameLabel.text = "Cassa Times Square"
        hotelNameLabel.textColor = UIColor.white
        hotelNameLabel.font = UIFont.boldSystemFont(ofSize: 16)
        hotelNameLabel.textAlignment = .left
        topRightView.addSubview(hotelNameLabel)
        
        distanceLabel.text = "0.7m - New York, NY"
        distanceLabel.textColor = grayColorLabel
        distanceLabel.font = UIFont.systemFont(ofSize: 12)
        distanceLabel.textAlignment = .left
        topRightView.addSubview(distanceLabel)
        
        topRightView.addSubview(tripAdvisorView)
        
        priceLabel.text = "$1210"
        priceLabel.textColor = priceColor
        priceLabel.font = UIFont.systemFont(ofSize: 25)
        priceLabel.textAlignment = .left
        bottomRightView.addSubview(priceLabel)
        
        wasLabel.text = "was"
        wasLabel.textColor = grayColorLabel
        wasLabel.font = UIFont.systemFont(ofSize: 12)
        wasLabel.textAlignment = .left
        bottomRightView.addSubview(wasLabel)
        
        wasPriceLabel.text = "$275"
        wasPriceLabel.textColor = grayColorLabel
        wasPriceLabel.font = UIFont.systemFont(ofSize: 12)
        wasPriceLabel.textAlignment = .left
        bottomRightView.addSubview(wasPriceLabel)
        
        leftView.clipsToBounds = true
        leftView.addSubview(imageView)
       
        containerView.addSubview(leftView)
        containerView.addSubview(topRightView)
        containerView.addSubview(bottomRightView)
        
        addSubview(containerView)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 5.0
        topRightView.layer.cornerRadius = 5.0
        bottomRightView.layer.cornerRadius = 5.0
        
        imageView.roundCorners(corners: [.topLeft, .bottomLeft], radius: 10.0)
        
        containerView.pin.left(2).top(2).right(2).width(UIHelper.multiplierFrom8() * 308).height(128)
        leftView.pin.left(0).top(0).width(30%).height(100%)
        
        topRightView.pin.after(of: leftView).marginLeft(0).top(0).width(70%).height(64%)
        bottomRightView.pin.after(of: leftView).marginLeft(0).below(of: topRightView).marginTop(0).right(0).width(70%).height(36%)
        
        hotelNameLabel.pin.left(8).top(8).right(0).height(22%)
        distanceLabel.pin.left(8).below(of: hotelNameLabel).marginTop(4).right(0).height(22%)
        tripAdvisorView.pin.left(8).below(of: distanceLabel).marginTop(4).right(0).height(22%)
        priceLabel.pin.left(8).bottom(8).width(35%).height(80%)
        wasLabel.pin.after(of: priceLabel).bottom(16).width(12%).height(30%)
        wasPriceLabel.pin.after(of: wasLabel).bottom(16).width(15%).height(30%)
        
        imageView.pin.all()
       
        tripAdvisorIconView.pin.left(0).top(0).width(25).height(15)
        tripAdvisorRatingView.pin.after(of: tripAdvisorIconView).marginLeft(3).top(3).width(11).height(11)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
