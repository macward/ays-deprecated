//
//  HotelDetailsHeader.swift
//  As You Stay
//
//  Created by Max Ward on 16/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit
import ImageSlideshow


class HotelDetailsHeader: UIView {
    
    var image = UIImageView()
    let gradient = CAGradientLayer()
    let sView: UIView = {
        let sView = UIView(frame: .zero)
        sView.backgroundColor = .clear
        return sView
    }()
    
    fileprivate var slideshow: ImageSlideshow = ImageSlideshow(frame: .zero)
    fileprivate var parentViewController: UIViewController!
    let ImageHotelViewModel = HotelResultsViewModel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(image)
        addSubview(sView)
        let imageViewRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        image.addGestureRecognizer(imageViewRecognizer)
              
        let shadowViewRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        sView.addGestureRecognizer(shadowViewRecognizer)
        self.clipsToBounds = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        image.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        image.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        image.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        image.contentMode = .scaleAspectFill
        
        sView.translatesAutoresizingMaskIntoConstraints = false
        sView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        sView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        sView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        sView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        sView.layer.addSublayer(gradient)
        
        gradient.frame = sView.bounds
        gradient.locations = [0,1]
        gradient.colors = [UIColor.clear.cgColor, UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x:0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y:1.0)
        
        layoutIfNeeded()
    }
    
    fileprivate func configureSlideshow(with album: [AlbumElement]) {
        
        var slideshowSource: [AlamofireSource] = []
        for item in album {
            slideshowSource.append(AlamofireSource(urlString: item.url)!)
        }
        slideshow.setImageInputs(slideshowSource)
    }
    
    @objc func didTap() {
        let fullScreenController = slideshow.presentFullScreenController(from: self.parentViewController)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    func configure(with hotel: HotelDetails, parent: UIViewController) {
        ImageHotelViewModel.getImageHotel(url: hotel.album[0].url, completion: { (image) in
            self.image.image = image
        })
        parentViewController = parent
        configureSlideshow(with: hotel.album)
    }
}
