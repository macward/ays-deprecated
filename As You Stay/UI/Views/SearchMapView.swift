//
//  SearchMapView.swift
//  As You Stay
//
//  Created by Max Ward on 25/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import PinLayout
import GoogleMaps
import AssistantKit


class SearchMapView: UIView {
    
    let flowLayout = UICollectionViewFlowLayout()
    var containerView: UIView!
    var infoContentView: UIView!
    
    let leftContainerView = UIView()
    let rightContainerView = UIView()
    let bottomLeftContainerView = UIView()
    let bottomRightContainerView = UIView()
    
    fileprivate let topContainerView = UIView()
    fileprivate let bottomContainerView = UIView()
    
    let whereLabel = UILabel()
    let cityLabel = UILabel()
    
    let nightsLabel = UILabel()
    let totalNightsLabel = UILabel()
    
    let checkinLabel = UILabel()
    let checkoutLabel = UILabel()
    let checkinIconView = UIImageView()
    let checkoutIconView = UIImageView()
    
    let hDividerLine = UIView()
    
    var didTapLocationTrigger: (() -> Void)?
    var didTapCalendarTrigger: (() -> Void)?
    var didCreateHotelMarkerList: ((_ map: GMSMapView) -> Void)?
    var mapView = GMSMapView()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        backgroundColor = .white
        
        let separator = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: 20))
        separator.backgroundColor = .white
        
        infoContentView = UIView(frame: .zero)
        infoContentView.backgroundColor = UIColor(red:32/256, green:35/256, blue:42/256, alpha:1)
        
        infoContentView.addShadow()
        infoContentView.clipsToBounds = false
        
        whereLabel.text = "WHERE:"
        whereLabel.textColor = UIColor(red:0.74, green:0.78, blue:0.86, alpha:1)
        whereLabel.textAlignment = .left
        whereLabel.font = UIFont.Text.normal()
        whereLabel.sizeToFit()
        
        leftContainerView.addSubview(whereLabel)
        
        cityLabel.text = "New York City"
        cityLabel.textAlignment = .left
        cityLabel.textColor = UIColor(red:0.2, green:0.72, blue:0.84, alpha:1)
        cityLabel.font = UIFont.App.bold(size: 16)
        cityLabel.minimumScaleFactor = 0.1
        cityLabel.numberOfLines = 1
        cityLabel.adjustsFontSizeToFitWidth = true
        
        leftContainerView.addSubview(cityLabel)
        
        nightsLabel.text = "NIGHTS:"
        nightsLabel.textColor = UIColor(red:0.74, green:0.78, blue:0.86, alpha:1)
        nightsLabel.textAlignment = .left
        nightsLabel.font = UIFont.Text.normal()
        nightsLabel.sizeToFit()
        
        rightContainerView.addSubview(nightsLabel)
        
        totalNightsLabel.text = "2.5 Nights"
        totalNightsLabel.textColor = UIColor(red:0.2, green:0.72, blue:0.84, alpha:1)
        totalNightsLabel.textAlignment = .left
        totalNightsLabel.font = UIFont.App.bold(size: 16)
        totalNightsLabel.minimumScaleFactor = 0.1
        totalNightsLabel.numberOfLines = 1
        totalNightsLabel.adjustsFontSizeToFitWidth = true
        
        rightContainerView.addSubview(totalNightsLabel)
        
        
        checkinLabel.text = "Mon, JAN 18 - 10 AM"
        checkinLabel.textColor = .white
        checkinLabel.textAlignment = .center
        checkinLabel.font =  UIFont.App.bold(size: 14)
        checkinLabel.minimumScaleFactor = 0.1
        checkinLabel.numberOfLines = 1
        checkinLabel.adjustsFontSizeToFitWidth = true
        
        checkinIconView.image = UIImage(named: "icon-sign-in")
        checkinIconView.contentMode = .scaleAspectFit
        checkinIconView.image = UIImage(named: "icon-sign-in")?.withRenderingMode(.alwaysTemplate)
        checkinIconView.tintColor = UIColor(red:0.2, green:0.72, blue:0.84, alpha:1)
        
        bottomLeftContainerView.addSubview(checkinLabel)
        bottomLeftContainerView.addSubview(checkinIconView)
        
        checkoutLabel.frame = self.bounds
        checkoutLabel.text = "Tue, JAN 20 - 16 PM"
        checkoutLabel.textColor = .white
        checkoutLabel.font =  UIFont.App.bold(size: 14)
        checkoutLabel.textAlignment = .center
        checkoutLabel.minimumScaleFactor = 0.1
        checkoutLabel.numberOfLines = 1
        checkoutLabel.adjustsFontSizeToFitWidth = true
        
        checkoutIconView.image = UIImage(named: "icon-sign-out")?.withRenderingMode(.alwaysTemplate)
        checkoutIconView.tintColor = UIColor(red:0.2, green:0.72, blue:0.84, alpha:1)
        checkoutIconView.contentMode = .scaleAspectFit
        
        bottomRightContainerView.addSubview(checkoutLabel)
        bottomRightContainerView.addSubview(checkoutIconView)
        
        hDividerLine.backgroundColor = UIColor(red:0.74, green:0.78, blue:0.86, alpha:1)
        
        bottomContainerView.addSubview(hDividerLine)
        bottomContainerView.addSubview(bottomRightContainerView)
        bottomContainerView.addSubview(bottomLeftContainerView)
        
        topContainerView.addSubview(leftContainerView)
        topContainerView.addSubview(rightContainerView)
        
        infoContentView.addSubview(topContainerView)
        infoContentView.addSubview(bottomContainerView)
        
        let camera = GMSCameraPosition.camera(withLatitude: 40.73, longitude: -73.93, zoom: 14)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        //mapView.setMinZoom(12, maxZoom: 20)
        //mapView.delegate = self
        containerView = mapView
        addSubview(mapView)
        addSubview(infoContentView)
        
        let leftViewTap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
        leftContainerView.isUserInteractionEnabled = true
        leftContainerView.addGestureRecognizer(leftViewTap)
        
        let rightViewTap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
        rightContainerView.isUserInteractionEnabled = true
        rightContainerView.addGestureRecognizer(rightViewTap)
    }
    
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        if sender.view == rightContainerView {
            self.didTapCalendarTrigger?()
        } else if sender.view == leftContainerView {
            self.didTapLocationTrigger?()
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints(){
        
        containerView.pin.all()
        infoContentView.cornerRadius = 5
        infoContentView.pin.top(76 * UIHelper.multiplier()).left(16).right(16).height(93)
        topContainerView.pin.left().top().right().height(65%)
        bottomContainerView.pin.left().bottom().right().height(35%)
        
        leftContainerView.pin.left().top().bottom().width(50%)
        rightContainerView.pin.right().top().bottom().width(50%)
        
        bottomLeftContainerView.pin.left().top().bottom().width(50%)
        bottomRightContainerView.pin.right().top().bottom().width(50%)
        
        whereLabel.pin.left(8).top(8).right().height(8)
        cityLabel.pin.below(of: whereLabel).left(8).top().right().bottom()
        
        nightsLabel.pin.left(8).top(8).right().height(12)
        totalNightsLabel.pin.below(of: nightsLabel).left(8).top().right().bottom()
        
        hDividerLine.pin.height(0.5).left(8).right(8).top(0)
        
        checkinIconView.pin.left(8).below(of: hDividerLine).marginTop(8).width(12).height(12)
        checkinLabel.pin.after(of: checkinIconView).marginLeft(8).below(of: hDividerLine).marginTop(8).right(0).height(12)
        
        checkoutIconView.pin.left(8).below(of: hDividerLine).marginTop(8).width(12).height(12)
        checkoutLabel.pin.after(of: checkoutIconView).marginLeft(8).below(of: hDividerLine).marginTop(8).right(4).height(12)
        
        
        flowLayout.itemSize = CGSize(width:frame.width - 80, height:216)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        didCreateHotelMarkerList?(mapView)
    }
    
    func toFullScreen(){
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.infoContentView.pin.top().left().right().height(120)
            self.infoContentView.cornerRadius = 0
            self.topContainerView.pin.left().top(0).right().height(65%)
            self.bottomContainerView.pin.left().bottom().right().height(35%)
            self.leftContainerView.pin.left().top().bottom().width(50%)
            self.rightContainerView.pin.right().top().bottom().width(50%)
            
            self.bottomLeftContainerView.pin.left().top().bottom().width(50%)
            self.bottomRightContainerView.pin.right().top().bottom().width(50%)
            
            self.whereLabel.pin.left(8).top(30).right().height(12)
            self.cityLabel.pin.below(of: self.whereLabel).left(8).top(0).right().bottom()
            
            self.nightsLabel.pin.left(8).top(30).right().height(12)
            self.totalNightsLabel.pin.below(of: self.nightsLabel).left(8).top().right().bottom()
            
            self.hDividerLine.pin.height(0.5).left(8).right(8).top(0)
            
            self.checkinIconView.pin.left(8).width(12).height(12).vCenter()
            self.checkinLabel.pin.after(of: self.checkinIconView).marginLeft(8).right(0).height(12).vCenter()
            
            self.checkoutIconView.pin.left(8).width(12).height(12).vCenter()
            self.checkoutLabel.pin.after(of: self.checkoutIconView).marginLeft(8).right(0).height(12).vCenter()
            //
            self.flowLayout.itemSize = CGSize(width:self.frame.width - 80, height:216)
        }, completion: nil)
        
    }
    
    func toParcialScreen(){
        UIView.animate(withDuration: 0.3, animations: {
            self.setupConstraints()
        })
    }
}
