//
//  CreditCardLocalService.swift
//  As You Stay
//
//  Created by Max Ward on 06/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//
import Foundation


class CreditCardLocalService {
    
    static func store(creditCard: CreditCardObject) {
        let dataProvider = DataProvider()
        dataProvider.add(creditCard)
    }
    
    static func destroy() {
        let dataProvider = DataProvider()
        dataProvider.clearAllData()
    }
    
    static func destroy(with identification: String) {
        let dataProvider = DataProvider()
        let predicate = NSPredicate(format: "identification == %@", identification)
        guard let obj = dataProvider.objects(CreditCardObject.self, predicate: predicate)?.first ?? nil else { return }
        dataProvider.delete(obj)
    }

    static func list() -> [CreditCardObject]?{
        let dataProvider = DataProvider()
        let list = dataProvider.objects(CreditCardObject.self)
        return list?.toArray(type: CreditCardObject.self)
    }
    
    static func isPrimary(with id: String) -> Bool {
        let dataProvider = DataProvider()
        let predicate = NSPredicate(format: "id == %@", id)
        guard (dataProvider.objects(CreditCardObject.self,predicate: predicate)?.first ?? nil) != nil else { return false }
        return true
    }
    
    static func setAsPrimary( with identification: String) {
        guard let newPrimary  = self.getCreditCard(with: identification) else { return }
        guard let old  = self.getPrimaryCard() else { return }
        let dataProvider = DataProvider ()
        dataProvider.runTransaction {
            old.isPrimary = false
            newPrimary.isPrimary = true
        }
    }
    static func getCreditCard (with identification: String) -> CreditCardObject? {
        let dataProvider = DataProvider()
        let predicate = NSPredicate(format: "identification == %@", identification)
        guard let obj = dataProvider.objects(CreditCardObject.self, predicate: predicate)?.first ?? nil else { return nil}
        return obj
    }
    
    static func getPrimaryCard() -> CreditCardObject? {
        let dataProvider = DataProvider()
        let predicate = NSPredicate(format: "isPrimary == true")
        guard let obj = dataProvider.objects(CreditCardObject.self, predicate: predicate)?.first ?? nil else { return  nil }
        return obj
    }
    
    static func updateLabel(with identification: String, newLabel: String) {
        guard let card  = self.getCreditCard(with: identification) else { return }
        let dataProvider = DataProvider ()
        dataProvider.runTransaction {
            card.label = newLabel
       }
    }

}

