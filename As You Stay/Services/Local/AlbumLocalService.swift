//
//  AlbumLocalService.swift
//  As You Stay
//
//  Created by Max Ward on 13/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class AlbumLocalService {
    
    static func destroy(photo: AlbumElementObject) {
        let dataProvider = DataProvider()
        dataProvider.delete(photo)
    }
    
    static func getLastIndex( )-> Int{
        let dataProvider = DataProvider()
        if let photo = dataProvider.objects(AlbumElementObject.self)?.sorted(byKeyPath: "id", ascending: false ).first {
            return photo.id
        } else {
            return 0
        }
    }
}
