//
//  BookingLocalService.swift
//  As You Stay
//
//  Created by Zibra Coders on 12/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class BookingLocalService {
    static func getIndex() -> Int {
        let dataProvider = DataProvider()
        if let lastIndex = dataProvider.objects(BookingObject.self)?.last?.id {
            return lastIndex + 1
        }
        return 1
    }
    
    
}

