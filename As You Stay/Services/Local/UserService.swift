//
//  UserService.swift
//  As You Stay
//
//  Created by Max Ward on 04/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class UserService {
    
    static func getIndex() -> Int {
        let dataProvider = DataProvider()
        if let lastIndex = dataProvider.objects(SessionObject.self)?.last?.id {
            return lastIndex + 1
        }
        return 1
    }
}
