//
//  ProfileLocalService.swift
//  As You Stay
//
//  Created by Max Ward on 06/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation


class ProfileLocalService {
    
    static func getIndex() -> Int {
//        let dataProvider = DataProvider()
//        if let lastIndex = dataProvider.objects(ProfileObject.self)?.last?.id {
//            return lastIndex + 1
//        }
        return 1
    }
    static func getProfile() -> ProfileObject? {
        let dataProvider = DataProvider()
        return dataProvider.objects(ProfileObject.self)?.first
    }
    
    static func store(profile: ProfileObject) {
        let dataProvider = DataProvider()
        dataProvider.add(profile)
    }
    
    static func destroy() {
        let dataProvider = DataProvider()
        guard let profile = dataProvider.objects(ProfileObject.self)?.first ?? nil else { return }
        dataProvider.delete(profile)
    }
    
    static func exists () -> Bool {
        let dataProvider = DataProvider()
        if (dataProvider.objects(ProfileObject.self)?.first) != nil {
            return true
        }
        return false
    }
    
    static func updateLoyalty(substract ammount: Float){
        let dataProvider = DataProvider()
        guard let profile = dataProvider.objects(ProfileObject.self)?.first ?? nil else { return }
        dataProvider.runTransaction {
            profile.accountBalance -= ammount
        }
    }
    
    static func updateLoyalty(add ammount: Float){
        let dataProvider = DataProvider()
        guard let profile = dataProvider.objects(ProfileObject.self)?.first ?? nil else { return }
        dataProvider.runTransaction {
            profile.accountBalance += ammount
        }
    }
    
    static func update(firstName: String, lastName: String, phone: String) {
        guard let profile  = self.getProfile() else { return }
         let dataProvider = DataProvider ()
         dataProvider.runTransaction {
            profile.firstName = firstName
            profile.lastName = lastName
            profile.phone = phone

        }
    }
}
