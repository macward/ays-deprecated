//
//  TokenService.swift
//  As You Stay
//
//  Created by Max Ward on 04/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class TokenService {
    
    static func accessToken() -> String? {
        let dataProvider = DataProvider()
        if let token = dataProvider.objects(SessionObject.self)?.first {
            return token.token
        }
        return nil
    }
    
    static func refreshToken(){
        
    }
}
