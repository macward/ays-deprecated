//
//  LocalAuthService.swift
//  As You Stay
//
//  Created by Max Ward on 7/18/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class SessionService {
    
    static func exists () -> Bool {
        let dataProvider = DataProvider()
        if (dataProvider.objects(SessionObject.self)?.first) != nil {
            return true
        }
        return false
    }
    
    static func retrieve() -> SessionObject? {
        let dataProvider = DataProvider()
        let session = dataProvider.objects(SessionObject.self)?.first ?? nil
        return session
    }
    
    static func store(session: SessionObject) {
        let dataProvider = DataProvider()
        dataProvider.add(session)
    }
    
    static func destroy(session: SessionObject){
        let dataProvider = DataProvider()
        dataProvider.delete(session)
    }
    
    static func destroy() {
        let dataProvider = DataProvider()
        guard let session = dataProvider.objects(SessionObject.self)?.first ?? nil else { return }
        dataProvider.delete(session)
    }
    
    class func getHeaders() -> [String: String] {            
        let headers = ["Pays-Client": getApplicationVersion(),
                       "PAYS-BUILD": getApplicationBuild(),
                       "Authorization": "Token \(TokenService.accessToken() ?? "")",
                        HTTPHeaderField.contentType.rawValue: ContentType.json.rawValue,
                        HTTPHeaderField.acceptType.rawValue: ContentType.json.rawValue]

        
        return headers as! [String : String]
    }
}
