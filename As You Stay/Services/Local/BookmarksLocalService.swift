//
//  BookmarksLocalService.swift
//  As You Stay
//
//  Created by Max Ward on 13/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation


class BookmarksLocalService {
    
//    static func getIndex() -> Int {
//        let dataProvider = DataProvider()
//        if let lastIndex = dataProvider.objects(HotelDetailsObject.self)?.last?.id {
//            return lastIndex + 1
//        }
//        return 1
//    }
    
//    static func store(favorite: HotelDetailsObject) {
//       let dataProvider = DataProvider()
//       dataProvider.add(favorite, update: false)
//    }
    
    static func store(favorite: HotelObject) {
        let dataProvider = DataProvider()
        dataProvider.add(favorite, update: false)
    }
    
    static func exists (id: String) -> Bool {
        let dataProvider = DataProvider()
        let predicate = NSPredicate(format: "id == %@", id)
        if (dataProvider.objects(HotelObject.self, predicate: predicate)?.first) != nil {
            return true
        } else {
            return false
        }
    }
    
    static func list() -> [HotelObject]?{
        let dataProvider = DataProvider()
        let list = dataProvider.objects(HotelObject.self)
        return list?.toArray(type: HotelObject.self)
    }
    
    static func updateAvailability(id: String, availability: Bool){
        let dataProvider = DataProvider()
        let predicate = NSPredicate(format: "id == %@", id)
        if let hotel = dataProvider.objects(HotelObject.self, predicate: predicate)?.first {
            dataProvider.runTransaction {
                hotel.availability = availability
            }
        }
    }
    
    static func destroy(id: String) {
        let dataProvider = DataProvider()
        let predicate = NSPredicate(format: "id == %@", id)
        guard let hotel = dataProvider.objects(HotelObject.self, predicate: predicate)?.first ?? nil else { return }
        dataProvider.delete(hotel)
    }
    
    static func getFavorite(id: String) -> HotelObject?{
        let dataProvider = DataProvider()
        let predicate = NSPredicate(format: "id == %@", id)
        guard let hotel = dataProvider.objects(HotelObject.self,predicate: predicate)?.first ?? nil else { return nil }
        return hotel
    }
    
}
