//
//  AuthService.swift
//  As You Stay
//
//  Created by Max Ward on 15/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

typealias ApiResponseStatus = (_ status: Bool) -> Void
typealias ApiSessionResponse = (Result<GenericApiResponse<SessionData>>)-> Void
typealias ApiSingupResponse = (Result<GenericApiResponse<SignupData>>)-> Void
typealias ApiForgotPasswordResponse = (Result<GenericApiResponse<ForgotPasswordData>>)-> Void


class AuthService {
    
    fileprivate let genericApiService = GenericApiService()
    
    /**
     Login
     
     - Parameter username: user email
     - Parameter password: password
     - Parameter completion: callback response
    
     ## URL Request: ##
     ````
     POST "https://asyoustay.com/services/client/account/login/
     ````
     ## Response example: ##
     ````
     {
        "status_code": 0,
        "message": null,
        "data": {
            "token": "cde49c5f8864f58545c5f241a21d7374f974c18e",
            "id": 123
        }
     }
     ````
     */
    
    func sigin(username: String, password: String, completion: @escaping ApiSessionResponse){
        
        let request = AuthEndpoint.login(username: username, password: password)
        GenericApiService.webService(req: request) { (response) in
            completion(response)
        }
    }
    
    /**
     Signup
     
     - Parameter firstName: user first name
     - Parameter lastName: user last name
     - Parameter password: user password
     - Parameter email: user email
     - Parameter phone: user phone
     - Parameter signupPromoCode: promo code
     - Parameter deepLink:
     - Parameter marketingSrc:
     - Parameter udid:
     - Parameter googleToken: google token
     - Parameter fbUserId: facebook user id
     - Parameter marketingTitle:
     - Parameter marketingAttrs:
     - Parameter afInstallData:
     - Parameter completion: callback response
    
     ## URL Request: ##
     ````
     POST "https://asyoustay.com/services/client/account/signup/
     ````
     ## Response example: ##
     ````
     {
        "status_code": 0,
        "message": null,
        "data": {
            "token": "cde49c5f8864f58545c5f241a21d7374f974c18e",
        }
     }
     ````
     */
    
    func sigup(firstName: String, lastName: String, password: String, email: String, phone: String, signupPromoCode: String, deepLink: String, marketingSrc: String, udid: String, googleToken: String, fbUserId: String, marketingTitle: String, marketingAttrs: String, afInstallData: String, completion:@escaping ApiSingupResponse){
        
        let request = AuthEndpoint.signup(first_name: firstName,
                                          last_name: lastName,
                                          password: password,
                                          email: email,
                                          phone: phone,
                                          signup_promo_code: signupPromoCode,
                                          deep_link: deepLink,
                                          marketing_src: marketingSrc,
                                          udid: udid,
                                          google_token: googleToken,
                                          fb_user_id: fbUserId,
                                          marketing_title: marketingTitle,
                                          marketing_attrs: marketingAttrs,
                                          af_install_data: afInstallData)
        GenericApiService.webService(req: request) { (response) in
            completion(response)
        }
    }
    
    /**
     After entering with Facebook, you must send the data to the server to finalize sign in
     
     - Parameter fb_user_id: ID assigned by Facebook
     - Parameter email: user email
     - Parameter token: Token assigned by Facebook
     - Parameter completion: callback response
     
     ## URL Request: ##
     ````
     POST https://asyoustay.com/services/client/account/facebook/connect/
     ````
     ## Response example: ##
     ````
     {
     "status_code": 0,
     "message": null,
     "data": {
     "token": "cde49c5f8864f58545c5f241a21d7374f974c18e",
     "id": 123
     }
     }
     ````
     */
    func facebookTokenVerification(fb_user_id:String, email: String, token: String, completion: @escaping ApiSessionResponse){
        let request = AuthEndpoint.facebookTokenVerification(fb_user_id: fb_user_id, email: email, token: token)
        GenericApiService.webService(req: request) { (response) in
            completion(response)
        }
    }
    /**
    After entering with Google, you must send the data to the server to finalize sign in
    
    - Parameter os: Required false, 'android' or 'iphone'
    - Parameter email: user email
    - Parameter token: Token assigned by Google
    - Parameter completion: callback response
    
    ## URL Request: ##
    ````
    POST https://asyoustay.com/services/client/account/google/connect/
    ````
    ## Response example: ##
    ````
    {
    "status_code": 0,
    "message": null,
    "data": {
    "token": "cde49c5f8864f58545c5f241a21d7374f974c18e",
    "id": 123
    }
    }
    ````
    */
    func googleTokenVerification(os:String, email: String, token: String, completion: @escaping ApiSessionResponse){
        let request = AuthEndpoint.googleTokenVerification(os: os, email: email, token: token)
        GenericApiService.webService(req: request) { (response) in
            completion(response)
        }
    }
    
    
    /**
     Forgot password
     
     - Parameter email: user email
     - Parameter completion: callback response
    
     ## URL Request: ##
     ````
     GET https://asyoustay.com/services/client/account/forgetPassword
     ````
     ## Response example: ##
     ````
     {
        "status_code": 0,
        "message": "",
        "data":{}
     }
     ````
     */
    func forgotPassword(email: String, completion: @escaping ApiForgotPasswordResponse) {
        let request = AuthEndpoint.forgotPassword(email: email)
        GenericApiService.webService(req: request) { (response) in
            completion(response)
        }
    }

}
