//
//  ProfileService.swift
//  As You Stay
//
//  Created by Max Ward on 06/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

typealias ApiProfileResponse = (Result<GenericApiResponse<DetailProfile>>)-> Void
typealias ApiDefaultProfileResponse = (Result<ProfileDefaultResponse>)-> Void

class ProfileService {
    
    /**
     Service  get User Profile
     
     ## URL Request: ##
     ````
     GET "https://asyoustay.com/services/client/account/getProfile/"
     ````
    ## Response example: ##
    ````
     {
         "status_code": 0,
         "message": "",
         "data": {
             "upcoming": {
                 "status": "Hotel Declined",
                 "hotel": {
                     "album": [],
                     "policy": [],
                     "display_address": "Times Square - Theatre District",
                     "name": "The Hotel @ Times Square",
                     "lat": "40.757186",
                     "amenities": [],
                     "lng": "-73.981234",
                     "id": "07a40f6d-c796-403f-aa14-5414fb5cff51",
                     "full_address": "59 West 46th Street, New York, NY 10036",
                     "description": "The Hotel at Times ."
                 },
                 "id": 103,
                 "price_usd": 0,
                 "guest": {},
                 "notes": "change me",
                 "ts": "2016-07-06 23:06:40",
                 "booking_id": "4VJGGH5K",
                 "check_in_datetime": "2016-07-06 23:00:00",
                 "check_out_datetime": "2016-07-07 07:00:00",
                 "default_room_image": {}
             },
             "promo": "",
             "reservations": [],
             "keys": {},
             "payments": [],
             "details": {
                 "first_name": "Leonid",
                 "last_name": "Ivanov",
                 "phone": "+1 (646) 808-5942",
                 "country": "United States",
                 "states": "California",
                 "city": "Los Angeles",
                 "formatted_address": "Los Angeles, California, United States",
                 "date_of_birth": "1991-03-15",
                 "account_balance": 0,
                 "phone_verified": true,
                 "referral_counter": 0,
                 "referral_code": "TESTA4174",
                 "city_opening_subscribe": true,
                 "braintree_session": "eyJ2ZXJzaW9uIjoyLCJhd",
                 "total_hrs": 367,
                 "email": "testapp@asyoustay.com"
             }
         }
     }
     ````
    */
   
    func getProfile( completion: @escaping ApiProfileResponse ){
        let urlRequest = AuthEndpoint.getProfile
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
        
    }
    
    
    /**
     Service  update user profile
     
     - Parameter firstName: user first name
     - Parameter lastName: user last name
     - Parameter phone: user phone
     - Parameter country: user country
     - Parameter dateOfBirth: user birth date
     - Parameter city: user city
     - Parameter formattedAddress: user address
     - Parameter completion: callback response
     ## URL Request: ##
     ````
    POST https://asyoustay.com/services/client/account/update/
     ````
    ## Response example: ##
    ````
     {
       "status_code":0,
       "message":"",
       "data":{}
     }
    ````
    */
    
    func updateProfile(firstName: String, lastName: String, phone: String, country: String, state: String, dateOfBirth: String, city: String, formattedAddress: String, completion: @escaping ApiDefaultProfileResponse) {
    
        let urlRequest = ProfileEndpoint.updateProfile(firstName: firstName, lastName: lastName, phone: phone, country: country, state: state, dateOfBirth: dateOfBirth, city: city, formattedAddress: formattedAddress)
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
        
    }
}
