//
//  ProfileEndpoint.swift
//  As You Stay
//
//  Created by Max Ward on 20/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation
import Alamofire

enum ProfileEndpoint: URLRequestConvertible {
    
    case updateProfile (firstName: String, lastName: String, phone: String, country: String, state: String, dateOfBirth: String, city: String, formattedAddress: String)
//    case changePassword(password: String)
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .updateProfile(_, _, _, _, _, _, _, _):
            return .post
//        case .changePassword (_):
//            return .post
        }
    }
    // MARK: - Path
    var path: String {
        switch self {
        case .updateProfile (_):
            return "/account/update/"
//        case .changePassword (_):
//            return "/account/signup/"
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
           
        let url = try K.Server.baseURL.asURL()
           
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
           
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
           
        urlRequest.allHTTPHeaderFields = SessionService.getHeaders()
           
        switch self {
        case .updateProfile(let firstName, let lastName, let phone, let country, let state, let dateOfBirth, let city, let formattedAddress):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["first_name": firstName,
                                                                                "last_name": lastName,
                                                                                "phone": phone,
                                                                                "country": country,
                                                                                "state": state,
                                                                                "date_of_birth": dateOfBirth,
                                                                                "city": city,
                                                                                "formatted_address": formattedAddress])
//        case .changePassword(let password):
//            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["card_id": id])
        }
    }
}
