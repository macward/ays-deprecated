//
//  Hotels.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//
import Foundation
import Alamofire

struct Filter {
    var type: String
    var value: Any
}

enum HotelEndpoint: URLRequestConvertible {
    
    case searchHotelsByLocation(params: [String: Any])
    
    case getHotelDetails(hotel_id: String,
        checkin_datetime: String,
        checkout_datetime: String,
        rooms: [String: Any]
        /*guests: Int*/)
    
    case getRoomPricing( checkin_datetime: String,
        checkout_datetime: String,
        hotel_id: String,
        promo_code: String,
        isBooking: Bool,
        ppn_bundle:String)
    
    case getRoomPricingV3( checkin_datetime: String,
        checkout_datetime: String,
        adults: Int,
        children: Int,
        hotel_id: String,
        promo_code: String,
        isBooking: Bool,
        ppn_bundle:String)
   
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .searchHotelsByLocation(_):
            return .post
        case .getHotelDetails(_, _, _, _):
            return .get
        case .getRoomPricing(_, _, _, _, _, _):
            return .post
        case .getRoomPricingV3(_, _, _, _, _, _, _, _):
            return .post
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .searchHotelsByLocation(_):
            return "/search/locationV2/"
        case .getHotelDetails(_, _, _, _):
            return "/hotels/getHotelDetailsV2"
        case .getRoomPricing(_, _, _, _, _, _):
            return "/bookings/getPricingV2/"
        case .getRoomPricingV3(_, _, _, _, _, _, _, _):
            return "/bookings/getPricingV3/"
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try K.Server.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
//        if SessionService.exists() {
//            urlRequest.allHTTPHeaderFields = SessionService.getHeadersHotelRequest()
//        }
        
        switch self {
            
        case .searchHotelsByLocation(let params):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: params)
            
        case .getHotelDetails(let hotel_id, let checkin_datetime, let checkout_datetime,let rooms):// let guests):
            
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: ["hotel_id": hotel_id,
                                                                                "checkin_datetime": checkin_datetime,
                                                                                "checkout_datetime": checkout_datetime,
                                                                                "rooms": rooms ])
//                                                                                "guests": guests])
            
        case .getRoomPricing(let checkin_datetime, let checkout_datetime, let hotel_id, let promo_code, let isBooking, let ppn_bundle):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["checkin_datetime": checkin_datetime,
                                                                                "checkout_datetime": checkout_datetime,
                                                                                "hotel_id": hotel_id,
                                                                                "promo_code": promo_code,
                                                                                "is_booking": isBooking,
                                                                                "ppn_bundle": ppn_bundle])
        case .getRoomPricingV3(let checkin_datetime, let checkout_datetime, let adults, let children, let hotel_id, let promo_code, let isBooking, let ppn_bundle):
            
            let room: [String: Any] = [
                "adults": adults,
                "children": children
            ]
            
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["checkin_datetime": checkin_datetime,
                                                                                "checkout_datetime": checkout_datetime,
                                                                                "rooms": [room],
                                                                                "hotel_id": hotel_id,
                                                                                "promo_code": promo_code,
                                                                                "is_booking": isBooking,
                                                                                "ppn_bundle": ppn_bundle])
        
        }
    }
}
