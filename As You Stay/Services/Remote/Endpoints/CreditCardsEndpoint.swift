//
//  CreditCardsEndpoint.swift
//  As You Stay
//
//  Created by Max Ward on 06/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation
import Alamofire

enum CreditCardsEndpoint: URLRequestConvertible {
   
    case addCreditCard( dto: CreditCardDTO)
    case remove(id: String)
    case setAsPrimary(id: String)
    case updateLabel(id: String, label: String)
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .addCreditCard (_):
            return .post
        case .remove (_):
            return .post
        case .setAsPrimary (_):
            return .post
        case .updateLabel(_, _):
            return .post
        }
    }
    // MARK: - Path
    var path: String {
        switch self {
            case .addCreditCard (_):
               return "/payments/addCreditCard/"
        case .remove (_):
             return "/payments/removeCreditCard/"
        case .setAsPrimary:
            return "/payments/setCardAsPrimary/"
        case .updateLabel(_, _):
        return "/payments/updateCardLabel/"
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
           
        let url = try K.Server.baseURL.asURL()
           
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
           
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
           
        urlRequest.allHTTPHeaderFields = SessionService.getHeaders()
           
        switch self {
        case .addCreditCard (let dto):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: dto.toDict())
        case .remove(let id):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["card_id": id])
        case .setAsPrimary (let id):
             return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["card_id": id])
        case .updateLabel(let id, let label):
             return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["card_id": id,
                                                                                 "label": label])
        
        }
    }
}
