//
//  AccountEndpoint.swift
//  EMGP
//
//  Created by Max Ward on 14/05/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import Foundation
import Alamofire

enum AuthEndpoint: URLRequestConvertible {
    
    
    case login(username: String, password: String)
    case signup(first_name: String, last_name: String, password: String,
                email: String, phone: String, signup_promo_code: String,
                deep_link: String, marketing_src: String, udid: String,
                google_token: String, fb_user_id: String, marketing_title: String,
                marketing_attrs: String, af_install_data: String)
    case facebookTokenVerification(fb_user_id: String, email: String, token: String)
    case googleTokenVerification(os: String, email: String, token: String)
    case getProfile
    case forgotPassword( email: String)
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .login(_, _):
            return .post
        case .signup(_, _, _, _, _, _, _, _, _, _, _, _, _, _):
            return .post
        case .facebookTokenVerification:
            return .post
        case .googleTokenVerification:
            return .post
        case .getProfile:
            return .get
        case .forgotPassword(_):
            return .get
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .login(_, _):
            return "/account/login/"
        case .signup(_, _, _, _, _, _, _, _, _, _, _, _, _, _):
            return "/account/signup/"
        case .facebookTokenVerification(_, _, _):
            return "/account/facebook/connect/"
        case .googleTokenVerification(_, _, _):
            return "/account/google/connect/"
        case .getProfile:
        return "/account/getProfile/"
        case .forgotPassword(_):
            return "/account/forgetPassword/"
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try K.Server.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.allHTTPHeaderFields = SessionService.getHeaders()
        
        switch self {
        case .login(let username, let password):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["email": username, "password": password])
        case .signup(let first_name, let last_name, let password, let email, let phone, let signup_promo_code, let deep_link, let marketing_src, let udid, let google_token, let fb_user_id, let marketing_title, let marketing_attrs, let af_install_data):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["email": email,
                                                                                "first_name": first_name,
                                                                                "last_name": last_name,
                                                                                "marketing_src": marketing_src,
                                                                                "marketing_title": marketing_title,
                                                                                "password": password,
                                                                                "phone": phone,
                                                                                "signup_promo_code": signup_promo_code,
                                                                                "udid": udid,
                                                                                "marketing_attrs": marketing_attrs,
                                                                                "af_install_data": af_install_data,
                                                                                "fb_user_id": fb_user_id,
                                                                                "google_token": google_token,
                                                                                "deep_link": deep_link
                                                                                ])
        case .facebookTokenVerification(let fb_user_id, let email, let token):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["fb_user_id": fb_user_id, "email":email, "token":token])
        case .googleTokenVerification(let os, let email, let token):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: ["os": os, "email":email, "token":token])
        case .getProfile:
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: nil)
        case .forgotPassword(let email):
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: ["email": email])
            
        }
    }
}
