//
//  BokingNDP.swift
//  As You Stay
//
//  Created by Max Ward on 11/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import Alamofire

enum BookingEndpoint: URLRequestConvertible {
   
    case submitBooking(bookingDTO: BookingDTO)
    case getReceipt(bookingID: String)
    case getUpcomingReservations
    case validatePromo(promoCode: String)


    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .submitBooking(_):
            return .post
        case .getReceipt(_):
            return .get
        case .getUpcomingReservations:
            return .get
        case .validatePromo(_):
            return .get
        }
    }
    // MARK: - Path
    var path: String {
        switch self {
            case .submitBooking(_):
               return "/bookings/submitBookingV2/"
            case .getReceipt(_):
                return "/bookings/getReceipt/"
            case .getUpcomingReservations:
                return "bookings/getUpcomingReservations/"
            case .validatePromo(_):
                return "/bookings/validatePromo/"

        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
           
       let url = try K.Server.baseURL.asURL()
           
       var urlRequest = URLRequest(url: url.appendingPathComponent(path))
           
       // HTTP Method
       urlRequest.httpMethod = method.rawValue
           
       urlRequest.allHTTPHeaderFields = SessionService.getHeaders()
           
       switch self {
            case .submitBooking(let bookingDTO ):
                return try Alamofire.JSONEncoding.default.encode(urlRequest, with: bookingDTO.toDict())
            case . getReceipt(let bookingID):
                return try Alamofire.URLEncoding.default.encode(urlRequest, with: ["booking_id": bookingID])
            case .getUpcomingReservations:
                return try Alamofire.URLEncoding.default.encode(urlRequest, with: nil)
            case .validatePromo(let promoCode):
                return try Alamofire.URLEncoding.default.encode(urlRequest, with: ["promo": promoCode])
           }
       }
}
