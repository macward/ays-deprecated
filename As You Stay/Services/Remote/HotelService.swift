//
//  HotelService.swift
//  As You Stay
//
//  Created by Max Ward on 8/6/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

typealias ApiHotelDetailsResponse = (Result<GenericApiResponse<HotelDetailsData>>)-> Void
typealias ApiRoomPricingV3Response = (Result<GenericApiResponse<PricingV3Data>>)-> Void
typealias ApiSearchHotelByLocationResponse = (Result<GenericApiResponse<HotelData>>)-> Void

class HotelService {

    
    func getImageHotel(url: String, completion :@escaping ((_ imageHotel : UIImage)->Void) ){
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completion(image)
            }//falta completar con error
        }
    }
    
    ///
    ///SEARCH HOTEL BY LOCATION V2
    ///
    
    func searchHotelByLocation(params: [String: Any], completion: @escaping (ApiSearchHotelByLocationResponse)){
        
        let url = HotelEndpoint.searchHotelsByLocation(params: params)
        GenericApiService.webService(req: url) { (response) in
            completion(response)
        }
    }
    
    /**
     Service hotel details
     
     - Parameter hotel_id: Hotel and city id
     - Parameter checkin_datetime: Check-In datetime (format: "2019-09-20 17:00:00")
     - Parameter checkout_datetime: Check-Out datetime (format: "2019-09-23 14:00:00")
     - Parameter guets: Number of guests (default: 2)
    
     ## URL Request: ##
     ````
     GET https://asyoustay.com/services/client/hotels/getHotelDetailsV2/
     ````
     ## Response example: ##
     ````
     {
       "status_code": 0,
       "message": "",
       "data": {
         "details": {
           "id": "pl_700114268",
           "name": "Fairfield Inn By Marriott East Rutherford Meadowlands",
           "full_address": "850 Route 120, East Rutherford, US",
           "display_address": "850 Route 120",
           "lat": "40.82969002",
           "lng": "-74.0837308",
           "description": "The freebies include Wi-Fi and parking at the non-smoking Fairfield...",
           "room_type": "Standard Room",
           "ppn_bundle": "HER_yVR2lfnwnKDL-...",
           "star_rating": 2.5,
           "album": [
             {
               "url": "https://q-xx.bstatic.com/xdata/images/hotel/max1...",
               "cover": false,
               "id": 6,
               "description": "Fairfield Inn By Marriott East Rutherford Meadowlands Photo #6"
             }
           ],
           "amenities": [
             {
               "price_usd": 0,
               "name": "Free Breakfast",
               "is_display_free": false,
               "is_cost_extra": false,
               "is_available": true,
               "icon": "https://d1d0bgvqdg71ke.cloudfront.net/images/amenities/mvp/icon-bellman-cart.png"
             }
           ],
           "policy": [
             "- Your purchase is non-refundable.",
             "- Guests must check-out no later than the specified check-out time to avoid additional charges.",
           ]
         },
         "reviews": {
           "reviews": [
             {
               "date": "2019-07-31",
               "positive": "Staff was professional and caring.",
               "user_name": "Anonymous",
               "negative": "No",
               "rating": "9"
             }
           ],
           "reviews_count": "11",
           "reviews_rating": 9.4
         },
       }
     }
     ````
     */
    
    func getHotelDetails(hotel_id:String, checkin_datetime: String, checkout_datetime: String, rooms: [String: Any], completion: @escaping ApiHotelDetailsResponse ){

        let request = HotelEndpoint.getHotelDetails(hotel_id: hotel_id, checkin_datetime: checkin_datetime, checkout_datetime: checkout_datetime, rooms: rooms)

        GenericApiService.webService(req: request) { (response) in
            completion(response)
        }
    }
   
    /**
     Service  Room Pricing
     
     - Parameter checkin_datetime: Check-In datetime (format: "2019-09-20 17:00:00")
     - Parameter checkout_datetime: Check-Out datetime (format: "2019-09-23 14:00:00")
     - Parameter hotel_id: Hotel ID
     - Parameter promo_code: Promo code
     - Parameter ppn_bundle: Bundle from getHotelDetails
    
     ## URL Request: ##
     ````
     POST https://asyoustay.com/services/client/bookings/getPricingV2/
     ````
    ## Response example: ##
    ````
     {
        "status_code": 0,
        "message": "",
        "data": {
            "pricing": {
                "avg_market_price": 377.0,
                "avg_member_savings": 63.0,
                "member_savings_perc": 17.0,
                "avg_night_price": 314.0,
                "market_price": 1131.0,
                "member_savings": 189.0,
                "early_checkin_late_checkout_fees": 156.0,
                "booking_subtotal": 1098.0,
                "taxes_total": 200.37,
                "property_fee": 0,
                "booking_total": 1298.37,
                "savings_deviation": -3,
                "fees_deviation": 3,
                "nights_amount": 3,
                "duration": 58,
                "promo_discounts": 0,
                "booking_policy": "All bookings are non-refundable.",
        },
        "address": "151 Maiden Lane, New York City, New York, United States",
        "ppn_bundle": "HEC_irdmWXQitFw7OEAZV0Gs26SfZC-GvfkdotFnam13DQKo-_dNaygXJ5hSG6TaTrX....",
        "property_fee_details": null,
        "taxes_and_fees_info": [
             "Please note that we are unable to facilitate...",
             "Depending on the property you stay at you may also..."
         ],
         "important_info": [
             "All rooms will accommodate up to 2 people...."
         ],
     }
     ````
    */
    
    func getRoomPricingV3(checkin_datetime: String,
                          checkout_datetime: String,
                          adults: Int,
                          children: Int,
                          hotel_id: String,
                          is_booking: Bool,
                          promo_code: String,
                          ppn_bundle: String,
                          
                          completion: @escaping ApiRoomPricingV3Response ){
        
        let urlRequest  = HotelEndpoint.getRoomPricingV3(checkin_datetime: checkin_datetime, checkout_datetime: checkout_datetime, adults: adults, children: children, hotel_id: hotel_id, promo_code: promo_code, isBooking: is_booking, ppn_bundle: ppn_bundle)
        
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
    }
}
