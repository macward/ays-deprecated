//
//  CreditCardService.swift
//  As You Stay
//
//  Created by Max Ward on 06/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

typealias ApiAddCreditCardResponse = (Result<GenericApiResponse<CreditCardData>>)-> Void
typealias ApiCreditCardResponse = (Result<GenericApiResponse<CreditCardData>>)-> Void

class CreditCardService {
    
    /**
     Service add new credit card
         
         ## URL Request: ##
         ````
         POST https://asyoustay.com/services/client/payments/addCreditCard/
         
         ````
     ## Response example: ##
     ````
     {
        {
            "status_code": 0,
            "message": "",
            "data": {}
        }
     }
     ````
     */
    
    func addCreditCard(card: CreditCardDTO,completion: @escaping ApiAddCreditCardResponse){
        let urlRequest = CreditCardsEndpoint.addCreditCard(dto: card)
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
    }
    
    /**
    Service  set credit card as primary
        
  - Parameter id: credit card id
    ## URL Request: ##
    ````
    POST https://asyoustay.com/services/client/payments/setCardAsPrimary/
    ````
    ## Response example: ##
    ````
    {
       {
           "status_code": 0,
           "message": "",
           "data": {}
       }
    }
    ````
    */
    func setAsPrimary(identification: String, completion: @escaping ApiCreditCardResponse) {
        let urlRequest = CreditCardsEndpoint.setAsPrimary(id: identification)
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
    }
    
    /**
    Service  remove credit card
    - Parameter id: credit card id
    ## URL Request: ##
    ````
    POST https://asyoustay.com/services/client/payments/removeCreditCard/
    
    ````
    ## Response example: ##
    ````
    {
       {
           "status_code": 0,
           "message": "",
           "data": {}
       }
    }
    ````
    */
    func remove(id: String, completion: @escaping ApiCreditCardResponse) {
        let urlRequest = CreditCardsEndpoint.remove(id: id)
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
    }
    
    /**
    Service update credit card label
    - Parameter id: credit card id
    - Parameter label: new label to update
    ## URL Request: ##
    ````
    POST https://asyoustay.com/services/client/payments/updateCardLabel/
    
    ````
    ## Response example: ##
    ````
    {
       {
           "status_code": 0,
           "message": "",
           "data": {}
       }
    }
    ````
    */
    func updateLabel(id: String,label: String, completion: @escaping ApiCreditCardResponse) {
        let urlRequest = CreditCardsEndpoint.updateLabel(id: id, label: label)
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
    }
    
}
