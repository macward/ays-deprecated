//
//  BookingApiService.swift
//  As You Stay
//
//  Created by Max Ward on 12/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

typealias ApiBookingListResponse = (Result<BookingListResponse>)-> Void

class BookingApiService {
    
    /**
        Service  get list of my bookings
        
        ## URL Request: ##
        ````
        GET https://asyoustay.com/services/client/bookings/getList
        
        ````
    */
   
    func getList( completion: @escaping ApiBookingListResponse ){
        
//        let urlRequest = BookingEndPoint.getList
//        
//        GenericApiService.webService(req: urlRequest) { (result) in
//            completion(result)
//        }
    }
    
//    func getList(){
//        if let url = URL(string: "https://asyoustay.com/services/client/bookings/getList") {
//          var request = URLRequest(url: url)
//          // Set headers
//          request.setValue("Pays-Client", forHTTPHeaderField: "desktop-0.1")
//          request.setValue("Authorization", forHTTPHeaderField: "Authorization")
//            request.setValue("Content-type", forHTTPHeaderField: "application/json")
//         
//            URLSession.shared.dataTask(with: request, completionHandler: {(data, response, err) in
//                print(data)
//            }).resume()
//        } else {
//          // Something went wrong
//        }
//    }
}
