//
//  File.swift
//  EMGP
//
//  Created by Max Ward on 10/06/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire
import AlamofireLogging

protocol GeneriApiProtocol {
    static func webService<T>(req: URLRequestConvertible, completion: @escaping (Result<T>) -> Void) where T: Codable
}

class GenericApiService: GeneriApiProtocol {
    
    static func webService<T: Codable>(req: URLRequestConvertible, completion: @escaping (Result<T>) -> Void) {
        Alamofire.request(req).log(level: .verbose).responseDecodableObject{ (response: DataResponse<T>) in
            completion(response.result)
        }
    }
    static func web_Service<T: Codable>(req: URLRequestConvertible, completion: @escaping (DataResponse<T>) -> Void) {
           Alamofire.request(req).log(level: .verbose).responseDecodableObject{ (response: DataResponse<T>) in
            
               completion(response)
           }
       }
    
    static func singleWebService(req: URLRequestConvertible, completion: @escaping (Result<Any>) -> Void) {
        Alamofire.request(req).responseJSON { (response) in
            completion(response.result)
        }
    }
    
}
