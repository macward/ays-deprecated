//
//  BookingService.swift
//  As You Stay
//
//  Created by Max Ward on 08/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import CodableAlamofire
import Alamofire

typealias ApiValidatePromoCode = (Result<GenericApiResponse<Validation>>)-> Void
typealias ApiSubmitBookingResponse = (Result<GenericApiResponse<BookingData>>)-> Void
typealias ApiBookingReceiptResponse = (Result<GenericApiResponse<ReceiptObject>>)-> Void
typealias ApiBookingUpcomingReservationsResponse = (Result<GenericApiResponse<ReceiptObjectList>>)-> Void

class BookingService {
    
    /**
     Service  submit booking
     
     - Parameter booking: booking dto object
     
     ## URL Request: ##
     ````
    POST https://asyoustay.com/services/client/bookings/submitBookingV2/
     ````
    ## Response example: ##
    ````
     {
        "status_code": 0,
        "message": null,
        "data": {
            "booking_id": "AYS-C5ESEUS8",
            "total_hours": 24
        }
     }
     ````
    */
    
    func submitBooking(booking: BookingDTO, completion: @escaping ApiSubmitBookingResponse ){
        let urlRequest = BookingEndpoint.submitBooking(bookingDTO: booking)
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
    }
    
    
    /**
     Service  get Receipt
     
     - Parameter booking_id: booking id
     
     ## URL Request: ##
     ````
    GET https://asyoustay.com/services/client/bookings/getReceipt
     ````
    ## Response example: ##
    ````
     {
         "status_code": 0,
         "message": "",
         "data": {
             "hotel": {
                 "name": "InterContinental NEW YORK BARCLAY - Newly Renovated",
                 "address": "111 East 48th Street, New York City, New York, United States",
                 "notes": "Check these notes\rWow!",
                 "phone": "212-755-5900",
                 "confirmation_code": "CONF0"
             },
             "guest": {
                 "name": "Chen Demos",
                 "email": "testapp@asyoustay.com",
                 "phone": "+54 92233023291"
             },
             "booking": {
                 "id": "AYS-3L7UJ6XH",
                 "check_in": "2019-12-28 15:00:00",
                 "check_out": "2019-12-30 11:00:00",
                 "duration_hrs": 1, "status_label_1": "Your room is booked!",
                 "status_label_2": "Confirmation: #AYS-3L7UJ6XH",
                 "status": 2
             },
             "pricing": {
                 "room_price": 888.0,
                 "member_savings": 38.0,
                 "check_in_deviation": 0.0,
                 "check_in_price": 0.0,
                 "check_out_deviation": 0.0,
                 "check_out_price": 0.0,
                 "tax": 139.56,
                 "credit": 0.0,
                 "promo_amount": 0.0,
                 "promo_code": "",
                 "hotel_fees": 68.86,
                 "total": 989.56,
                 "cc_label": "Business",
                 "cc_type": "MasterCard",
                 "cc_last4": "4444"
             }
         }
     }
     ````
    */
    
    func getReceipt(id: String, completion: @escaping ApiBookingReceiptResponse){
        let urlRequest = BookingEndpoint.getReceipt(bookingID: id)
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
    }
    
    func getUpcomingReservations(completion: @escaping ApiBookingUpcomingReservationsResponse) {
        let urlRequest = BookingEndpoint.getUpcomingReservations
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
    }
    
    func validatePromoCodde(promoCode: String, completion: @escaping ApiValidatePromoCode) {
        let urlRequest = BookingEndpoint.validatePromo(promoCode: promoCode)
        GenericApiService.webService(req: urlRequest) { (response) in
            completion(response)
        }
    }
}
