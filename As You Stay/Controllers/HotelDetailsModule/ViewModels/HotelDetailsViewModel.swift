//
//  HotelDetailsViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 04/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation


class HotelDetailsViewModel {
    
    var pricingV3Data: PricingV3Data?
    var hotelDetail: HotelDetails?
    var selectedGuest: GuestSearchDTO?
    
    func getRoomPricingV3(checkin_datetime: String,
                          checkout_datetime: String,
                          adults: Int,
                          children: Int,
                          hotel_id: String,
                          promo_code: String,
                          is_booking: Bool,
                          ppn_bundle: String,
                          completion: @escaping(PricingV3Data?, String?)-> Void ){
        
        let hotelService = HotelService()
        hotelService.getRoomPricingV3(checkin_datetime: checkin_datetime,
                                      checkout_datetime: checkout_datetime,
                                      adults: adults, children: children,
                                      hotel_id: hotel_id,
                                      is_booking: is_booking,
                                      promo_code: promo_code,
                                      ppn_bundle: ppn_bundle) { (result) in
            
            switch result {
            case .success(let value):
                if value.statusCode == 0 {
                    self.pricingV3Data = value.data
                    completion(value.data, value.message)
                } else {
                    completion(nil, value.message)
                }
            case .failure(let err):
                completion(nil, err.localizedDescription)
            }
        }
    }
}
