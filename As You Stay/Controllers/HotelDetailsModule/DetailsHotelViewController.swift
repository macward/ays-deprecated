//
//  DetailsHotelViewController.swift
//  As You Stay
//
//  Created by Zibra Coders on 8/13/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class DetailsHotelViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var priceView: UIView!
    
    var coordinator: HotelCoordinator?
    
    var detailsHotel: HotelResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "HotelNameTableViewCell", bundle: nil), forCellReuseIdentifier: "HotelNameCell")
        tableView.register(ImageHotelTableViewCell.self, forCellReuseIdentifier: "ImageHotelCell")
//        tableView.register(HotelNameTableViewCell.self, forCellReuseIdentifier: "HotelNameCell")
        tableView.register(AboutThisHotelTableViewCell.self, forCellReuseIdentifier: "AboutThisCell")
        tableView.register(AddressTableViewCell.self, forCellReuseIdentifier: "AddressCell")
        tableView.register(PolicyTableViewCell.self, forCellReuseIdentifier: "PolicyCell")
        tableView.delegate = self
        tableView.dataSource = self
        
     }
}
extension DetailsHotelViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let nameCell = tableView.dequeueReusableCell(withIdentifier: "HotelNameCell") as! HotelNameTableViewCell
        //nameCell.textLabel?.text = "asdas"
//        if let row = detailsHotel {
//            nameCell.setupRow(hotel: row)
//        }
        return nameCell
        
//        switch indexPath.row {
//        case 0:
//            let imageCell = tableView.dequeueReusableCell(withIdentifier: "ImageHotelCell") as! ImageHotelTableViewCell
//            return imageCell
//        case 1:
//            let nameCell = tableView.dequeueReusableCell(withIdentifier: "HotelNameCell") as! HotelNameTableViewCell
//            return nameCell
//        case 2:
//            let aboutCell = tableView.dequeueReusableCell(withIdentifier: "AboutThisCell") as! AboutThisHotelTableViewCell
//            return aboutCell
//        case 3:
//            let addressCell = tableView.dequeueReusableCell(withIdentifier: "AddressCell") as! AddressTableViewCell
//            return addressCell
//        case 4:
//            let policyCell = tableView.dequeueReusableCell(withIdentifier: "PolicyCell") as! PolicyTableViewCell
//            return policyCell
//        default:
//            let cell = UITableViewCell()
//            cell.textLabel?.text = "cell"
//            return cell
//        }
    }
    
    
}
