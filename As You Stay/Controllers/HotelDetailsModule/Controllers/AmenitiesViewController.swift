//
//  AmenitiesViewController.swift
//  As You Stay
//
//  Created by Max Ward on 9/2/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import Alamofire

class AmenitiesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    weak var coordinator: HotelCoordinator?
    var amenities: [Amenities]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        setupTableView()
        title = "Amenities"
        addCancelNavigationButton()
    }
    func setupTableView() {
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1)
        tableView.register(UINib(nibName: "DetailAmenitiesTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailAmenityCell")
    }
    
    fileprivate func addCancelNavigationButton(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back-arrow"),
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(didTapCancel))
    }
    
    @objc fileprivate func didTapCancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true)
    }
    
    @objc func didTapCloseButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AmenitiesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return amenities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailAmenityCell") as! DetailAmenitiesTableViewCell
        cell.setRow( amenity: amenities?[indexPath.row].name ?? "amenity")
        return cell
    }
    
    
}
