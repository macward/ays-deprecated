//
//  DetailHotelTestViewController.swift
//  As You Stay
//
//  Created by Max Ward on 8/13/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class HotelDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabelContainer: UIView!
    @IBOutlet weak var priceLabel: UILabel! {
        didSet {
            priceLabel.isHidden = false
        }
    }
    @IBOutlet weak var continueButton: UIButton!
    
    
    var activityIndicator = UIActivityIndicatorView(frame: .zero)
    var detailsHotel: HotelDetails?
    var policyCellExpanded: Bool = false
    var viewModel = HotelDetailsViewModel()
    var bookmarksViewModel = BookmarksViewModel()
    var kTableHeaderHeight:CGFloat = 300.0
    var headerView = HotelDetailsHeader()
    
    weak var coordinator: HotelCoordinator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
                
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1)
        priceView.backgroundColor = UIColor(red: 51/256, green: 56/256, blue: 63/256, alpha: 1)
        
        continueButton.cornerRadius = 5.0
        
        tableView.register(UINib(nibName: "HotelNameTableViewCell", bundle: nil), forCellReuseIdentifier: "HotelNameCell")
        tableView.register(UINib(nibName: "AboutThisHotelTableViewCell", bundle: nil), forCellReuseIdentifier: "AboutThisCell")
        tableView.register(UINib(nibName: "AmenitiesTableViewCell", bundle: nil), forCellReuseIdentifier: "AmenitiesCell")
        tableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "AddressCell")
        tableView.register(UINib(nibName: "PolicyTableViewCell", bundle: nil), forCellReuseIdentifier: "PolicyCell")
        tableView.register(UINib(nibName: "DescriptionPolicyTableViewCell", bundle: nil), forCellReuseIdentifier: "DescriptionPolicyCell")
        
        //setupPricing()
        setupPricingV3()
        
        setupHeader()
    }
    
    override func viewWillLayoutSubviews() {
        updateHeaderView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scrollViewDidScroll(tableView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if self.isMovingFromParent {
            if SessionService.exists() {
                let data:[String: Int] = ["index": 0]
                let notificationName = Notification.Name(rawValue: K.NotificationKeys.refreshMainTabBar)
                NotificationCenter.default.post(name:  notificationName, object: nil, userInfo: data)
            }
        }
    }
    
    func setupHeader(){
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 300)
        headerView.backgroundColor = .clear
        guard let itemDetail = viewModel.hotelDetail else { return  }
        headerView.configure(with: itemDetail, parent: self)
        tableView.addSubview(headerView)
        
        tableView.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
        updateHeaderView()
        
        if hidesBottomBarWhenPushed {
        
            let color =  UIColor.init(red: 1, green: 1, blue: 1, alpha: 0)
            let statusBarView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width + 44, height: self.statusBarHeight()))
            statusBarView.tag = 100
            statusBarView.backgroundColor = color
            statusBarView.alpha = 1
            self.navigationController?.view.addSubview(statusBarView)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
        
        let offset = scrollView.contentOffset.y * -1
        // Slow scroll
        if offset < 191 && offset > 179 {
            let alpha = (10 - (offset - 180)) / 10
            let color =  UIColor(red: 0.1254901961, green: 0.137254902, blue: 0.1647058824, alpha: alpha)
            if let views =  self.navigationController?.view.subviews{
                for view in views {
                    if view.tag == 100{
                        view.backgroundColor = color
                        self.navigationController?.navigationBar.backgroundColor = color
                    }
                }
            }
        }
        // Show
        if offset < 180 {
            UIView.animate(withDuration: 0.2) {
                let color =  UIColor(red: 0.1254901961, green: 0.137254902, blue: 0.1647058824, alpha: 1)
                if let views =  self.navigationController?.view.subviews{
                    for view in views {
                        if view.tag == 100{
                            view.backgroundColor = color
                            self.navigationController?.navigationBar.backgroundColor = color
                        }
                    }
                }
            }
        }
        if offset < 165 {
            UIView.animate(withDuration: 1.2) {
                let label = UILabel()
                label.backgroundColor = .clear
                label.numberOfLines = 2
                label.font = UIFont(name: "Montserrat-Regular", size: 16.0)!
                label.textAlignment = .center
                label.textColor = .white
                label.text = self.viewModel.hotelDetail?.name
                self.navigationItem.titleView = label
            }
        }
        if offset > 165 {
            UIView.animate(withDuration: 1.2) {
                self.navigationItem.titleView = UILabel()
            }
        }
        // Hide
        if offset > 191 {
            UIView.animate(withDuration: 0.2, animations: {
                if let views =  self.navigationController?.view.subviews{
                    for view in views {
                        if view.tag == 100{
                            let color = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0)
                            self.navigationController?.navigationBar.backgroundColor = color
                            view.backgroundColor = color
                        }
                    }
                }
                
            })
        }
    }
    
    func statusBarHeight() -> CGFloat {
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        return Swift.min(statusBarSize.width, statusBarSize.height)
    }
    
    func updateHeaderView() {
        
        var headerRect = CGRect(x: 0, y: -kTableHeaderHeight, width: tableView.bounds.width, height: kTableHeaderHeight)
        if tableView.contentOffset.y < -kTableHeaderHeight {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
        }
        
        headerView.frame = headerRect
    }
    
    func setupPricingV3(){
    // edit viewmodel.pricinvV3data
        priceLabelContainer.addSubview(activityIndicator)
        activityIndicator.color = .white
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: priceLabelContainer.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: priceLabelContainer.centerXAnchor).isActive = true
        activityIndicator.startAnimating()
        continueButton.isEnabled = false
        priceLabel.text = ""
        
        guard let checkin  = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)?.getFormatToRequest() else { return }
        guard let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)?.getFormatToRequest() else { return }
        
        viewModel.getRoomPricingV3(checkin_datetime: checkin,
                                   checkout_datetime: checkout,
                                   adults: viewModel.selectedGuest?.adults ?? 2,
                                   children: viewModel.selectedGuest?.children ?? 0,
                                   hotel_id: viewModel.hotelDetail?.id ?? "",
                                   promo_code: "",
                                   is_booking: false,
                                   ppn_bundle: /*viewModel.hotelDetail?.ppn_bundle ?? */"") { (pricingData, message) in
            if pricingData != nil {
                
                self.priceLabel.text = "CHOOSE ROOM"
                self.continueButton.isHidden = false
                self.continueButton.isEnabled = true
            } else {
                self.priceLabel.text = " -- "
                self.continueButton.isHidden = true
                self.continueButton.isEnabled = false
                self.present(simpleAlert(title: "Error", message: message ?? ""), animated: true)
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    func setupNavigationBar(){
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "back-arrow"), for: .normal)
        backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        // add to favorites
        setupNavigationBarRightButton(id: viewModel.hotelDetail?.id ?? "")
    }
    
    func setupNavigationBarRightButton(id: String) {
        let rightButton = UIButton(type: .custom)
        let bookmarkIcon = bookmarksViewModel.switchBookmarkIcon(id: id)

        rightButton.setImage(bookmarkIcon, for: .normal)
        rightButton.addTarget(self, action: #selector(didTapAddToFavorites), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc fileprivate func didTapBackButton(_ sender: UIBarButtonItem){
        let color = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0)
        self.navigationController?.navigationBar.backgroundColor = color
        if let views =  self.navigationController?.view.subviews{
            for view in views {
                if view.tag == 100{
                    view.removeFromSuperview()
                }
            }
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func didTapAddToFavorites(_ sender: UIBarButtonItem){
        
        if let hotel = viewModel.hotelDetail {
            if bookmarksViewModel.isFavorites(id: hotel.id){
                
                let alertController = customAlert(title: "Warning", message: "Do you want to remove from favorites?", firstTitleAction: "Remove", secondTitleAction: "Cancel", firstActionHandler: { (UIAlertAction) in
                    
                    self.bookmarksViewModel.destroy(id: hotel.id)
                    self.setupNavigationBarRightButton(id: hotel.id)
                    
                }) { (UIAlertAction) in
                    print("cancel remove hotel to the favorites")
                }

                self.present(alertController, animated: true, completion: nil)
                
            } else {
                bookmarksViewModel.store(hotelDetails: hotel)
                self.setupNavigationBarRightButton(id: hotel.id)
                
            }
        }
       
    }
    
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        priceLabel.textColor = UIColor.white
        priceLabel.numberOfLines = 1
        priceLabel.adjustsFontSizeToFitWidth = true
    
    }
    
    @IBAction func didTapContinueButton(_ sender: Any) {
        coordinator.roomsV3(data: viewModel.pricingV3Data, hotel: viewModel.hotelDetail, guests: viewModel.selectedGuest)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension HotelDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let itemDetail = viewModel.hotelDetail else { return UITableViewCell() }
        
        switch indexPath.row {
        case 0:
            let nameCell = tableView.dequeueReusableCell(withIdentifier: "HotelNameCell") as! HotelNameTableViewCell
           nameCell.configure(with: itemDetail)
            return nameCell
        case 1:
            let aboutCell = tableView.dequeueReusableCell(withIdentifier: "AboutThisCell") as! AboutThisHotelTableViewCell
            aboutCell.setupRow(detail: itemDetail)
            return aboutCell
        case 2:
            let amenitiesCell = tableView.dequeueReusableCell(withIdentifier: "AmenitiesCell") as! AmenitiesTableViewCell
            if let image = UIImage(named: "chevron-right") {
                let checkmark  = UIImageView(frame:CGRect(x:0, y:0, width:(image.size.width), height:(image.size.height)));
                checkmark.image = image
                amenitiesCell.accessoryView = checkmark
            }
            
            return amenitiesCell
        case 3:
            let addressCell = tableView.dequeueReusableCell(withIdentifier: "AddressCell") as! AddressTableViewCell
            addressCell.configure(with: itemDetail)
            return addressCell
        case 4:
            let policyCell = tableView.dequeueReusableCell(withIdentifier: "PolicyCell") as! PolicyTableViewCell
            return policyCell
        default:
            let cell = UITableViewCell()
            cell.textLabel?.text = "cell"
      
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        tableView.cellForRow(at: indexPath)?.selectedBackgroundView?.backgroundColor = .clear
        tableView.cellForRow(at: indexPath)?.selectionStyle = .none
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            let aboutCell = tableView.cellForRow(at: indexPath) as! AboutThisHotelTableViewCell
            aboutCell.makeToggle()
            tableView.reloadData()
        }
        if indexPath.row == 2 {
            let viewController = AmenitiesViewController()
            viewController.amenities = viewModel.hotelDetail?.amenities
            let navController = UINavigationController(rootViewController: viewController)
            self.present(navController, animated: true, completion: nil)
        }
        if indexPath.row == 4 {
            guard let itemDetail = viewModel.hotelDetail else { return }
            let policyCell = tableView.cellForRow(at: indexPath) as! PolicyTableViewCell
            policyCell.setRow(policy: itemDetail)
            tableView.reloadData()
            tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
}
