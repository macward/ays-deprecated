//
//  ProfileViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 06/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class ProfileViewModel {
    
    func store(completion: @escaping (Bool, String?) ->Void ) {
        let profileService = ProfileService()
        profileService.getProfile { (result) in
            switch result {
            case .success(let value):
                if value.statusCode == 0 {
                    if let data = value.data {
                        let profile = data.detailProfileObject
                        let profileObject = self.getProfileObject(profile: profile)
                        
                        let creditCards = data.payments
                        let creditCardsList = self.getCreditCardList(list: creditCards)
                        
                        if ProfileLocalService.exists() {
                            if let localProfile = ProfileLocalService.getProfile(){
                                //por si hay cambio de usuario
                                if localProfile.email != profile.email{
                                    
                                    CreditCardLocalService.destroy()
                                    self.storeCreditCards(list: creditCardsList)
                                    
                                    ProfileLocalService.destroy()
                                    
                                }
                            }
                            ProfileLocalService.store(profile: profileObject)
                            APITexts.loadKeys(data.keys)
                            completion(true, value.message)
                        } else {
                            self.storeCreditCards(list: creditCardsList)
                            ProfileLocalService.store(profile: profileObject)
                            completion(true, value.message)
                        }
                        
                    }
                } else {
                    completion(false, value.message)
                }
                
            case .failure( let err):
                completion(false, err.localizedDescription)
//                print(err)
                
            }
        }
    }
    
    func updateLoyalty(substract ammount: Float) {
        ProfileLocalService.updateLoyalty(substract: ammount)
    }
    
    func updateLoyalty(add ammount: Float) {
        ProfileLocalService.updateLoyalty(add: ammount)
    }
    
    func getLocalProfile() -> ProfileObject? {
        return ProfileLocalService.getProfile()
    }
    
    func getCardList() -> [CreditCardObject]? {
        return CreditCardLocalService.list()
    }
    
    private func getProfileObject(profile: DetailProfileObject) -> ProfileObject {
        
        let profileObject = ProfileObject()
        profileObject.firstName = profile.firstName
        profileObject.lastName = profile.lastName
        profileObject.accountBalance = profile.accountBalance
        profileObject.braintreeSession = profile.braintreeSession
        profileObject.city = profile.city ?? ""
        profileObject.cityOpeningSubscribe = profile.cityOpeningSubscribe
        profileObject.country = profile.country ?? ""
        profileObject.dateOfBirth = profile.dateOfBirth ?? ""
        profileObject.email = profile.email
        profileObject.formattedAddress = profile.formattedAddress ?? ""
        profileObject.phone = profile.phone
        profileObject.phoneVerified = profile.phoneVerified
        profileObject.referralCode = profile.referralCode
        profileObject.referralCounter = profile.referralCounter
        profileObject.states = profile.states ?? ""
        profileObject.totalHrs = profile.totalHrs
        
        return profileObject
    }
    
    private func getCreditCardList(list: [CreditCard]) -> [CreditCardObject]  {
        
        var creditCardsList: [CreditCardObject] = []
        for item in list {
            let aux = CreditCardObject()
            aux.expMm = item.expMm
            aux.expYy = item.expYy
            aux.holder = item.holder
            aux.identification = item.identification
            aux.isPrimary = item.isPrimary
            aux.label = item.label
            aux.last4 = item.last4
            aux.type = item.type
            aux.zipcode = item.zipcode
            creditCardsList.append(aux)
        }
        return creditCardsList
    }
    
    private func storeCreditCards(list: [CreditCardObject]){
        for item in list {
            //item.id = CreditCardLocalService.getIndex()
            CreditCardLocalService.store(creditCard: item)
        }
    }
    
    func updateProfile(firstName:String, lastName:String, phone: String, completion: @escaping (Bool) -> Void) {
        
        guard getLocalProfile() != nil else {
            completion(false)
            return
        }
        let profileService = ProfileService()
        profileService.updateProfile(firstName: firstName, lastName: lastName, phone: phone, country: "", state: "", dateOfBirth: "", city: "", formattedAddress: "") { (result) in
            switch result {
            case .success(_):
                print("Success profile updated!")
                ProfileLocalService.update(firstName: firstName, lastName: lastName, phone: phone)
                completion(true)
            case .failure(_):
                print("Failure update profile")
                completion(false)
            }
        }
    }
    
    func getShareCode() -> String {
        guard let profile = ProfileLocalService.getProfile() else { return "" }
        return profile.referralCode
    }
    
    func getSmsText() -> String {
        let message = APITexts.sharedInstace.shareTextSMS
        let newText = message.replacingOccurrences(of: "{{CODE}}", with: getShareCode())
        return newText
    }
    
    func getMailBody() -> String {
        let message = APITexts.sharedInstace.shareEmailBody
        let newText = message.replacingOccurrences(of: "{{CODE}}", with: getShareCode())
        return newText
    }
    
    func getMailSubject() -> String {
        return APITexts.sharedInstace.shareEmailSubject
    }
    
    func getSupportPhone() -> String {
        return APITexts.sharedInstace.supportPhone
    }
    
    func callSupport() {
        self.getSupportPhone().makeACall()
    }
    
    func getFacebookContentURL() -> String{
        return APITexts.sharedInstace.shareUrl
    }
    
    func getFacebookContentQuote() -> String{
        return APITexts.sharedInstace.shareFacebookTitle
    }
}
