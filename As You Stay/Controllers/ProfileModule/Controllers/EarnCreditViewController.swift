//
//  EarnCreditViewController.swift
//  As You Stay
//
//  Created by Max Ward on 21/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit
import Social
import MessageUI
import FBSDKShareKit

class EarnCreditViewController: UIViewController {
    
    @IBOutlet weak var shareCodeLabel: UILabel!
    fileprivate var viewModel = ProfileViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Earn credit"
        shareCodeLabel.text = viewModel.getShareCode()
    }
    
    @IBAction func shareMailDidTap(_ sender: Any) {
        let mailComposeViewController = MFMailComposeViewController()
        mailComposeViewController.mailComposeDelegate = self
        mailComposeViewController.setBccRecipients([])
        mailComposeViewController.setSubject(viewModel.getMailSubject())
        mailComposeViewController.setMessageBody(viewModel.getMailBody(), isHTML: false)
        if MFMailComposeViewController.canSendMail() {
            self.navigationController!.navigationBar.barTintColor = UIColor.black
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
           print("Error")
        }
    }
    
    @IBAction func shareSmsDidTap(_ sender: Any) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = viewModel.getSmsText()
            controller.recipients = []
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func shareFacebookDidTap(_ sender: Any) {
        let shareContent : ShareLinkContent = ShareLinkContent()
        shareContent.contentURL = URL(string: viewModel.getFacebookContentURL())!
        shareContent.quote = viewModel.getFacebookContentQuote()

        let dialog : ShareDialog = ShareDialog()
        dialog.mode = ShareDialog.Mode.native
        if !dialog.canShow {
            dialog.mode = ShareDialog.Mode.feedWeb
        }
        dialog.shareContent = shareContent
        dialog.fromViewController = self
        dialog.delegate = self
        dialog.show()
    }
}

// MARK: - Mail/SMS Compose Delegate
extension EarnCreditViewController: MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        if result == MessageComposeResult.sent {
           print("Message sended")
        }
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: Facebook share delegate
extension EarnCreditViewController: SharingDelegate {
    
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        print("Facebook share results: \(results)")
    }
    
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print("Facebook share fail with erro: \(error)")
    }
    
    func sharerDidCancel(_ sharer: Sharing) {
        print("Facebook share did cancel")
    }
}
