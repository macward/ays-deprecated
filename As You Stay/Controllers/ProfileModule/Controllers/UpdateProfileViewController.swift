//
//  UpdateProfileViewController.swift
//  As You Stay
//
//  Created by Max Ward on 20/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class UpdateProfileViewController: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phonoTextField: UITextField!
    var activityIndicator: ActivityIndicator!
    var delegate: UpdatedProfileDelegate?
    var viewModel: ProfileViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameTextField.text = viewModel?.getLocalProfile()?.firstName ?? ""
        lastNameTextField.text = viewModel?.getLocalProfile()?.lastName ?? ""
        phonoTextField.text = viewModel?.getLocalProfile()?.phone ?? ""
        
        shouldDismissKeyboard()
    }
    @IBAction func didTapUpdate(_ sender: Any) {
        
        activityIndicator = ActivityIndicator(view: view)
        activityIndicator.startAnimating()
        viewModel?.updateProfile(firstName: firstNameTextField.text ?? "", lastName: lastNameTextField.text ?? "", phone: phonoTextField.text ?? "", completion: { (status) in
            self.activityIndicator.stopAnimating()
            if status {
                self.delegate?.updatedProfile()
                self.dismiss(animated: true, completion: nil)
            } else {
                self.present(simpleAlert(title: "Error", message: "Something went wrong!"), animated: true)
            }
            
        })
        
    }
}
