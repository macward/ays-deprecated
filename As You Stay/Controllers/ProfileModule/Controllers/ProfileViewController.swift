//
//  ProfileViewController.swift
//  As You Stay
//
//  Created by Max Ward on 08/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit
import WebKit
import Intercom
import AdSupport

class ProfileViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var circularCreditView: UIView!
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var circularHourstView: UIView!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var circularEarnView: UIView!
    
    var viewModel: ProfileViewModel?
    var coordinator: ProfileCoordinator?
    
    lazy var faqWebView: WKWebView = {
        let web = WKWebView.init(frame: self.view.bounds)
        let url = URL.init(string: "https://asyoustay.com/faq/")!
        let request = URLRequest.init(url: url)
        web.load(request)
        return web
    }()
    
    lazy var termsWebView: WKWebView = {
        let web = WKWebView.init(frame: self.view.bounds)
        let url = URL.init(string: "https://asyoustay.com/terms/")!
        let request = URLRequest.init(url: url)
        web.load(request)
        return web
    }()
    
    lazy var privacyWebView: WKWebView = {
        let web = WKWebView.init(frame: self.view.bounds)
        let url = URL.init(string: "https://asyoustay.com/privacy/")!
        let request = URLRequest.init(url: url)
        web.load(request)
        return web
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Profile"
        setup()
    }
    
    @IBAction func didTapSignoutButton(_ sender: Any) {
        signout()
    }
    
    @IBAction func didTapEditButton(_ sender: Any) {
        let viewController = UpdateProfileViewController()
        viewController.viewModel = ProfileViewModel()
        viewController.delegate = self
        self.present(viewController, animated: true)
    }
    @IBAction func didTapCreditCardsButton(_ sender: Any) {
        showCreditCards()
    }
    
    @IBAction func faqDidTap(_ sender: Any) {
        let controller = UIViewController()
        controller.view.addSubview(self.faqWebView)
        controller.view.backgroundColor = .white
        let navigation = UINavigationController(rootViewController: controller)
        controller.title = "FAQ"
        self.present(navigation,animated: true)
    }
    
    @IBAction func termsDidTap(_ sender: Any) {
        let controller = UIViewController()
        controller.view.addSubview(self.termsWebView)
        controller.view.backgroundColor = .white
        let navigation = UINavigationController(rootViewController: controller)
        controller.title = "Terms and conditions"
        self.present(navigation,animated: true)
    }
    
    @IBAction func earnCreditDidTap(_ sender: Any) {
        let controller = EarnCreditViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func privacyDidTap(_ sender: Any) {
        let controller = UIViewController()
        controller.view.addSubview(self.privacyWebView)
        controller.view.backgroundColor = .white
        let navigation = UINavigationController(rootViewController: controller)
        controller.title = "Privacy and policy"
        self.present(navigation,animated: true)
    }
    
    @IBAction func callDidTap(_ sender: Any) {
        print(#function)
        if let viewModel = viewModel {
            viewModel.callSupport()
        } else {
            alert(title: "Error", msg: "There be an error to contact support.")
        }
    }
    
    @IBAction func chatDidTap(_ sender: Any) {
//        Intercom.presentMessageComposer("Hello!")
//        Intercom.presentHelpCenter()
        Intercom.presentMessenger()
    }
    
    fileprivate func setup(){
        guard let profile = viewModel?.getLocalProfile() else { return }
        
        nameLabel.text = "\(profile.firstName) \(profile.lastName)"
        emailLabel.text = profile.email
        phoneLabel.text = profile.phone
        
        creditLabel.text = "\(profile.accountBalance)"
        creditLabel.sizeToFit()
        hoursLabel.text = "\(profile.totalHrs)"
        hoursLabel.sizeToFit()
        
        circularCreditView.layer.cornerRadius = circularCreditView.frame.width / 2
        circularCreditView.clipsToBounds = true
        
        circularHourstView.layer.cornerRadius = circularHourstView.frame.width / 2
        circularHourstView.clipsToBounds = true
        
        circularEarnView.layer.cornerRadius = circularEarnView.frame.width / 2
        circularEarnView.clipsToBounds = true
        
        Intercom.setApiKey("fszdj8jw", forAppId: "ios_sdk-1a7905ce1a149e7ddf846026cc505c772718b7ad")
        Intercom.enableLogging()
        Intercom.registerUser(withEmail: profile.email)
        let userAttributes = ICMUserAttributes()
        userAttributes.name = "\(profile.firstName) \(profile.lastName)"
        userAttributes.email = profile.email
        userAttributes.phone = profile.phone
        userAttributes.userId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        Intercom.updateUser(userAttributes)
    }
    
    fileprivate func signout() {
        let alert = UIAlertController(title: "Destroy Session", message: "Are you sure?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Close", style: .cancel) { (status) in
            let authViewModel = AuthViewModel()
            authViewModel.remove()
            let tabBarViewController = MainTabBarViewController()
            tabBarViewController.modalPresentationStyle = .fullScreen
            Intercom.logout()
            self.setRootViewController(tabBarViewController)
        }
        alert.addAction(action)
        present(alert, animated: true)
    }
    
    fileprivate func showCreditCards(){
        navigationController?.pushViewController(CreditCardsViewController(), animated: true)
    }

}
extension ProfileViewController: UpdatedProfileDelegate {
    func updatedProfile() {
        guard let profile = viewModel?.getLocalProfile() else { return }
        nameLabel.text = "\(profile.firstName) \(profile.lastName)"
        emailLabel.text = profile.email
        phoneLabel.text = profile.phone
    }
    
    
}
