//
//  BookingViewController.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class BookingViewController: UIViewController {
    
    weak var coordinator: HotelCoordinator!
    var selectedCreditCard: CreditCardObject?
    var viewModel: BookingViewModel = BookingViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    var activityIndicator: ActivityIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityIndicator()
        setupTableView()
        setupNavigationBackButton()
        selectedCreditCard = getPrimaryCard()
        title = "Booking"
        if let views =  self.navigationController?.view.subviews{
            for view in views {
                if view.tag == 100{
                    view.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.137254902, blue: 0.1647058824, alpha: 1)
                    self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.137254902, blue: 0.1647058824, alpha: 1)
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        activityIndicator.backgroundView.frame = self.view.bounds
        activityIndicator.view?.frame = self.view.bounds
        activityIndicator.animationView.center = self.view.center
    }
    
    func setupActivityIndicator () {
        activityIndicator = ActivityIndicator(view: view)
    }
    
    func setupNavigationBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "back-arrow"), for: .normal)
        backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        
    }
    @objc fileprivate func didTapBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getPrimaryCard()-> CreditCardObject?{
        let profileViewModel = ProfileViewModel()
        if let creditCardList = profileViewModel.getCardList() {
            return creditCardList.first(where: { $0.isPrimary })
        }
        else { return nil }
    }
    
    func setupTableView(){
        tableView.backgroundColor = UIColor(hexString: "#1F232A")//(red: 32/255, green: 25/255, blue: 42/255, alpha: 1)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        registerCells()
    }
    
    func registerCells(){
        
        tableView.register(UINib(nibName: "TitleHotelTableViewCell", bundle: nil), forCellReuseIdentifier: "TitleHotelCell")
        
        tableView.register(UINib(nibName: "GuestTableViewCell", bundle: nil), forCellReuseIdentifier: "GuestCell")
        
        tableView.register(UINib(nibName: "CheckinCheckoutTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckinCheckoutCell")
        
        tableView.register(UINib(nibName: "RoomTableViewCell", bundle: nil), forCellReuseIdentifier: "RoomCell")
        
        tableView.register(UINib(nibName: "RoomPriceTableViewCell", bundle: nil), forCellReuseIdentifier: "RoomPriceCell")
        
        tableView.register(UINib(nibName: "SavingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SavingCell")
        
        tableView.register(UINib(nibName: "TotalTableViewCell", bundle: nil), forCellReuseIdentifier: "TotalCell")
        
        tableView.register(UINib(nibName: "BookingCreditCardsTableViewCell", bundle: nil), forCellReuseIdentifier: "BookingCreditCardsCell")
        
        tableView.register(UINib(nibName: "BookingSubmitTableViewCell", bundle: nil), forCellReuseIdentifier: "BookingSubmitCell")
    }
}

// MARK: - Table view delegate, source
extension BookingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 2:
            if let resortFees = viewModel.bookingDRO.selectedRoom?.propertyFee, resortFees > 0{
                

                let titleLabel = UILabel(frame: CGRect(x: 16, y: 0, width: tableView.frame.size.width - 32, height: 35))
                titleLabel.numberOfLines = 0;
                titleLabel.lineBreakMode = .byWordWrapping
                titleLabel.backgroundColor = UIColor.clear
                titleLabel.font = UIFont(name: "Montserrat-Regular", size: 12)
                titleLabel.text  = "* Resort fees. This hotel charges an additional $\(resortFees) per room and will be charged to you directly by the hotel."
                titleLabel.textColor = .white

                return titleLabel
            } else {
                return nil
            }
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 2:
            if let resortFees = viewModel.bookingDRO.selectedRoom?.propertyFee, resortFees > 0{
                return 45
            } else {
                return UITableView.automaticDimension
            }
            
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        case 1:
            return 2
        case 2:
            return 1
        case 3:
            return 2
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            switch indexPath.row {
            case 0:
                let titleCell = tableView.dequeueReusableCell(withIdentifier: "TitleHotelCell") as! TitleHotelTableViewCell
                titleCell.configure(with: viewModel.bookingDRO)
                return titleCell
            case 1:
                let guestCell = tableView.dequeueReusableCell(withIdentifier: "GuestCell") as! GuestTableViewCell
                guestCell.configure(with: viewModel.bookingDRO)
                return guestCell
            case 2:
                let CheckinCheckoutCell = tableView.dequeueReusableCell(withIdentifier: "CheckinCheckoutCell") as! CheckinCheckoutTableViewCell
                CheckinCheckoutCell.configure(with: viewModel.bookingDRO)
                return CheckinCheckoutCell
            case 3:
                let roomCell = tableView.dequeueReusableCell(withIdentifier: "RoomCell") as! RoomTableViewCell
                
                roomCell.configure(with: viewModel.bookingDRO)
                return roomCell
            default:
                let cell = UITableViewCell()
                return cell
            }
        case 1:
            switch indexPath.row {
            case 0:
                let roomPriceCell = tableView.dequeueReusableCell(withIdentifier: "RoomPriceCell") as! RoomPriceTableViewCell
                roomPriceCell.configure(with: viewModel.bookingDRO)
                return roomPriceCell
            case 1:
                let savingCell = tableView.dequeueReusableCell(withIdentifier: "SavingCell") as! SavingsTableViewCell
                savingCell.configure(with: viewModel.bookingDRO)
                return savingCell
            default:
                let cell = UITableViewCell()
                return cell
            }
        case 2:
            let totalCell = tableView.dequeueReusableCell(withIdentifier: "TotalCell") as! TotalTableViewCell
            totalCell.configure(with: viewModel.bookingDRO)
            totalCell.delegate = self
            return totalCell
            
        case 3:
            switch indexPath.row {
            case 0:
                let creditCardsCell = tableView.dequeueReusableCell(withIdentifier: "BookingCreditCardsCell") as! BookingCreditCardsTableViewCell
                if let creditCard = selectedCreditCard {
                    viewModel.bookingDRO.creditCardId = creditCard.identification
                    creditCardsCell.configure(with: creditCard)
                }
                return creditCardsCell
            case 1:
                let submitCell = tableView.dequeueReusableCell(withIdentifier: "BookingSubmitCell") as! BookingSubmitTableViewCell
                //booking.creditCardId = selectedCreditCard?.identification ?? ""
                submitCell.configure(with: viewModel.bookingDRO)
                submitCell.delegate = self
                return submitCell
            default:
                let cell = UITableViewCell()
                return cell
            }
        default:
            let cell = UITableViewCell()
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3 && indexPath.row == 0 {
            let profileViewModel = ProfileViewModel()
            if let creditCards = profileViewModel.getCardList(){
                self.coordinator.creditCards(ref: self, cardList: creditCards)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
    }
}

// MARK: - Did selected credit card delegate
extension BookingViewController: DidSelectedCreditCard {
    func didSelectedCreditCard(creditCard: CreditCardObject) {
        selectedCreditCard = creditCard
        viewModel.bookingDRO.creditCardId = creditCard.identification
        tableView.reloadData()
    }
}

// MARK: - Submit booking delegate
extension BookingViewController: SubmitBookingDelegate {
    func didTapSubmitButton() {
        
        activityIndicator.startAnimating()
        viewModel.submitBooking { (message, result)  in
            if let result = result {
                
                self.viewModel.getReceipt(bookingID: result.bookingId) { (status, result) in
                    self.activityIndicator.stopAnimating()
                    if let receipt = result {
                        self.coordinator.receipt(receipt)
                    } else {
                        let alert = simpleAlert(title: "Warning", message: message ?? "Confirmed reservation, but could not generate receipt, please check your reservation list")
                        self.present(alert, animated: true)
                    }
                }
            } else {
                let alert = simpleAlert(title: "Error", message: message ?? "Something went wrong!")
                self.present(alert, animated: true)
            }
        }
    }
}

// MARK: - Booking Total Discounts Delegate
extension BookingViewController: BookingTotalDiscountsDelegate {
    
    func setPromocode() {
    
        //FIXME: - Alert custom design
        let alert = UIAlertController(title: "Promo Code", message: "Promo code.", preferredStyle: .alert)
        alert.view.layer.borderColor = UIColor(hexString: "#0CC2EB").cgColor
        alert.view.layer.borderWidth = 1
        alert.view.layer.cornerRadius = 10
        alert.setTitlet(font: UIFont.boldSystemFont(ofSize: 26), color: UIColor(hexString: "#0CC2EB"))
        alert.setMessage(font: UIFont(name: "System Bold", size: 18), color: UIColor(hexString: "#0CC2EB"))
        alert.setBackgroundColor(color: UIColor(hexString: "#1F232A"))
        alert.setTint(color: UIColor(hexString: "#0CC2EB"))
        alert.addTextField { (textfield) in
            textfield.attributedPlaceholder = NSAttributedString(string: "Promo code",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            textfield.textColor = .white
        }
        
        for view: UIView in alert.textFields! {
            let container: UIView = view.superview!
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor(red: 43/256, green: 47/256, blue: 58/256, alpha: 1)
            effectView.removeFromSuperview()
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let submitAction = UIAlertAction(title: "Send", style: .default) { (action) in
            if let promoCode = alert.textFields?.first?.text?.uppercased(){
                self.activityIndicator.startAnimating()
                self.viewModel.validatePromoCodde(promoCode: promoCode) { message,validResponse in
                    self.activityIndicator.stopAnimating()
                    if validResponse != nil {
                        self.viewModel.bookingDRO.promoCode = promoCode
                        self.viewModel.getPricing(isBooking: true){ (success) in
                            if success {
                                self.tableView.reloadData()
                            }
                        }
                    } else {
                        self.present(simpleAlert(title: "Error", message: message ?? ""), animated: true)
                    }
                }
            }
        }

        alert.addAction(cancelAction)
        alert.addAction(submitAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func setLoyaltyAmmount() {
        guard let profile = ProfileLocalService.getProfile() else { return }
        
        let alert = UIAlertController(title: "Loyalty", message: "Use your loyalty credit. ($\(profile.accountBalance))", preferredStyle: .alert)
        alert.view.addShadow()
        alert.view.layer.borderColor = UIColor(hexString: "#0CC2EB").cgColor
        alert.view.layer.borderWidth = 1
        alert.view.layer.cornerRadius = 10
        alert.setTitlet(font: UIFont.boldSystemFont(ofSize: 26), color: UIColor(hexString: "#0CC2EB"))
        alert.setMessage(font: UIFont(name: "System Bold", size: 18), color: UIColor(hexString: "#0CC2EB"))
        alert.setBackgroundColor(color: UIColor(hexString: "#1F232A"))
        alert.setTint(color: UIColor(hexString: "#0CC2EB"))
        alert.addTextField { (textfield) in
            textfield.attributedPlaceholder = NSAttributedString(string: "Loyalty ammount",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            textfield.keyboardType = UIKeyboardType.decimalPad
            textfield.textColor = .white

        }
        
        for view: UIView in alert.textFields! {
            var container: UIView = view.superview!
            var effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor(red: 43/256, green: 47/256, blue: 58/256, alpha: 1)
            effectView.removeFromSuperview()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let submitAction = UIAlertAction(title: "Send", style: .default) { (action) in
            if let loyaltyAmmount = (alert.textFields?.first?.text as NSString?)?.floatValue{
                
                if loyaltyAmmount <= (self.viewModel.bookingDRO.selectedRoom?.marketPrice)! &&
                    loyaltyAmmount <= profile.accountBalance{
                    self.viewModel.bookingDRO.loyaltyCredit = loyaltyAmmount
                    self.tableView.reloadData()
                } else {
                    self.viewModel.bookingDRO.loyaltyCredit = 0
                    self.tableView.reloadData()
                }
            }
        }

        alert.addAction(cancelAction)
        alert.addAction(submitAction)
        self.present(alert, animated: true, completion: nil)
    }
}
