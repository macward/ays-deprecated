//
//  ReceiptViewController.swift
//  As You Stay
//
//  Created by Max Ward on 04/05/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class ReceiptViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var viewModel = ReceiptViewModel()
    weak var coordinator: HotelCoordinator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        refreshTabBar()
        coordinator.moveToInitial()
    }
    
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ReceiptTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiptCell")
        tableView.register(UINib(nibName: "HotelNameReceiptTableViewCell", bundle: nil), forCellReuseIdentifier: "HotelNameReceiptCell")
        tableView.register(UINib(nibName: "AcceptButtonReceiptTableViewCell", bundle: nil), forCellReuseIdentifier: "AcceptButtonReceiptCell")
    }
    
    func refreshTabBar() {
        let data:[String: Int] = ["index": 0]
        let notificationName = Notification.Name(rawValue: K.NotificationKeys.refreshMainTabBar)
        NotificationCenter.default.post(name:  notificationName, object: nil, userInfo: data)
    }
}
extension ReceiptViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let receipt = viewModel.receipt else { return UITableViewCell() }
        switch indexPath.section {
        case 0:
            let nameCell = tableView.dequeueReusableCell(withIdentifier: "HotelNameReceiptCell") as! HotelNameReceiptTableViewCell
            nameCell.configure(hotel: receipt.hotel.name, address: receipt.hotel.address)
            return nameCell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptCell") as! ReceiptTableViewCell
            cell.configure(with: "Booking Number", receipt.booking.id)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptCell") as! ReceiptTableViewCell
            cell.configure(with: "Confirmation Number", receipt.hotel.confirmationCode)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptCell") as! ReceiptTableViewCell
            cell.configure(with: "Check in", receipt.booking.checkIn)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptCell") as! ReceiptTableViewCell
            cell.configure(with: "Check out", receipt.booking.checkOut)
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptCell") as! ReceiptTableViewCell
            cell.configure(with: "Total", "$\(receipt.pricing.total)")
            return cell
        case 6:
             let cell = tableView.dequeueReusableCell(withIdentifier: "AcceptButtonReceiptCell") as! AcceptButtonReceiptTableViewCell
             cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    
}

extension ReceiptViewController: AcceptReceiptDelegate {
    func didTapAccept() {
        refreshTabBar()
        coordinator.moveToInitial()
    }
}
