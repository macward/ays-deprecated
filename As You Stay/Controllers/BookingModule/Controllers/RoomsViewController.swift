//
//  RoomsViewController.swift
//  As You Stay
//
//  Created by Max Ward on 16/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class RoomsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var roomTypes: [PricingV3RoomType]?
    var important: [String]?
    var dro = BookingDRO()
    var viewModel: BookingViewModel = BookingViewModel()
    var activityIndicator: ActivityIndicator!
    
    weak var coordinator: HotelCoordinator!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = ActivityIndicator(view: view)
        setupTableView()
        setupNavigationBackButton()
        title = "Room types"
        
        if let views =  self.navigationController?.view.subviews{
            for view in views {
                if view.tag == 100{
                    view.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.137254902, blue: 0.1647058824, alpha: 1)
                    self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.137254902, blue: 0.1647058824, alpha: 1)
                }
            }
        }
    }
    
    fileprivate func updateProfile(){
        let profileViewModel = ProfileViewModel()
        profileViewModel.store { (status, message) in
            if status {
                print("Profile stored")
            } else {
                print(message ?? "")
            }
        }
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: "RoomTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "RoomTypeCell")
        tableView.backgroundColor = UIColor(hexString: "#1F232A")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setupNavigationBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "back-arrow"), for: .normal)
        backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        
    }
    @objc fileprivate func didTapBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        activityIndicator.backgroundView.frame = self.view.bounds
        activityIndicator.view?.frame = self.view.bounds
        activityIndicator.animationView.center = self.view.center
    }
}

// MARK: - Table View Delegate
extension RoomsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return roomTypes?.count ?? 0
        case 1:
            return important?.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 129
        default:
            return UITableView.automaticDimension
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard (self.important?.count) != nil else { return 1}
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 1:
            if let importantCount = self.important?.count, importantCount > 0 {
                let titleLabel = UILabel(frame: CGRect(x: 16, y: 0, width: tableView.frame.size.width - 32, height: 20))
                titleLabel.numberOfLines = 0;
                titleLabel.lineBreakMode = .byWordWrapping
                titleLabel.backgroundColor = UIColor.clear
                titleLabel.font = UIFont(name: "Montserrat-Regular", size: 17)
                titleLabel.text  = "Important info"
                titleLabel.textColor = .white

                return titleLabel
            } else {
                return nil
            }
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            if let importantCount = self.important?.count, importantCount > 0 {
                return 36
            } else {
                return UITableView.automaticDimension
            }
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RoomTypeCell") as! RoomTypeTableViewCell
            cell.selectionStyle = .none
            
            if let rooms = roomTypes {
                cell.configure(with: rooms[indexPath.row])
            }
            cell.layer.borderWidth = 5
            cell.cornerRadius = 5
            cell.layer.borderColor = UIColor(named: "Background")?.cgColor
            return cell
        default:
            let cell =  UITableViewCell()
            guard let info = important?[indexPath.row] else { return cell }
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.text = info
            cell.textLabel?.textColor = .white
            cell.backgroundColor = UIColor(hexString: "#2B2F38")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != 0 { return }
        guard let room = roomTypes else { return }
        let ppnBundle = room[indexPath.row].ppnBundle
        let hotelID = dro.hotelId
        activityIndicator = ActivityIndicator(view: view.superview!.superview!.superview!.superview!.superview!.superview!.superview!.superview!)
        activityIndicator.startAnimating()
        self.viewModel.getPricingV3(adults: viewModel.selectedGuests?.adults ?? 2,
                                    children: viewModel.selectedGuests?.children ?? 0,
                                    hotolID: hotelID,
                                    ppnBundle: ppnBundle) { (response) in
            if let room = response?.roomTypes.first   {
                var booking = self.dro
                //booking.roomType =
                //booking.pricing = pricing
                booking.selectedRoom = room
                booking.nightAmount = (response?.duration ?? 0) /  24
                booking.guests = self.viewModel.selectedGuests
                self.activityIndicator.stopAnimating()
                self.coordinator?.booking(dro: booking)
            }
        }
    }
}
