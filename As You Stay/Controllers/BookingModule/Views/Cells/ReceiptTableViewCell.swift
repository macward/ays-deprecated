//
//  ReceiptTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 04/05/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class ReceiptTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure( with title: String,_ detail: String) {
        titleLabel.text = title
        detailLabel.text = detail
    }
}
