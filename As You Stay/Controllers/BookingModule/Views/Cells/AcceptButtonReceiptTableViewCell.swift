//
//  AcceptButtonReceiptTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 07/05/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class AcceptButtonReceiptTableViewCell: UITableViewCell {

    var delegate: AcceptReceiptDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didTapAcceptButton ( _ sender: Any) {
        delegate?.didTapAccept()
    }
}
