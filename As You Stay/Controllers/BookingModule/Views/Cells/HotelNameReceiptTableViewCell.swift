//
//  HotelNameReceiptTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 04/05/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class HotelNameReceiptTableViewCell: UITableViewCell {
    @IBOutlet weak var hotelNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure( hotel name: String, address: String) {
        hotelNameLabel.text = name
        addressLabel.text = address
    }
    
}
