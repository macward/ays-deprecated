//
//  bookingTotalDiscountsViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 06/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation

class bookingTotalDiscountsViewModel: BookingTotalDiscountsViewModelProtocol {
    internal let bookingService = BookingService()
    var bookingDRO = BookingDRO()
    
    func validatePromoCodde(promoCode: String, completion: @escaping (String?, Validation?) -> Void) {
        bookingService.validatePromoCodde(promoCode: promoCode) { (result) in
            switch result {
            case .success(let receipt):
                if receipt.statusCode == 0 {
                    completion(nil, receipt.data)
                } else {
                    completion(receipt.message, receipt.data)
                }
                
            case .failure(let err):
                print(err)
                completion(err.localizedDescription, nil)
            }
        }
    }
}
