//
//  BookingViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 12/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import Braintree

class BookingViewModel {
    
    fileprivate let bookingService = BookingService()
    var bookingDRO = BookingDRO()
    var slectedRoom: PricingV3RoomType?
    var selectedGuests: GuestSearchDTO?
    
    func submitBooking(/*with dro: BookingDRO,*/ completion: @escaping (String?,BookingData?) -> Void ) {
        
        var dataCollector: BTDataCollector?
        
        guard let profile = ProfileLocalService.getProfile() else { return }
        let brainTree = profile.braintreeSession
        
        if let apiClient = BTAPIClient(authorization: brainTree) {
            dataCollector = BTDataCollector(apiClient: apiClient)
        }
        dataCollector?.collectFraudData({ (deviceData) in
            
            let checkin = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)
            let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)
            let guests = GuestSearchDTO(adults: 2, children: 0)
            let bookingObj = BookingDTO(hotelId: self.bookingDRO.hotelId,
                                        checkInDate: checkin?.getFormatToRequest() ?? "",
                                        checkOutDate: checkout?.getFormatToRequest() ?? "",
                                        guestFirstName: self.bookingDRO.guestFirstName,
                                        guestLastName: self.bookingDRO.guestLastName,
                                        creditCard: self.bookingDRO.creditCardId,
                                        promoCode: self.bookingDRO.promoCode,
                                        redeemCredits: Double(self.bookingDRO.loyaltyCredit),
                                        deviceData: deviceData,
                                        ppnBundle: self.bookingDRO.selectedRoom?.ppnBundle ?? self.bookingDRO.ppnBundle,
                                        rooms: self.bookingDRO.guests ?? guests)
            
            self.bookingService.submitBooking(booking: bookingObj) { (result) in
                switch result{
                case .success(let value):
                    if value.statusCode == 0 {
                        if self.bookingDRO.loyaltyCredit != 0 {
                            let profileViewModel = ProfileViewModel()
                            profileViewModel.updateLoyalty(substract: self.bookingDRO.loyaltyCredit)
                        }
                        completion(nil, value.data)
                    } else {
                        completion(value.message, nil)
                    }
                    
                case .failure(let err):
                    completion(err.localizedDescription, nil)
                }
            }
            
        })
        
    }
    
    func getReceipt(bookingID: String, completion: @escaping (String?, ReceiptObject?) ->Void){
        bookingService.getReceipt(id: bookingID) { (result) in
            switch result {
            case .success(let receipt):
                if receipt.statusCode == 0 {
                    completion(nil, receipt.data)
                } else {
                    completion(receipt.message, nil)
                }
            case .failure(let err):
                completion(err.localizedDescription, nil)
            }
        }
    }
    
    func getUpcomingReservations(completion: @escaping (ReceiptObjectList?) ->Void) {
        bookingService.getUpcomingReservations() { (result) in
            switch result {
            case .success(let receipt):
                completion(receipt.data)
            case .failure(let err):
                print(err)
                completion(nil)
            }
        }
    }
    
    func validatePromoCodde(promoCode: String, completion: @escaping (String?,Validation?) -> Void) {
        bookingService.validatePromoCodde(promoCode: promoCode) { (result) in
            switch result {
            case .success(let receipt):
                if receipt.statusCode == 0 {
                    completion(nil,receipt.data)
                }else  {
                    completion(receipt.message,receipt.data)
                }
                
            case .failure(let err):
                print(err)
                completion(err.localizedDescription, nil)
            }
        }
    }
    
    func getPricing(isBooking: Bool = false, completion: @escaping (Bool) -> Void){
        let hotelService = HotelService()
        
        guard let checkin  = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)?.getFormatToRequest() else { return }
        guard let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)?.getFormatToRequest() else { return }
        
        hotelService.getRoomPricingV3(checkin_datetime: checkin,
                                      checkout_datetime: checkout,
                                      adults: selectedGuests?.adults ?? 2 ,
                                      children: selectedGuests?.children ?? 0,
                                      hotel_id: bookingDRO.hotelId,
                                      is_booking: isBooking,
                                      promo_code: bookingDRO.promoCode,
                                      ppn_bundle: bookingDRO.selectedRoom?.ppnBundle ?? bookingDRO.ppnBundle) { (result) in
            
            switch result{
            case .success(let value):
                self.bookingDRO.selectedRoom = value.data?.roomTypes.first
                completion(true)
            case .failure(let err):
                print(err)
                completion(false)
            }
            
        }
    }
    
    func getPricingV3(adults: Int,children: Int,hotolID: String, ppnBundle: String, completion: @escaping (PricingV3Data?) -> Void){
        let hotelService = HotelService()
        
        guard let checkin  = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)?.getFormatToRequest() else { return }
        guard let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)?.getFormatToRequest() else { return }
        
        hotelService.getRoomPricingV3(checkin_datetime: checkin,
                                      checkout_datetime: checkout,
                                      adults: adults,
                                      children: children,
                                      hotel_id: hotolID,
                                      is_booking: true,
                                      promo_code: "",
                                      ppn_bundle: ppnBundle) { (result) in
            
            switch result{
            case .success(let value):
                self.slectedRoom = value.data?.roomTypes.first
                completion(value.data)
            case .failure(let err):
                print(err)
                completion(nil)
            }
        }
    }
}
