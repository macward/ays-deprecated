//
//  NotificationsViewController.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {
    
    var coordinator: NotificationsCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.137254902, blue: 0.1647058824, alpha: 1)
        title = "Notifications"
    }
    
}
