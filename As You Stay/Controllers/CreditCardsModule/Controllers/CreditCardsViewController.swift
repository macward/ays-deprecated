//
//  CreditCardsViewController.swift
//  As You Stay
//
//  Created by Max Ward on 07/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class CreditCardsViewController: UIViewController {
    
    var viewModel = CreditCardViewModel()
    var delegate: DidSelectedCreditCard?
    var activityIndicator: ActivityIndicator!
    
    weak var coordinator: HotelCoordinator!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCardButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAddCardButton()
        setupActivityIndicator()
        setupTableView()
        title = "Credit cards"
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.137254902, blue: 0.1647058824, alpha: 1)
        self.view.backgroundColor = UIColor(red: 32/255, green: 35/255, blue: 42/255, alpha: 1)
    }
    
    override func viewDidLayoutSubviews() {
        activityIndicator.backgroundView.frame = self.view.bounds
        activityIndicator.view?.frame = self.view.bounds
        activityIndicator.animationView.center = self.view.center
    }
    
    @IBAction func didTapAddCardButton(_ sender: Any) {
        let viewController = AddCreditCardViewController()
        viewController.delegate = self
        viewController.viewModel = CreditCardViewModel()
        self.present(viewController, animated: true)
    }
    
    func setupAddCardButton(){
      addCardButton.layer.cornerRadius = 0.5 * addCardButton.bounds.size.width
      addCardButton.clipsToBounds = true
    }
    
    func setupActivityIndicator () {
        activityIndicator = ActivityIndicator(view: view)
    }
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.register(UINib(nibName: "CreditCardsTableViewCell", bundle: nil), forCellReuseIdentifier: "CreditCardsCell")
    }
    
    fileprivate func configureIsPrimaryAction (index: Int) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "Primary") { (action, view, nil) in
            self.present(customAlert(title: "Set as primary card ", message: "Are you sure ?", firstTitleAction: "YES", secondTitleAction: "NO", firstActionHandler: { (action) in
                guard let cards = self.viewModel.getCardList() else { return }
                self.activityIndicator.startAnimating()
                self.viewModel.setAsPrimary(identification: cards[index].identification) { (status) in
                    self.activityIndicator.stopAnimating()
                    if status {
                        self.tableView.reloadData()
                    }
                }
                
               
                }, secondActionHandler: { (action) in
                    print(" cancel")
                }), animated: true)
        }
        action.backgroundColor = UIColor(red: 12/255, green: 194/255, blue: 235/255, alpha: 1)
        return action
    }
    
    fileprivate func configureDeleteAction (index: Int) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "Remove") { (action, view, nil) in
            self.present(customAlert(title: "Remove ", message: "Are you sure ?", firstTitleAction: "YES", secondTitleAction: "NO", firstActionHandler: { (action) in
                
                guard let cards = self.viewModel.getCardList() else { return }
                self.activityIndicator.startAnimating()
                self.viewModel.remove(with: cards[index].identification) { (status) in
                    self.activityIndicator.stopAnimating()
                    if status {
                        print("borrada")
                        self.tableView.reloadData()
                    }
                }
               
            }, secondActionHandler: { (action) in
                print(" cancel")
                self.tableView.reloadData()
            }), animated: true)
        }
        action.backgroundColor = .red
        return action
    }
    fileprivate func configureUpdateAction (index: Int) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "Update") { (action, view, nil) in
            self.present(self.setUpdteLabelAlert(index: index), animated: true)
        }
        action.backgroundColor = .yellow
        return action
    }
    
    func setUpdteLabelAlert(index: Int) -> UIAlertController {
        let alert = UIAlertController(title: "Update Label", message: "Write new label", preferredStyle: .alert)
        alert.view.layer.borderColor = UIColor(hexString: "#0CC2EB").cgColor
        alert.view.layer.borderWidth = 1
        alert.view.layer.cornerRadius = 10
        alert.setTitlet(font: UIFont.boldSystemFont(ofSize: 26), color: UIColor(hexString: "#0CC2EB"))
        alert.setMessage(font: UIFont(name: "System Bold", size: 18), color: UIColor(hexString: "#0CC2EB"))
        alert.setBackgroundColor(color: UIColor(hexString: "#1F232A"))
        alert.setTint(color: UIColor(hexString: "#0CC2EB"))
        alert.addTextField { (textfield) in
            textfield.attributedPlaceholder = NSAttributedString(string: "New Label",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            textfield.textColor = .white
        }
        
        for view: UIView in alert.textFields! {
            let container: UIView = view.superview!
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor(red: 43/256, green: 47/256, blue: 58/256, alpha: 1)
            effectView.removeFromSuperview()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            self.tableView.reloadData()
        })
        let submitAction = UIAlertAction(title: "Update", style: .default) { (action) in
            
            guard let cards = self.viewModel.getCardList() else { return }
            guard let textField = alert.textFields?[0] else { return }
            self.activityIndicator.startAnimating()
            self.viewModel.updateLabel(identification: cards[index].identification, label: textField.text ?? "") { (status) in
                 self.activityIndicator.stopAnimating()
                if status {
                    self.tableView.reloadData()
                }
            }
        }

        alert.addAction(cancelAction)
        alert.addAction(submitAction)
        
        return alert
    }
}

// MARK: - Table view data source, delegate
extension CreditCardsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getCardList()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreditCardsCell") as! CreditCardsTableViewCell
        if let card = viewModel.getCardList()?[indexPath.row] {
             cell.configure(with: card)
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let card = viewModel.getCardList()?[indexPath.row] {
             delegate?.didSelectedCreditCard(creditCard: card )
        }
       
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let cards = viewModel.getCardList() else { return nil }
        if cards[indexPath.row].isPrimary {
            return nil
        } else {
            return UISwipeActionsConfiguration(actions: [configureIsPrimaryAction(index: indexPath.row)])
        }
        
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
       
        return UISwipeActionsConfiguration(actions: [configureDeleteAction(index: indexPath.row), configureUpdateAction(index: indexPath.row)])
    }
}
extension CreditCardsViewController : DidAddCreditCardDelegate {
    func addedNewCard() {
        tableView.reloadData()
    }
}
