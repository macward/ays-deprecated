//
//  AddCreditCardViewController.swift
//  As You Stay
//
//  Created by Max Ward on 06/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class AddCreditCardViewController: UIViewController {
    
    @IBOutlet weak var nameOnCardTextField: UITextField!
    @IBOutlet weak var numberCardTextField: UITextField!
    @IBOutlet weak var MMYYTextField: UITextField!
    @IBOutlet weak var CVVTextField: UITextField!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var paymentTitleTextField: UITextField!
    @IBOutlet weak var primarySwitch: UISwitch!
    
    var delegate: DidAddCreditCardDelegate?
    var viewModel: CreditCardViewModel?
    var activityIndicator: ActivityIndicator!
    
    @IBAction func didTapAddCardButton(_ sender: Any) {
        
        guard let viewModel = viewModel else {
            return
        }
        if viewModel.validateZipCode(using: zipCodeTextField.text!)
            && viewModel.validateNumberCard(usign: numberCardTextField.text!)
            && viewModel.validateCVV(using: CVVTextField.text!) {
            
            activityIndicator.startAnimating()
                   viewModel.addCreditCard(cardNumber: numberCardTextField.text!,
                                            mmyy: MMYYTextField.text!,
                                            cvv: CVVTextField.text!,
                                            zipCode: zipCodeTextField.text!,
                                            nameOnCard: nameOnCardTextField.text!,
                                            paymentTitle: paymentTitleTextField.text!,
                                            isPrimary: primarySwitch.isOn,
                                            completion: { (status, response) in
                                               self.activityIndicator.stopAnimating()
                                               if status {
                                                    self.dismiss(animated: true, completion: {
                                                        self.delegate?.addedNewCard()
                                                    })
                                               } else {
                                                    self.present(simpleAlert(title: "Failure", message: "\(response)"), animated: true)
                                               }
                                               
                   })
            
        } else {
            self.present(simpleAlert(title: "Error", message: "Data incorrect"), animated: true)
        }
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityIndicator()
        MMYYTextField.delegate = self
        shouldDismissKeyboard()
    }
    
    override func viewDidLayoutSubviews() {
        activityIndicator.backgroundView.frame = self.view.bounds
        activityIndicator.view?.frame = self.view.bounds
        activityIndicator.animationView.center = self.view.center
    }
    
    func setupActivityIndicator () {
        activityIndicator = ActivityIndicator(view: view)
    }
}

extension AddCreditCardViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        
        let currentText = textField.text! as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: string)
        
        textField.text = updatedText
        let numberOfCharacters = updatedText.count
        if numberOfCharacters == 2 {
            textField.text?.append("/")
        }
        
        if range.location > 4 {
               textField.text?.removeLast()
        }
        return false
    }
}
