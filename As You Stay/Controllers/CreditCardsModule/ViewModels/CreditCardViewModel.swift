//
//  CreditCardViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 07/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit
import Braintree

class CreditCardViewModel {
    
    fileprivate let creditCardService = CreditCardService()
    
    func setImage(with type: String)-> UIImage?{
        switch type {
        case "Visa":
            return UIImage(named: "Visa_White")
        case "MasterCard":
            return UIImage(named: "MasterCard_White")
        case "American Express":
            return UIImage(named: "Amex")
        default:
            return UIImage(named: "credit-card-placeholder")
        }
    }
    
    func getCardList() -> [CreditCardObject]? {
        return CreditCardLocalService.list()
    }
    
    func validateCVV(using CVV: String) -> Bool {
        if CVV.count < 3 {
            return false
        }
        return true
    }
   
    func validateNumberCard(usign number: String) -> Bool {
        if number.count < 6 {
            return false
        }
        return true
    }
    func validateZipCode(using code: String ) -> Bool {
        if code.count < 1 {
            return false
        }
        return true
    }
    
    func addCreditCard(cardNumber: String,
                       mmyy:String,
                       cvv: String,
                       zipCode: String,
                       nameOnCard: String,
                       paymentTitle: String,
                       isPrimary: Bool, completion: @escaping (Bool, String) -> ()) {

        var dataCollector: BTDataCollector?

        guard let profile = ProfileLocalService.getProfile() else { return }
        let brainTree = profile.braintreeSession

        if let apiClient = BTAPIClient(authorization: brainTree) {
            dataCollector = BTDataCollector(apiClient: apiClient)
        }
       
        dataCollector?.collectCardFraudData({ (deviceData) in

            let braintreeClient = BTAPIClient(authorization: brainTree)!
            let cardClient = BTCardClient(apiClient: braintreeClient)
            let card = BTCard(number: cardNumber, expirationMonth: self.getMM(using: mmyy), expirationYear: self.getYY(using: mmyy), cvv: cvv)
            cardClient.tokenizeCard(card) { (response, error) in
                if error == nil {
                    let creditCardService = CreditCardService()
                    let dto = CreditCardDTO(cvv: cvv,
                                            isPrimary: isPrimary,
                                            expMM: self.getMM(using: mmyy),
                                            zipCode: zipCode,
                                            label: paymentTitle,
                                            last4: self.getLast4(using: cardNumber),
                                            token: response!.nonce,
                                            expYY: self.getYY(using: mmyy),
                                            deviceData: deviceData,
                                            cardHolder: nameOnCard)
                    creditCardService.addCreditCard(card: dto) { (result) in
                        switch result {
                        case .success( _):
                            print("success, new card added")
                            let creditCard = CreditCardObject()
                            creditCard.expMm = self.getMM(using: mmyy)
                            creditCard.expYy = self.getYY(using: mmyy)
                            creditCard.holder = nameOnCard
                            creditCard.identification = cardNumber
                            creditCard.isPrimary = isPrimary
                            creditCard.label = paymentTitle
                            creditCard.last4 = self.getLast4(using: cardNumber)
                            creditCard.zipcode = ""
                            creditCard.type = self.getType(using: cardNumber)
                            CreditCardLocalService.store(creditCard: creditCard)
                            completion(true, response!.nonce)
                        case .failure(let err):
                            completion(false, "")
                            print("failure, new card added")
                            print(err)
                        }
                    }
                } else {
                    print("failure, tokenize Card")
                    print(error ?? "")
                    completion(false, "Credit card is not valid!")
                }
            }
        })
    }
    
    func getLast4( using number: String) -> String {
        let index = number.index(number.endIndex, offsetBy: -4)
        return  String(number[index...])
    }
    
    func getMM(using experitationDate: String) -> String {
        let index = experitationDate.index(experitationDate.startIndex, offsetBy: 2)
        return String(experitationDate[..<index])
    }
    
    func getYY(using experirationDate: String) -> String {
        return String(experirationDate.suffix(2))
    }
    
    func getType(using number: String) -> String {
//        Visa: 4xxxxxx
//        Mastercard: 5xxxxx
//        Discover: 6011xx, 644xxx, 65xxxxs
//        American Express: 3xxxx, 37xxxx
//        Diner's Club and Carte Blanche: 300xxx-305xxx, 36xxxx, 38xxxx
       let index = number.index(number.startIndex, offsetBy: 1)
        
        switch  String(number[..<index]){
        case "3":
            return "American Express"
        case "4":
           return "Visa"
        case "5":
            return "MasterCard"
        default:
            return ""
        }
    }
    
    func remove(with identification: String, completion: @escaping (Bool) -> Void) {
        let creditCardService = CreditCardService()
        creditCardService.remove(id: identification) { (result) in
            switch result {
            case .success(_):
                CreditCardLocalService.destroy(with: identification)
                completion(true)
            case .failure(let err):
                print(err)
                completion(false)
            }
        }
    }
    
    func setAsPrimary(identification: String, completion: @escaping (Bool)->Void) {
        let creditCardService = CreditCardService()
        creditCardService.setAsPrimary(identification: identification) { (result) in
            switch result {
            case .success(_):
                CreditCardLocalService.setAsPrimary(with: identification)
                completion(true)
            case .failure(let err):
                print(err)
                completion(false)
            }
        }
    }
    
    func updateLabel( identification: String, label: String, completion: @escaping (Bool)->Void){
        let creditCardService = CreditCardService()
        creditCardService.updateLabel(id: identification, label: label) { (result) in
            switch result {
            case .success(_):
                CreditCardLocalService.updateLabel(with: identification, newLabel: label)
                completion(true)
            case .failure(let err):
                print(err)
                completion(false)
            }
        }
    }
}
