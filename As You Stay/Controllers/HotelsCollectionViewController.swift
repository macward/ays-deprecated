//
//  HotelsCollectionViewController.swift
//  As You Stay
//
//  Created by Max Ward on 8/1/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import PinLayout

class HotelsCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var screenWidth: CGFloat = 0.0
    var screenHeight: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenWidth = view.bounds.width
        screenHeight = view.bounds.height
        let layout = collectionView.collectionViewLayout  as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: (UIHelper.multiplierFrom8() * 312), height: 135 )
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "HotelCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HotelCell")
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.contentInsetAdjustmentBehavior = .always
        collectionView.isPagingEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstraints()
    }
    
    func setupConstraints(){
        collectionView.pin.left(0).bottom(8).right(0).height(135)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HotelCellNew", for: indexPath) as! HotelsCollectionViewCellNew
        cell.backgroundColor = UIColor.white
        return cell
    }
}
