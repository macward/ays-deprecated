//
//  CalendarViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 8/30/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class CalendarViewModel {
    
    func setSelectableDate(days: Int) -> Date {
       return Calendar.current.date(byAdding: .day, value: days, to: Date())!
    }
    
    func datesRange(from: Date, to: Date) -> [Date] {
        
        if from > to { return [Date]() }
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        return array
    }
}
