//
//  CalendarViewController.swift
//  As You Stay
//
//  Created by Max Ward on 8/21/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import FSCalendar
import AssistantKit

class CalendarViewController: UIViewController {

    @IBOutlet weak var checkinCheckoutLabel: UILabel!
    @IBOutlet weak var checkinLabel: UILabel!
    @IBOutlet weak var checkoutlabel: UILabel!
    
    @IBOutlet weak var checkinDateLabel: UILabel!
    @IBOutlet weak var checkouDateLabel: UILabel!
    
    @IBOutlet weak var checkinCollectionView: UICollectionView!
    @IBOutlet weak var checkoutCollectionView: UICollectionView!
    @IBOutlet weak var calendar: FSCalendar!
    
    @IBOutlet weak var calendarHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var buttonOKBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkinCollectionViewBottomConstraint: NSLayoutConstraint!
    
    var firstDate: Date?
    var lastDate: Date?
    var checkinHours : Int = 0
    var checkoutHours: Int = 0
    var hoursArray = Array(0...23)
    var maxDateRange : Int = 14
    var cellSize: Int = 0
    var didSelectedDateDelegate: DidSelectedDateDelegate?
    let viewModel = CalendarViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCalendar()
        setupLabels()
        setupCollectionViews()
        preSelectedDates()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkinCollectionView.selectItem(at: NSIndexPath(item: DatesManager.shared.checkinHours, section: 0) as IndexPath, animated: false, scrollPosition: .centeredHorizontally)

        checkoutCollectionView.selectItem(at: NSIndexPath(item: DatesManager.shared.checkoutHours, section: 0) as IndexPath, animated: false, scrollPosition: .centeredHorizontally)
    }
    
    @IBAction func didTapOKButton(_ sender: Any) {
        
        guard let checkin = firstDate, let checkout = lastDate else {
            self.present(simpleAlert(title: "Warning", message: "Select a checkin date!"), animated: true)
            return
        }
        if checkin == checkout {
            if checkoutHours == checkinHours {
                self.present(simpleAlert(title: "Warning", message: "The checkin time cannot be the same as the checkout time!"), animated: true)
                return
            }
            if checkoutHours < checkinHours {
                self.present(simpleAlert(title: "Warning", message: "The checkin time must be less than the checkout time!"), animated: true)
                return
            }
        }
        dismiss(animated: true) {
            self.didSelectedDateDelegate?.didSelectedDate(checkin: checkin, checkout: checkout, checkinHours: self.checkinHours, checkoutHours: self.checkoutHours)
        }
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupCalendar(){
        switch Device.screen.rawValue {
        case 6.5:
            calendarHeightContraint.constant = 400
            buttonOKBottomConstraint.constant = 52
            checkinCollectionViewBottomConstraint.constant = 52
            titleTopConstraint.constant = 40
            calendar.appearance.titleFont = UIFont.App.regular(size: 18)//dias del calendario
            calendar.appearance.weekdayFont = UIFont.App.bold(size: 18)//dias literales S,M,T, etc
            calendar.adjustsBoundingRectWhenChangingMonths = false
            cellSize = 60
            break
        case 6.1://xr
            calendarHeightContraint.constant = 350
            buttonOKBottomConstraint.constant = 52
            checkinCollectionViewBottomConstraint.constant = 52
            titleTopConstraint.constant = 40
            calendar.appearance.titleFont = UIFont.App.regular(size: 18)//dias del calendario
            calendar.appearance.weekdayFont = UIFont.App.bold(size: 18)//dias literales S,M,T, etc
            calendar.adjustsBoundingRectWhenChangingMonths = false
            cellSize = 60
            break
        case 5.8: // x
            calendarHeightContraint.constant = 350
            buttonOKBottomConstraint.constant = 24
            checkinCollectionViewBottomConstraint.constant = 24
            titleTopConstraint.constant = 24
            calendar.appearance.titleFont = UIFont.App.regular(size: 17)//dias del calendario
            calendar.appearance.weekdayFont = UIFont.App.bold(size: 17)//dias literales S,M,T, etc
            calendar.adjustsBoundingRectWhenChangingMonths = false
            cellSize = 50
            break
        case 5.5: //7+,8+
            calendarHeightContraint.constant = 325
            cellSize = 50
            buttonOKBottomConstraint.constant = 32
            checkinCollectionViewBottomConstraint.constant = 32
            titleTopConstraint.constant = 24
            calendar.appearance.titleFont = UIFont.App.regular(size: 17)//dias del calendario
            calendar.appearance.weekdayFont = UIFont.App.bold(size: 17)//dias literales S,M,T, etc
            calendar.adjustsBoundingRectWhenChangingMonths = false
            break
        default:
            calendarHeightContraint.constant = 300
            cellSize = 40
            calendar.appearance.titleFont = UIFont.App.regular(size: 16)//dias del calendario
            calendar.appearance.weekdayFont = UIFont.App.bold(size: 16)//dias literales S,M,T, etc
            calendar.adjustsBoundingRectWhenChangingMonths = false
            break
        }
        calendar.dataSource = self
        calendar.delegate = self
        calendar.register(FSCalendarCell.self, forCellReuseIdentifier: "CELL")
        calendar.allowsMultipleSelection = true
        calendar.headerHeight = 40.0
        calendar.backgroundColor = .clear
        calendar.appearance.borderSelectionColor = UIColor(red:0.12, green:0.62, blue:0.8, alpha:1)
        calendar.appearance.titleSelectionColor = UIColor(red:0.12, green:0.62, blue:0.8, alpha:1)
        calendar.appearance.headerTitleFont = UIFont.App.regular(size: 14)// mes literal
        calendar.appearance.caseOptions = .init(arrayLiteral:  .weekdayUsesSingleUpperCase, .headerUsesUpperCase)
        calendar.appearance.borderRadius = 0.3
        calendar.scrollEnabled = true
        calendar.scrollDirection = .vertical
        calendar.placeholderType = .none // con esto solo dejo visibles los dias del corriente mes
        
        

    }
    
    func setupLabels(){
        checkinCheckoutLabel.font = UIFont.App.regular(size: 16)
        checkinCheckoutLabel.numberOfLines = 1
        checkinCheckoutLabel.adjustsFontSizeToFitWidth = true
        checkinCheckoutLabel.textAlignment = .left
        
        checkinLabel.font = UIFont.App.regular(size: 14)
        checkinLabel.numberOfLines = 1
        checkinLabel.adjustsFontSizeToFitWidth = true
        checkinLabel.textAlignment = .left
        
        checkinDateLabel.text = "--"
        checkinDateLabel.font = UIFont.App.bold(size: 14)
        checkinDateLabel.numberOfLines = 1
        checkinDateLabel.adjustsFontSizeToFitWidth = true
        checkinDateLabel.textAlignment = .left
        checkinDateLabel.textColor = UIColor(red:0.12, green:0.62, blue:0.8, alpha:1)
        
        checkoutlabel.font = UIFont.App.regular(size: 14)
        checkoutlabel.numberOfLines = 1
        checkoutlabel.adjustsFontSizeToFitWidth = true
        checkoutlabel.textAlignment = .left
        
        checkouDateLabel.text = "--"
        checkouDateLabel.font = UIFont.App.bold(size: 14)
        checkouDateLabel.numberOfLines = 1
        checkouDateLabel.adjustsFontSizeToFitWidth = true
        checkouDateLabel.textAlignment = .left
        checkouDateLabel.textColor = UIColor(red:0.12, green:0.62, blue:0.8, alpha:1)
    }
    
    func setupCollectionViews() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 60, height: 60 )
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 42, bottom: 0, right: 42)
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        
        checkinCollectionView.backgroundColor = .clear
        checkinCollectionView.collectionViewLayout = layout
        checkinCollectionView.dataSource = self
        checkinCollectionView.delegate = self
        checkinCollectionView.register(UINib(nibName: "CheckinCheckoutCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CheckinCheckoutCell")
        checkinCollectionView.clipsToBounds = false
        checkinCollectionView.allowsMultipleSelection = false
        checkinCollectionView.showsHorizontalScrollIndicator = false
        checkinCollectionView.decelerationRate = UIScrollView.DecelerationRate.normal
        checkinCollectionView.selectItem(at: NSIndexPath(item: DatesManager.shared.checkinHours, section: 0) as IndexPath, animated: false, scrollPosition: .centeredHorizontally)
        
        checkoutCollectionView.backgroundColor = .clear
        checkoutCollectionView.collectionViewLayout = layout
        checkoutCollectionView.dataSource = self
        checkoutCollectionView.delegate = self
        checkoutCollectionView.register(UINib(nibName: "CheckinCheckoutCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CheckinCheckoutCell")
        checkinCollectionView.clipsToBounds = false
        checkoutCollectionView.allowsMultipleSelection = false
        checkoutCollectionView.showsHorizontalScrollIndicator = false
        checkinCollectionView.decelerationRate = UIScrollView.DecelerationRate.normal
        checkoutCollectionView.selectItem(at: NSIndexPath(item: DatesManager.shared.checkoutHours, section: 0) as IndexPath, animated: false, scrollPosition: .centeredHorizontally)
        
    }
    
    func preSelectedDates(){
        guard let checkin = firstDate, let checkout = lastDate else { return }

        var array: [Date] = []
        if checkin > checkout { array = [Date]() }
        var aux = checkin
        array = [aux]
        
        while aux <= checkout {
            array.append(aux)
            calendar.select(aux)
            aux = Calendar.current.date(byAdding: .day, value: 1, to: aux)!
        }
        
        var checkinWithHours = checkin.getCalendarFormatDate()
        checkinWithHours += checkin.getFormatHours(hour: DatesManager.shared.checkinHours)
        checkinDateLabel.text = checkinWithHours
        
        var checkoutWithHours = checkout.getCalendarFormatDate()
        checkoutWithHours += checkout.getFormatHours(hour: DatesManager.shared.checkoutHours)
        checkouDateLabel.text = checkoutWithHours
    }
}

extension CalendarViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hoursArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CheckinCheckoutCell", for: indexPath) as! CheckinCheckoutCollectionViewCell
        cell.setRow(hour: indexPath.row)
        
        if cell.isSelected {
            cell.selectedRow()
        } else {
            cell.deselectRow()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let cell = collectionView.cellForItem(at: indexPath) as! CheckinCheckoutCollectionViewCell
        if collectionView == self.checkinCollectionView {
            guard let start = firstDate else { return }
            //DatesManager.shared.checkinHours = indexPath.row
            checkinHours = indexPath.row
            var day = start.getCalendarFormatDate()
            day += "\(start.getFormatHours(hour: indexPath.row))"
            checkinDateLabel.text = day
            cell.isSelected = true
            checkinCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        } else {
            guard let end = lastDate else { return }
            //DatesManager.shared.checkoutHours = indexPath.row
            checkoutHours = indexPath.row
            var day = end.getCalendarFormatDate()
            day += "\(end.getFormatHours(hour: indexPath.row))"
            checkouDateLabel.text = day
            cell.isSelected = true
            checkoutCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        }
        cell.selectedRow()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
       
        guard let cell = collectionView.cellForItem(at: indexPath) as? CheckinCheckoutCollectionViewCell else { return }
        if collectionView == self.checkinCollectionView {
            checkinCollectionView.deselectItem(at: indexPath, animated: false)
        } else {
            checkoutCollectionView.deselectItem(at: indexPath, animated: false)
        }
        cell.isSelected = false
        cell.deselectRow()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.checkinCollectionView.scrollToNearestVisibleCollectionViewCell()
        self.checkoutCollectionView.scrollToNearestVisibleCollectionViewCell()
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.checkinCollectionView.scrollToNearestVisibleCollectionViewCell()
            self.checkoutCollectionView.scrollToNearestVisibleCollectionViewCell()
        }
    }
}

extension CalendarViewController: FSCalendarDelegate, FSCalendarDataSource  {
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "CELL", for: date, at: position)
        cell.frame.size = CGSize(width: cellSize, height: cellSize)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if firstDate == nil {
            firstDate = date
            lastDate = date
            checkinDateLabel.text = date.getCalendarFormatDate()  + date.getFormatHours(hour: DatesManager.shared.checkinHours)
            checkouDateLabel.text = date.getCalendarFormatDate()  + date.getFormatHours(hour: DatesManager.shared.checkoutHours)
            return
        }
        if date <= firstDate! {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            calendar.select(date)
            firstDate = date
            checkinDateLabel.text = date.getCalendarFormatDate() + date.getFormatHours(hour: DatesManager.shared.checkinHours)
            lastDate = date
            checkouDateLabel.text = date.getCalendarFormatDate()  + date.getFormatHours(hour: DatesManager.shared.checkoutHours)
            return
        } else {
            let range = viewModel.datesRange(from: firstDate!, to: date)
            if range.count > maxDateRange {
                for d in calendar.selectedDates {
                    calendar.deselect(d)
                }
                firstDate = nil
                lastDate = nil
                checkouDateLabel.text = ""
                checkinDateLabel.text = ""
            } else {
                lastDate = range.last
                checkouDateLabel.text = date.getCalendarFormatDate() + date.getFormatHours(hour: DatesManager.shared.checkoutHours)
                for d in range {
                    calendar.select(d)
                }
                return
            }
        }

    }

    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if firstDate != nil || lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            checkinDateLabel.text = ""
            checkouDateLabel.text = ""
            firstDate = nil
            lastDate = nil
            
        }
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return viewModel.setSelectableDate(days: 0)
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return viewModel.setSelectableDate(days: 45)
    }
}
