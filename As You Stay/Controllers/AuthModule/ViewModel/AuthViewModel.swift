//
//  AuthViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 7/17/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import GoogleSignIn

class AuthViewModel: NSObject, AuthViewModelProtocol {
    
    var googleDelegate: DidGoogleSigin?
   
    override init() {
        super.init()
    }
    
    func validate(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func validate(password: String) -> Bool {
        if password.isEmpty {
            return false
        }
        return true
    }
    
    func signin(username: String, password: String, completion: @escaping (Bool, String?) -> Void) {
        let authService = AuthService()
        authService.sigin(username: username, password: password) { (result) in
            switch result{
            case .success(let value):
                if value.statusCode == 0 {
                    let sessionObject = SessionObject()
                    sessionObject.date = Date()
                    sessionObject.token = value.data?.token ?? ""
                    sessionObject.userId = "\(String(describing: value.data?.id))"
                    sessionObject.sessionType = SessionTypes.email.rawValue
                    SessionService.store(session: sessionObject)
                    
                    completion(true, value.message)
                } else {
                    completion(false, value.message)
                }
                
            case .failure(let err):
                completion(false, err.localizedDescription)
            }
        }
    }
    
    func signup(firstName: String, lastName: String, password: String, email: String, completion: @escaping (Bool, String?) -> Void) {
        let authService = AuthService()
        authService.sigup(firstName: firstName,
                          lastName: lastName,
                          password: password,
                          email: email,
                          phone: "",
                          signupPromoCode: "",
                          deepLink: "",
                          marketingSrc: "",
                          udid: UIDevice.current.identifierForVendor!.uuidString,
                          googleToken: "",
                          fbUserId: "",
                          marketingTitle: "",
                          marketingAttrs: "",
                          afInstallData: "") { (result) in
            switch result{
            case .success(let value):
                if value.statusCode == 0 {
                    let sessionObject = SessionObject()
                    sessionObject.date = Date()
                    sessionObject.token = value.data?.token ?? ""
                    sessionObject.userId = "\(String(describing: value.data?.id) )"
                    sessionObject.sessionType = SessionTypes.email.rawValue
                    SessionService.store(session: sessionObject)
                    completion(true, nil)
                } else {
                    completion(false, value.message)
                }
                
            case .failure(let err):
                completion(false, err.localizedDescription)
            }
        }
    }
    
    func hasSession()-> Bool{
        return SessionService.exists()
    }
    
    func accessToken() -> String? {
        return TokenService.accessToken()
    }
    
    func remove() {
        guard SessionService.retrieve() != nil else { return }
        let dataProvider = DataProvider()
        dataProvider.clearAllData()
        GIDSignIn.sharedInstance().signOut()
    }
    
    func googleSignin(){
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    func forgotPassword(email: String, completion: @escaping(Bool, String?)->Void) {
        let authService = AuthService()
        authService.forgotPassword(email: email) { (result) in
            switch result {
            case .success(let value):
                if value.statusCode == 0 {
                    completion(true, value.message)
                } else {
                    completion(false, value.message)
                }
                
                
            case .failure(let err):
                completion(false, err.localizedDescription)
            }
        }
    }
}

// MARK: - Google sign in
extension AuthViewModel: GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let _ = error {
            googleDelegate?.didSigin(success: false)
        } else {
            let authService = AuthService()
            authService.googleTokenVerification(os: "iphone", email: user.profile.email, token: user.authentication.idToken) { (result) in
                switch result {
                case .success(let value):
                    if value.statusCode == 0 {
                        let sessionObject = SessionObject()
                        sessionObject.date = Date()
                        sessionObject.token = value.data?.token ?? ""
                        sessionObject.userId = "\(String(describing: value.data?.id))"
                        sessionObject.sessionType = SessionTypes.google.rawValue
                        SessionService.store(session: sessionObject)
                        self.googleDelegate?.didSigin(success: true)
                        
                    }else{
                        self.googleDelegate?.didSigin(success: false)
                    }
                    
                case .failure:
                    self.googleDelegate?.didSigin(success: false)
                }
            }
        }
    }
}
