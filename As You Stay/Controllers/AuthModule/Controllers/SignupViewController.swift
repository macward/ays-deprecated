//
//  SignupViewController.swift
//  As You Stay
//
//  Created by Max Ward on 13/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit
import WebKit
import GoogleSignIn

class SignupViewController: UIViewController {
        
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    fileprivate var viewModel = AuthViewModel()
    fileprivate var profileViewModel = ProfileViewModel()
    fileprivate var activityIndicator: ActivityIndicator!
    var delegate:completeRegistrationDelegate?
    
    weak var coordinator: MainCoordinator?
    
    lazy var webView: WKWebView = {
        let web = WKWebView.init(frame: UIScreen.main.bounds)
        let url = URL.init(string: "https://www.asyoustay.com/terms/")!
        let request = URLRequest.init(url: url)
        web.load(request)
        return web
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.shouldDismissKeyboard()
        self.setupTextfields()
        viewModel.googleDelegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
    }
    
    @IBAction func signupDidTap(_ sender: Any) {
        
        if validateTextFields() {
            activityIndicator = ActivityIndicator(view: view)
            self.activityIndicator.startAnimating()
            guard let firstName: String = self.firstNameTextField.text else { return }
            guard let lastName: String = self.lastNameTextField.text else { return }
            guard let password: String = self.passwordTextField.text else { return }
            guard let email: String = self.emailTextField.text else { return }
            
            viewModel.signup(firstName: firstName, lastName: lastName, password: password, email: email.lowercased()) { (success, message) in
                self.activityIndicator.stopAnimating()
                if success{
                    self.profileViewModel.store { (status, message) in
                        if status {
                            if let delegate = self.delegate {
                                self.dismiss(animated: true) {
                                    delegate.completedRegistretion()
                                }
                            } else {
                                let tabBarViewController = MainTabBarViewController()
                                tabBarViewController.modalPresentationStyle = .fullScreen
                                self.setRootViewController(tabBarViewController)
                            }
                        } else {
                            self.present(simpleAlert(title: "Error", message: message ?? "An error has occurred, try again"), animated: true, completion: nil)
                        }
                    }
                } else {
                    self.present(simpleAlert(title: "Error", message: message ?? "An error has occurred, try again"), animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func loginDidTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func termsDidTap(_ sender: Any) {
        let controller = UIViewController()
        controller.view.addSubview(self.webView)
        self.present(controller,animated: true)
    }
    
    @IBAction func googleSignupDidTap(_ sender: Any ) {
        self.viewModel.googleSignin()
    }
}

// MARK: - Setup text fields
extension SignupViewController {
    
    fileprivate func setupTextfields(){
        self.firstNameTextField.setCustomTextField(border: 0, image: UIImage(imageLiteralResourceName: "icon-user"), placeholder: "First name")
        self.lastNameTextField.setCustomTextField(border: 0, image: UIImage(imageLiteralResourceName: "icon-user"), placeholder: "Last name")
        self.emailTextField.setCustomTextField(border: 0, image: UIImage(imageLiteralResourceName: "icon-user"), placeholder: "Email")
        self.passwordTextField.setCustomTextField(border: 0, image: UIImage(imageLiteralResourceName: "icon-lock"), placeholder: "Password")
        
        self.firstNameTextField.addTarget(self.lastNameTextField, action: #selector(becomeFirstResponder), for: .editingDidEndOnExit)
        self.lastNameTextField.addTarget(self.emailTextField, action: #selector(becomeFirstResponder), for: .editingDidEndOnExit)
        self.emailTextField.addTarget(self.passwordTextField, action: #selector(becomeFirstResponder), for: .editingDidEndOnExit)
        self.passwordTextField.addTarget(nil, action: #selector(signupDidTap), for: .editingDidEndOnExit)
    }
}

// MARK: - Validate textfields
extension SignupViewController {
    
    fileprivate func validateTextFields() -> Bool {
        
        if !viewModel.validate(password: firstNameTextField.text!) {
            self.present(simpleAlert(title: "Error", message: "Please enter first name."), animated: true, completion: nil)
            return false
        }
        
        
        if !viewModel.validate(password: lastNameTextField.text!) {
            self.present(simpleAlert(title: "Error", message: "Please enter last name."), animated: true, completion: nil)
            return false
        }

        if !viewModel.validate(email: emailTextField.text!) {
            self.present(simpleAlert(title: "Error", message: "Please enter a valid email."), animated: true, completion: nil)
            return false
        }
        
        if !viewModel.validate(password: passwordTextField.text!) {
            self.present(simpleAlert(title: "Error", message: "Please enter a valid password."), animated: true, completion: nil)
            return false
        }
        
        return true
    }
}

// MARK: - Did google sign up
extension SignupViewController: DidGoogleSigin {
    
    func didSigin(success: Bool) {
        if success {
           
            self.profileViewModel.store { (status, message) in
                if status {
                    if let delegate = self.delegate {
                        self.dismiss(animated: true) {
                            delegate.completedRegistretion()
                        }
                    } else {
                        let tabBarViewController = MainTabBarViewController()
                        tabBarViewController.modalPresentationStyle = .fullScreen
                        self.setRootViewController(tabBarViewController)
                    }
                } else {
                    print(message ?? "Error DidGoogleSigin")
                }
            }
        }    
    }
}
