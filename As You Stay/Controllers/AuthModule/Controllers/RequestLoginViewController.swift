//
//  RequestLoginViewController.swift
//  As You Stay
//
//  Created by Max Ward on 13/03/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

enum SigninFrom: Int {
    case reservations = 2
    case profile = 4
    case rooms = -1
}

class RequestLoginViewController: UIViewController {
    
    weak var coordinator: ReservationsCoordinator?
    weak var profileCoordinator: ProfileCoordinator?
    weak var hotelCoordinator: HotelCoordinator!
    
    var viewModel: BookingViewModel = BookingViewModel()
    
//    var isReservatoin: Bool = true
//    var oneReceipt: Bool = false
    var activityIndicator: ActivityIndicator!
    
    var signinFrom: SigninFrom!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switchDescriptionViewController()
    }
    
    func switchDescriptionViewController(){
        switch signinFrom {
        case .profile:
            title =  "Profile"
            descriptionLabel.text = "You need to be logged in to see your profile"
            
        case .reservations:
            title =  "Reservations"
        case .rooms:
            title =  "Account required"
            descriptionLabel.text = "We're almost there. To continue with the reservation you need an account."
            
        case .none:
            print("none")
        }
    }
    @IBAction func signInButtonDidTap(_ sender: Any) {
        let navigationController = UINavigationController()
        let viewController = LoginViewController()
        viewController.completionSigninDelegate = self
        navigationController.pushViewController(viewController, animated: false)
        self.present(navigationController, animated: true, completion: nil)
    }
}

// MARK: - Reservation action
extension RequestLoginViewController {
    
    @objc fileprivate func showDownloadAlert(_ sender: UIBarButtonItem){
        setupImportReservationAlert()
    }
    
    fileprivate func addImportReservationNavigationButton(){
        if #available(iOS 13.0, *) {
            let iconImage = UIImage(systemName: "icloud.and.arrow.down")
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: iconImage,
                                                                     style: .plain,
                                                                     target: self,
                                                                     action: #selector(showDownloadAlert))
            self.navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.04705882353, green: 0.7607843137, blue: 0.9215686275, alpha: 1)
        }
    }
    
    fileprivate func setupImportReservationAlert() {
        let alert = UIAlertController(title: "Import Reservation", message: "If you have a reservatoin with us, you can inser the booking code that we send to your email.", preferredStyle: .alert)
        alert.view.layer.borderColor = UIColor(hexString: "#0CC2EB").cgColor
        alert.view.layer.borderWidth = 1
        alert.view.layer.cornerRadius = 10
        alert.setTitlet(font: UIFont.boldSystemFont(ofSize: 26), color: UIColor(hexString: "#0CC2EB"))
        alert.setMessage(font: UIFont(name: "System Bold", size: 18), color: UIColor(hexString: "#0CC2EB"))
        alert.setBackgroundColor(color: UIColor(hexString: "#1F232A"))
        alert.setTint(color: UIColor(hexString: "#0CC2EB"))
        
        alert.addTextField { (textfield) in
            textfield.attributedPlaceholder = NSAttributedString(string: "Reservation code",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            textfield.textColor = UIColor(hexString: "#0CC2EB")
            for view: UIView in alert.textFields! {
                let container: UIView = view.superview!
                let effectView: UIView = container.superview!.subviews[0]
                container.backgroundColor = UIColor(red: 43/256, green: 47/256, blue: 58/256, alpha: 1)
                effectView.removeFromSuperview()
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let submitAction = UIAlertAction(title: "Send", style: .default) { (action) in
            if let bookingId = alert.textFields?[0].text {
                self.activityIndicator.startAnimating()
                self.viewModel.getReceipt(bookingID: bookingId) { (message, receipt) in
                    self.activityIndicator.stopAnimating()
                    if let receipt = receipt {
                        //FIXME: - storage locally and show it
                    } else {
                        self.present(simpleAlert(title: "Error", message: message ?? "Submit erro."), animated: true, completion: nil)
                    }
                }
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(submitAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension RequestLoginViewController: CompletionSigninDelegate {
    func completedSignin() {
        switch signinFrom {
        case .rooms:
            self.hotelCoordinator?.booking(dro: self.viewModel.bookingDRO)
        default:
            let data:[String: Int] = ["index": signinFrom.rawValue]
            let notificationName = Notification.Name(rawValue: K.NotificationKeys.refreshMainTabBar)
            NotificationCenter.default.post(name:  notificationName, object: nil, userInfo: data)
        }
    }
}

