//
//  LoginViewController.swift
//  As You Stay
//
//  Created by Max Ward on 7/15/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import PinLayout
import AssistantKit
import GoogleSignIn



class LoginViewController: UIViewController{
    
    weak var coordinator: MainCoordinator?
    var activityIndicator: ActivityIndicator!
    var delegate: completeRegistrationDelegate?
    var completionSigninDelegate: CompletionSigninDelegate?
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var googleButton: UIButton!
//    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var dontHaveAnAccountLabel: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    
    var textFieldsViewContainer = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var socialNetStackView = UIStackView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var signupStackView = UIStackView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var underLineView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var multiplier: CGFloat = 1.0
    var authViewModel = AuthViewModel()
    var profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        shouldDismissKeyboard()
        authViewModel.googleDelegate = self
//        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
        navigationController?.transparentBar()
        self.view.backgroundColor = UIColor.black
        self.view.addSubview(textFieldsViewContainer)
        self.view.addSubview(underLineView)
        setMultiplier()
        userTextField.text = "testapp@asyoustay.com"
        passwordTextField.text = "testmeup!"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addConstraints()
    }

    @IBAction func didTapSigninButton(_ sender: Any) {
        guard let txtEmail = userTextField.text, let txtPassword = passwordTextField.text else { return }
        setupActivityIndicator()
        let emailIsValid = authViewModel.validate(email: txtEmail)
        if !emailIsValid {
            present(simpleAlert(title: "Error", message: "Invalid mail!"), animated: true, completion: nil)
            return
        }
        
        disabledTouchActions()
        self.activityIndicator.startAnimating()
        
        authViewModel.signin(username: txtEmail, password: txtPassword, completion: { (status, message) in
            if status {
                self.enabledTouchActions()
                
                self.profileViewModel.store { (status, message) in
                    if status {
                        self.dismiss(animated: true) {
                            self.activityIndicator.stopAnimating()
                            self.delegate?.completedRegistretion()
                            self.completionSigninDelegate?.completedSignin()
                        }
                    } else  {
                        self.activityIndicator.stopAnimating()
                        self.present(simpleAlert(title: "Error", message: message ?? "signin error"), animated: true)
                    }
                }
                
            } else {
                self.enabledTouchActions()
                self.activityIndicator.stopAnimating()
                self.present(simpleAlert(title: "Error", message: message ?? "An error has occurred, try again"), animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func didTapForgotPasswordButton(_ sender: Any) {
        configureResetPasswordAlert()
    }
    
    @IBAction func didTapGoogleSiginButton(_ sender: Any) {
        googleSignin()
    }
    
    @IBAction func didTapSignupButton(_ sender: Any) {
        let signupViewController = SignupViewController()
        signupViewController.delegate = self.delegate
        self.navigationController?.pushViewController(signupViewController, animated: true)
    }
    /**
     If open session, redirect to home view controller.
     */
    func handleSession(){
        if authViewModel.hasSession() {
            let tabBarViewController = MainTabBarViewController()
            self.setRootViewController(tabBarViewController)
        }
    }
    
    /**
     Add all the constraints for the auto layout
     */
    func addConstraints(){
        let marginTopTextFieldContainer: CGFloat = 146 * multiplier
        let marginTopLogo: CGFloat = 146 * multiplier
        let marginBottomSignUpButton: CGFloat = 73 * multiplier
        
        backgroundImage.contentMode = .top
        backgroundImage.pin.left(0).top(0).right(0).height(50%)
        logoImage.pin
            .left(18.59%)
            .top(marginTopLogo)
            .right(17.19%)
        
        textFieldsViewContainer.backgroundColor = UIColor.white
        textFieldsViewContainer.pin
            .left(4%)
            .below(of: logoImage)
            .right(4%)
            .height(170)
            .marginTop(marginTopTextFieldContainer)
        
        userTextField.setCustomTextField(border: 0, image: UIImage(imageLiteralResourceName: "icon-user"), placeholder: "User name")
        passwordTextField.setCustomTextField(border: 0, image: UIImage(imageLiteralResourceName: "icon-lock"), placeholder: "Password")
        
        underLineView.backgroundColor = UIColor(red: 234/256, green: 239/256, blue: 240/256, alpha: 1)
        
        textFieldsViewContainer.addSubview(userTextField)
        textFieldsViewContainer.addSubview(passwordTextField)
        textFieldsViewContainer.addSubview(underLineView)
        
        userTextField.pin
            .left(8%)
            .top(12.94%)
            .height(50)
            .right(8%)
        
        passwordTextField.pin
            .left(8%)
            .bottom(13.52%)
            .height(50)
            .right(8%)
        
        underLineView.pin
            .left(21%)
            .top(50%)
            .right(1%)
            .height(1)
    
        forgotPasswordButton.titleLabel?.textColor = UIColor.white
        forgotPasswordButton.pin
            .below(of: textFieldsViewContainer)
            .right(4%)
            .marginTop(2%)
        
        signupStackView.pin
            .bottom(marginBottomSignUpButton)
            .hCenter()
            .marginTop(2.5%)
        
        signupStackView.spacing = 5.0
        signupStackView.axis = .horizontal
        signupStackView.distribution = .equalSpacing
        signupStackView.alignment = .center
        
        signupStackView.translatesAutoresizingMaskIntoConstraints = false;
        signupStackView.addArrangedSubview(dontHaveAnAccountLabel)
        signupStackView.addArrangedSubview(signupButton)
        self.view.addSubview(signupStackView)
        
        //facebookButton.setImage(UIImage(named: "fb-btn"), for: .normal)
        googleButton.setImage(UIImage(named: "g-btn"), for: .normal)
        
        socialNetStackView.pin
            .above(of: signupStackView)
            .height(50)
            .hCenter()
            .marginBottom(4%)
        
        socialNetStackView.spacing = 20.0
        socialNetStackView.axis = .horizontal
        socialNetStackView.distribution = .fillEqually
        socialNetStackView.alignment = .center
        
        socialNetStackView.translatesAutoresizingMaskIntoConstraints = false;
//        socialNetStackView.addArrangedSubview(facebookButton)
        socialNetStackView.addArrangedSubview(googleButton)
        self.view.addSubview(socialNetStackView)
        
        signInButton.pin
            .above(of: socialNetStackView)
            .left(4%)
            .right(4%)
            .marginBottom(5%)
            .height(60)
        
        signInButton.backgroundColor =  UIColor(red: 12/256, green: 194/256, blue: 235/256, alpha: 1.0)
        signInButton.setTitleColor(UIColor.white, for: .normal)
        signInButton.setTitle("Sign in", for: .normal)
        
    }
    
    /**
    Set up a multiplier based on the device screen for the auto layout
     */
    func setMultiplier() {
        
        switch Device.screen.family.rawValue {
        case "old":
            multiplier = 0.25
        case "small":
            multiplier = 0.37
        case "medium":
            switch Device.screen.rawValue {
            case 6.5:
                multiplier = 1
            case 6.1:
                multiplier = 0.93
            case 5.8:
                multiplier = 0.80
            default:
                multiplier = 0.60
            }
        default:
            print("")
        }
    }
    
    /**
    Disable touch screen
     */
    func disabledTouchActions(){
         self.view.isUserInteractionEnabled = false
    }
    
    /**
     Enable touch screen
     */
    func enabledTouchActions(){
         self.view.isUserInteractionEnabled = true
    }
    
    /**
     Sign in with google credentials
     */
    func googleSignin(){
        authViewModel.googleSignin()
    }
    
    func configureResetPasswordAlert() {
        let alert = UIAlertController(title: "Reset", message: "If you have an account with us, we will send you a passowrd reset form link by email", preferredStyle: .alert)
        alert.view.layer.borderColor = UIColor(hexString: "#0CC2EB").cgColor
        alert.view.layer.borderWidth = 1
        alert.view.layer.cornerRadius = 10
        alert.setTitlet(font: UIFont.boldSystemFont(ofSize: 26), color: UIColor(hexString: "#0CC2EB"))
        alert.setMessage(font: UIFont(name: "System Bold", size: 18), color: UIColor(hexString: "#0CC2EB"))
        alert.setBackgroundColor(color: UIColor(hexString: "#1F232A"))
        alert.setTint(color: UIColor(hexString: "#0CC2EB"))
        
        alert.addTextField { (textfield) in
            textfield.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            textfield.textColor = UIColor(hexString: "#0CC2EB")
            for view: UIView in alert.textFields! {
                let container: UIView = view.superview!
                let effectView: UIView = container.superview!.subviews[0]
                container.backgroundColor = UIColor(red: 43/256, green: 47/256, blue: 58/256, alpha: 1)
                effectView.removeFromSuperview()
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let submitAction = UIAlertAction(title: "Send", style: .default) { (action) in
            self.activityIndicator.startAnimating()
            if let mail = alert.textFields?[0].text {
                if self.authViewModel.validate(email: mail) {
                    self.authViewModel.forgotPassword(email: mail) { (status, message) in
                        if status {
                            self.activityIndicator.stopAnimating()
                            let alert = self.configureAlert(title: "Success", message: "We sent you an email with instructions on how to reset your password.", borderColor: "#3fb91c", borderWidth: 1, borderRadius: 5, titleColor: "#3fb91c", messageColor: "#3fb91c", backgroundColor: "#1F232A", tintColor: "#3fb91c")
                            self.present(alert, animated: true)
                        } else {
                            self.activityIndicator.stopAnimating()
                            
                            let alert = self.configureAlert(title: "Error", message: message ?? "Something went wrong", borderColor: "#d80000", borderWidth: 1, borderRadius: 5, titleColor: "#d80000", messageColor: "#d80000", backgroundColor: "#1F232A", tintColor: "#d80000")
                            
                            self.present(alert, animated: true)
                        }
                    }
                } else {
                    self.activityIndicator.stopAnimating()
                }
            } else {
                self.activityIndicator.stopAnimating()
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(submitAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupActivityIndicator() {
        activityIndicator = ActivityIndicator(view: view)
    }
    
    func configureAlert(title: String, message: String, borderColor: String, borderWidth: CGFloat, borderRadius: CGFloat, titleColor: String, messageColor: String, backgroundColor: String, tintColor: String) -> UIAlertController {
        
        let alert = simpleAlert(title: title, message: message)
        alert.view.layer.borderColor = UIColor(hexString: borderColor).cgColor
        alert.view.layer.borderWidth = borderWidth
        alert.view.layer.cornerRadius = borderRadius
        alert.setTitlet(font: UIFont.boldSystemFont(ofSize: 26), color: UIColor(hexString: titleColor))
        alert.setMessage(font: UIFont(name: "System Bold", size: 18), color: UIColor(hexString: messageColor))
        alert.setBackgroundColor(color: UIColor(hexString: backgroundColor))
        alert.setTint(color: UIColor(hexString: tintColor))
        
        return alert
    }
}

extension LoginViewController: DidGoogleSigin {
    func didSigin(success: Bool) {
        if success {
            self.profileViewModel.store { (status, message) in
                //self.activityIndicator.stopAnimating()
                if status {
                    self.dismiss(animated: true) {
                        self.activityIndicator.stopAnimating()
                        self.delegate?.completedRegistretion()
                        self.completionSigninDelegate?.completedSignin()
                    }
                } else  {
                    self.activityIndicator.stopAnimating()
                    self.present(simpleAlert(title: "Error", message: message ?? "signin error"), animated: true)
                }
            }           
        } else {
           // self.activityIndicator.stopAnimating()
            self.present(simpleAlert(title: "Error", message: "Google signin error"), animated: true)
        }
    }
}
