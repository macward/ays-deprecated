//
//  MapViewController.swift
//  As You Stay
//
//  Created by Max Ward on 13/08/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapViewController: UIViewController {

    var coordinator: HotelCoordinator?
    var viewModel = HotelResultsViewModel()
    
    @IBOutlet weak var checkinView: UIView!
    @IBOutlet weak var checkoutView: UIView!
    @IBOutlet weak var checkinLabel: UILabel!
    @IBOutlet weak var checkoutLabel: UILabel!
    @IBOutlet weak var nightsLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var switchCollectionView: UIView!
    
    var vCollectionView: UICollectionView!
    var collectionView: UICollectionView!
    var verticalCollection: Bool = false
    
    var activityIndicator: ActivityIndicator!
    
    var hasLocation: Bool = false
    
    lazy var mapContainerView: UIView = {
        let mView = UIView(frame: view.bounds)
        return mView
    }()
    
    @IBOutlet weak var infoView: UIView! {
        didSet {
            //self.infoView.layer.cornerRadius = 5
            self.infoView.roundCorners(corners: [.topLeft, .topRight], radius: 5)
            self.infoView.addShadow()
        }
    }
    
    var mapView = GMSMapView()
    var locationManager = CLLocationManager()
    
    fileprivate var infoViewTopConstant: CGFloat = 20
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupNavigationBar()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        DatesManager.shared.checkin = viewModel.addingDaysToDate(days: 2, currentDate: Date())
        DatesManager.shared.checkout = viewModel.addingDaysToDate(days: 4, currentDate: Date())
        DatesManager.shared.checkinHours = 15
        DatesManager.shared.checkoutHours = 11
        
        
        nightsLabel.text = "\(viewModel.getNumberOfNights()) Nights"
        
        let checkin = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)
        let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)
        checkinLabel.text = checkin?.getMapFormatDate()
        checkoutLabel.text = checkout?.getMapFormatDate()
        locationLabel.text = "--"
        
        viewModel.setDateTime(checkin: checkin!, checkout: checkout!)
        viewModel.centerCell = self.centerCell
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
                
        setupMapView()
        
        let checkinViewTap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
        checkinView.isUserInteractionEnabled = true
        checkinView.addGestureRecognizer(checkinViewTap)
        
        let checkoutViewTap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
        checkoutView.isUserInteractionEnabled = true
        checkoutView.addGestureRecognizer(checkoutViewTap)
        
        setupActivityIndicator()
        setupCollectionView()
        setupVCollectionView()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        switchCollectionView.addGestureRecognizer(tap)
        switchCollectionView.isUserInteractionEnabled = true
        switchCollectionView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 5)
        
//        makeHotelRequest()
        
        viewModel.isSearching.addAndNotify(observer: self) { [unowned self] in
            if self.viewModel.isSearching.value == true {
                self.collectionView.isHidden = true
            }
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if verticalCollection {
            verticalCollection = false
        } else  {
            verticalCollection = true
        }
        reloadCollectionView(vertical: verticalCollection)
        
    }
    
    override func viewDidLayoutSubviews() {
         mapView.frame = view.bounds
    }
    
    func setupMapView(){
        let camera = GMSCameraPosition.camera(withLatitude: 40.73, longitude: -73.93, zoom: 14)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.setMinZoom(12, maxZoom: 20)
        mapView.frame = view.bounds
        mapContainerView.addSubview(mapView)
        mapView.delegate = viewModel
        view.insertSubview(mapContainerView, at: 0)
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func setupActivityIndicator () {
        activityIndicator = ActivityIndicator(view: view)
    }
    
    func setupCollectionView(){
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)

        collectionView.backgroundColor = .clear
        self.view.addSubview(collectionView)
        collectionView.isHidden = true
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.decelerationRate = UIScrollView.DecelerationRate.normal
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -8).isActive = true
        collectionView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        collectionView.register(UINib(nibName: "HotelsCollectionViewCellNew", bundle: nil), forCellWithReuseIdentifier: "HotelCellNew")
    }
    
    func setupVCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        vCollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)

        vCollectionView.backgroundColor = .clear
        self.view.addSubview(vCollectionView)
        self.vCollectionView.isHidden = true
        vCollectionView.dataSource = self
        vCollectionView.delegate = self
        vCollectionView.decelerationRate = UIScrollView.DecelerationRate.normal
        vCollectionView.showsHorizontalScrollIndicator = false
        
        vCollectionView.translatesAutoresizingMaskIntoConstraints = false
        vCollectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        vCollectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        vCollectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -8).isActive = true
        vCollectionView.topAnchor.constraint(equalTo: switchCollectionView.bottomAnchor, constant: 8).isActive = true
        vCollectionView.register(UINib(nibName: "HotelsCollectionViewCellNew", bundle: nil), forCellWithReuseIdentifier: "HotelCellNew")
        
        
    }
    func reloadCollectionView(vertical: Bool) {
        
        switch vertical {
            
        case true:
            UIView.animate(withDuration: 1.0) {
                self.collectionView.alpha = 0
                self.vCollectionView.alpha = 1.0
                self.collectionView.isHidden = true
                self.vCollectionView.isHidden = false
            }
            self.activityIndicator.stopAnimating()
            self.vCollectionView.reloadData()
        
        case false:
            UIView.animate(withDuration: 1.0) {
                self.collectionView.alpha = 1
                self.vCollectionView.alpha = 0
                self.collectionView.isHidden = false
                self.vCollectionView.isHidden = true
            }

             self.activityIndicator.stopAnimating()
             self.collectionView.reloadData()
             self.collectionView.scrollToNearestVisibleCollectionViewCell()
        }
    }
    
    func makeHotelRequest() {
        activityIndicator.startAnimating()
        viewModel.selectedIndex = 0
        viewModel.searchHotelsByLocation { (response) in
            if response {
                self.viewModel.createHotelMarkerList(mapView: self.mapView)
                if self.viewModel.hotelArray.count > 0 {
                    self.reloadCollectionView(vertical: self.verticalCollection)
                } else {
                    self.activityIndicator.stopAnimating()
                    self.present(simpleAlert(title: "Warning", message: "No hotels found"), animated: true, completion: nil)
                }
            } else {
                self.activityIndicator.stopAnimating()
                self.present(simpleAlert(title: "Warning", message: "No hotels found"), animated: true, completion: nil)
                
            }
        }
    }
    func makeMoreHotelRequest() {
        activityIndicator.startAnimating()
        viewModel.moreHotelsByLocation { (response) in
            if response {
                self.viewModel.createHotelMarkerList(mapView: self.mapView)
                if self.viewModel.hotelArray.count > 0 {
                    self.reloadCollectionView(vertical: self.verticalCollection)
                } else {
                    self.activityIndicator.stopAnimating()
                    self.present(simpleAlert(title: "Warning", message: "No more hotels found"), animated: true, completion: nil)
                }
            } else {
                self.activityIndicator.stopAnimating()
                self.present(simpleAlert(title: "Warning", message: "No more hotels found"), animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func didTapSearchLocation(_ sender: Any) {
        let viewController = SearchLocationViewController(nibName: nil, bundle: nil)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        viewController.selectedLocationDelegate = self
        viewController.modalPresentationStyle = .overCurrentContext
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func didTapNightButton(_ sender: Any) {
        showCalendarViewController()
    }
    
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        showCalendarViewController()
    }
    
    func showCalendarViewController() {
        
        let calendarViewController = CalendarViewController()
        calendarViewController.didSelectedDateDelegate = self
        calendarViewController.firstDate = DatesManager.shared.checkin
        calendarViewController.lastDate = DatesManager.shared.checkout
        calendarViewController.checkinHours  = DatesManager.shared.checkinHours
        calendarViewController.checkoutHours = DatesManager.shared.checkoutHours
        
        present(calendarViewController, animated: true)
    }
}

extension MapViewController: SelectedLocationDelegate {
    
    func didSelectLocation(address: Address) {
        viewModel.setAddress(address: address)
        DispatchQueue.main.async {
            self.makeHotelRequest()
        }
    }
    
    func didSelectLocation(location: String) {
        locationLabel.text = location
    }
}

// MARK: - UICollectionViewDataSource
extension MapViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.hotelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HotelCellNew", for: indexPath) as! HotelsCollectionViewCellNew
        let hotelRow = viewModel.hotelArray[indexPath.row]
        cell.setupRowHotel(hotel: hotelRow)
        viewModel.getImageHotel(url: hotelRow.photo.url, completion: { (image) in
            cell.hotelImage.image = image
        })
        return cell
    }
}

// MARK: - Calendar delegate
extension MapViewController: DidSelectedDateDelegate {
    
    func didSelectedDate(checkin: Date, checkout: Date, checkinHours: Int, checkoutHours: Int) {
        
        DatesManager.shared.checkin = checkin
        DatesManager.shared.checkout = checkout
        DatesManager.shared.checkinHours = checkinHours
        DatesManager.shared.checkoutHours = checkoutHours
        
        viewModel.removeAllMarkers(mapView: mapView)
        
        let checkinDate = checkin.combineDateWithHours(hours: checkinHours)
        let checkoutDate = checkout.combineDateWithHours(hours:checkoutHours)
        checkinLabel.text = checkinDate?.getMapFormatDate()
        checkoutLabel.text = checkoutDate?.getMapFormatDate()
        
        nightsLabel.text = "\(viewModel.getNumberOfNights()) Nights"
        
        viewModel.setDateTime(checkin: checkinDate!, checkout: checkoutDate!)
        
        makeHotelRequest()
    }
}

// MARK: - Location Manager delegate
extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let currentLocation = locations.last?.coordinate else { return }
        
        locationManager.stopUpdatingLocation()
        if !hasLocation {
            self.viewModel.setCoords(location: currentLocation)
            updateLocationUI(location: currentLocation)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        } else {
            makeHotelRequest()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func updateLocationUI (location: CLLocationCoordinate2D) {
        
        let geodecoder = GMSGeocoder()
        hasLocation = true
        geodecoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)) { (geocodeResponse, err) in
            guard let address = geocodeResponse?.firstResult() else { return }
            self.viewModel.setAddress(address: address)
            self.locationLabel.text = address.locality
            self.makeHotelRequest()
        }
    }
}

// MARK: - UICollectionView Delegate FlowLayout
extension MapViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         
        guard let checkin  = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)?.getFormatToRequest() else {
            return }
        guard let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)?.getFormatToRequest() else {
            return
        }
        
        activityIndicator.startAnimating()
        
        let hotelId = self.viewModel.hotelArray[indexPath.row].id
        
        viewModel.getHotelDetails(hotel_id: hotelId, checkin_datetime: checkin, checkout_datetime: checkout, guests: 2) { [unowned self] (response) in
            
            guard let hotelDetails = response else {
                self.coordinator?.errorAlert(ref: self)
                self.activityIndicator.stopAnimating()
                return
            }
            
            self.activityIndicator.stopAnimating()
            self.coordinator?.details(hotel: hotelDetails)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if verticalCollection {
            return CGSize(width: self.view.bounds.width - (self.view.bounds.width - self.infoView.bounds.width), height: 140)
        } else {
             return CGSize(width: self.view.bounds.width - 60, height: 140)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.collectionView.scrollToNearestVisibleCollectionViewCell { (indexPath) in
            let hotelRow = self.viewModel.hotelArray[indexPath.row]
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                if !self.verticalCollection {
                    self.viewModel.selectMarker(self.viewModel.hotelMarkers[indexPath.row])
                    self.centerMapLocation(lat: hotelRow.lat, lng: hotelRow.lng)
                }
               
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !self.verticalCollection {
            if !decelerate {
                self.collectionView.scrollToNearestVisibleCollectionViewCell()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if viewModel.hotelArray.count - 2 == indexPath.row {
            viewModel.setNextPage()
            makeMoreHotelRequest()
        }
    }
}

// MARK: - Center
extension MapViewController {
    
    func centerMapLocation(lat: Float, lng: Float){
        let lat = CLLocationDegrees(lat)
        let lng = CLLocationDegrees(lng)
        let googleMapCoord: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let camera = GMSCameraUpdate.setTarget(googleMapCoord, zoom: viewModel.zoom)
        mapView.animate(with: camera)
    }
    
    func centerCell(_ hotel: HotelResponse, _ index: Int){
        collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: true)
    }
}
