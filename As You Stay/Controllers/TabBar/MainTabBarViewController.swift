//
//  MenuTabBarViewController.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    var mainCoordinator: MainCoordinator?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBar()
        if SessionService.exists() {
            updateProfile()
        }
        addObservers()
    }
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTabBar(notification:)), name: Notification.Name(rawValue: K.NotificationKeys.refreshMainTabBar), object: nil)
    }
    
    @objc func refreshTabBar(notification: NSNotification) {
//        let indexViewController = notification.userInfo!["index"] as! Int
        self.refreshSession()
    }

    
    fileprivate func updateProfile(){
        let profileViewModel = ProfileViewModel()
        profileViewModel.store { (status, message) in
            if status {
                print("Profile stored")
            } else {
                print(message ?? "Error updateProfile")
            }
        }
    }
    
    func setupTabBar(index: Int = 0) {
        
        let hotelCoordinator = MainCoordinator(navigationController: UINavigationController())
        hotelCoordinator.map()
        hotelCoordinator.navigationController.tabBarItem = UITabBarItem(title: "Search", image: UIImage(named: "icon-ays"), tag: 0)
        
        let bookmarksCoordinator = MainCoordinator(navigationController: UINavigationController())
        bookmarksCoordinator.bookmarks()
        bookmarksCoordinator.navigationController.tabBarItem = UITabBarItem(title: "Bookmark", image: UIImage(named: "icon-bookmarks"), tag: 1)
        
        let reservationsCoordinator = MainCoordinator(navigationController: UINavigationController())
        reservationsCoordinator.reservations()
        reservationsCoordinator.navigationController.tabBarItem = UITabBarItem(title: "Reservations", image: UIImage(named: "icon-reservations"), tag: 2)

        let notificationsCoordinator = MainCoordinator(navigationController: UINavigationController())
        notificationsCoordinator.notifications()
        notificationsCoordinator.navigationController.tabBarItem = UITabBarItem(title: "Notifications", image: UIImage(named: "icon-bell"), tag: 3)
        
        let profileCoordinator = MainCoordinator(navigationController: UINavigationController())
        profileCoordinator.profile()
        profileCoordinator.navigationController.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "icon-profile"), tag: 4)
        
        let tabBarList = [hotelCoordinator.navigationController,
                          bookmarksCoordinator.navigationController,
                          reservationsCoordinator.navigationController,
                          notificationsCoordinator.navigationController,
                          profileCoordinator.navigationController]
        
        self.viewControllers = tabBarList
        self.selectedIndex = index
    }
    
    func refreshSession(){
        if SessionService.exists() {
            
            let reservationsCoordinator = MainCoordinator(navigationController: UINavigationController())
            reservationsCoordinator.reservations()
            reservationsCoordinator.navigationController.tabBarItem = UITabBarItem(title: "Reservations", image: UIImage(named: "icon-reservations"), tag: 2)
            self.viewControllers?[2] = reservationsCoordinator.navigationController
            
            let profileCoordinator = MainCoordinator(navigationController: UINavigationController())
                  profileCoordinator.profile()
                  profileCoordinator.navigationController.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "icon-profile"), tag: 4)
            
            self.viewControllers?[4] = profileCoordinator.navigationController

        }
    }
}
