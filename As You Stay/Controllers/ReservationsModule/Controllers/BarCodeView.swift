//
//  BarCodeView.swift
//  As You Stay
//
//  Created by Max Ward on 26/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class BarCodeView: UIView {

    @IBOutlet weak var barCodeImage: UIImageView!
    @IBOutlet weak var barCodeLabel: UILabel!
    
    static func configure() -> BarCodeView {
        let view: BarCodeView = initFromNib()
        return view
    }
    
    static func configure(code: String) -> BarCodeView {
        let view: BarCodeView = initFromNib()
        view.barCodeLabel.text = code
        guard let barcode = UIImage(barcode: code) else { return view }
        view.barCodeImage.image = barcode
        return view
    }
    
    func config(code: String) {
        self.barCodeLabel.text = code
        if let barcode = UIImage(barcode: code){
            self.barCodeImage.image = barcode
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addCut()
        drawDotLine()
    }
    
    fileprivate func addCut() {
        let leftCenter = CGPoint(x: self.bounds.minX, y: self.bounds.minY + 18)
        let rightCenter = CGPoint(x: self.bounds.maxX, y: self.bounds.minY + 18)
        
        let path = UIBezierPath(rect: self.bounds)
        path.addArc(withCenter: leftCenter, radius: 16, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        path.move(to: rightCenter)
        path.addArc(withCenter: rightCenter, radius: 16, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        
        let mask = CAShapeLayer()
        mask.fillRule = .evenOdd
        mask.path = path.cgPath

        layer.mask = mask
    }
    
    func drawDotLine(){
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.gray.cgColor
        shapeLayer.fillRule = .evenOdd
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [4,2]
        
        let path = CGMutablePath()
        path.move(to: CGPoint(x: self.bounds.minX + 17, y: self.bounds.minY + 18))
        path.addLine(to: CGPoint(x: self.bounds.maxX - 17, y: self.bounds.minY + 18))
        
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
}
