//
//  ReservationsViewController.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class ReservationsViewController: UIViewController {
    
    fileprivate var collectionView: UICollectionView!
    var coordinator: ReservationsCoordinator?
    var viewModel = ReservationsViewModel()
    var activityIndicator: ActivityIndicator!
    var oneReceipt: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Reservations"
        setupCollectionView()
        setupActivityIndicator()
    }
    
    override func viewDidLayoutSubviews() {
        activityIndicator.backgroundView.frame = self.view.bounds
        activityIndicator.view?.frame = self.view.bounds
        activityIndicator.animationView.center = self.view.center
    }
    
    fileprivate func setupCollectionView() {
        
        let offsetTop = (navigationController?.navigationBar.frame.size.height) ?? 0 + statusBarHeight()
        let offsetBottom = (tabBarController?.tabBar.frame.size.height) ?? 0
        
        let layout  = WLCollectionViewLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: self.view.bounds.width - 84, height: self.view.bounds.height - offsetBottom - offsetTop - 84)
        layout.minimumLineSpacing = 10
        
        collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collectionView.contentInset = UIEdgeInsets(top: 42, left: 42, bottom: 42, right: 42)
        collectionView.isPagingEnabled = false
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.view.addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ReservationsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ReservationCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        let offsetTop = (navigationController?.navigationBar.frame.size.height) ?? 0 + statusBarHeight()
        let offsetBottom = (tabBarController?.tabBar.frame.size.height) ?? 0
        
        let layout  = WLCollectionViewLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: self.view.bounds.width - 84, height: self.view.bounds.height - offsetBottom - offsetTop - 84)
        layout.minimumLineSpacing = 10
        collectionView.contentInset = UIEdgeInsets(top: 42, left: 42, bottom: 42, right: 42)
        collectionView.collectionViewLayout = layout
        
        checkOneReservation()
        
    }
    
    func statusBarHeight() -> CGFloat {
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        return Swift.min(statusBarSize.width, statusBarSize.height)
    }
    
    override func viewWillLayoutSubviews() {
        collectionView.layoutSubviews()
    }
    
    func getReceipts() {
        viewModel.getReceipts { (message) in
            self.activityIndicator.stopAnimating()
            if let message = message {
                self.present(simpleAlert(title: "error", message: message), animated: true)
            } else {
                self.collectionView.reloadData()
            }
            
        }
    }
    
    func checkOneReservation(){
        activityIndicator.startAnimating()
        if oneReceipt {
            oneReceipt = false
            activityIndicator.stopAnimating()
        } else {
            getReceipts()
        }
    }
    
    func setupActivityIndicator () {
        activityIndicator = ActivityIndicator(view: view)
    }
}

extension ReservationsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReservationCell", for: indexPath) as! ReservationsCollectionViewCell
        cell.configure(using: viewModel.list[indexPath.row])
        return cell
    }
}
