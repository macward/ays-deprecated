//
//  OneDayCalendarView.swift
//  As You Stay
//
//  Created by Max Ward on 21/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit

class OneDayCalendarView : UIView, Identifiable {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    
    static func configure() -> OneDayCalendarView {
        let view: OneDayCalendarView = initFromNib()
        return view
    }
    
    static func configure(month: String, day: String, hour: String) -> OneDayCalendarView {
        let view: OneDayCalendarView = initFromNib()
        view.monthLabel.text = month
        view.dayLabel.text = day
        view.hourLabel.text = hour
        view.addShadow()
        return view
    }
}
