//
//  WLCollectionViewLayout.swift
//  As You Stay
//
//  Created by Max Ward on 03/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation
import UIKit

protocol DidSelectedHotel {
    func selectedHotel(index: Int) -> Void
}

class WLCollectionViewLayout: UICollectionViewFlowLayout {
    var previousOffset: CGFloat = 0
    var currentItem: Int = 0
    var delegate: DidSelectedHotel?
    
    override func prepare() {
    }
    /*
            *
               |<-------     view width     -------->|
               ---------------------------------------
               |                                     |
               |<-w0->|<ws>|<--   wi  -->|<ws>|<-w0->|
        ---------------    ---------------    ---------------
        |             |    |             |    |             |
        |             |    |             |    |             |
        ---------------    ---------------    ---------------
               |                                     |
               |                                     |
               ---------------------------------------
            *
            * w0: itemEdgeOffset
            * ws: space
            * wi: itemWidth
    */
    //Returns the point at which to stop scrolling.
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {

        guard let collectionView = self.collectionView else {
            return CGPoint.zero
        }
        
        guard let itemsCount = collectionView.dataSource?.collectionView(collectionView, numberOfItemsInSection: 0) else {
            return CGPoint.zero
        }
        
        if ((previousOffset > collectionView.contentOffset.x) && (velocity.x < 0)) {
            currentItem = max(currentItem - 1, 0)
        } else if ((previousOffset < collectionView.contentOffset.x) && (velocity.x > 0.0)) {
            currentItem = min(currentItem + 1, itemsCount - 1);
        }
        let itemEdgeOffset:CGFloat = (collectionView.frame.width - itemSize.width -  minimumLineSpacing * 2) / 2
        let updatedOffset: CGFloat = (itemSize.width + minimumLineSpacing) * CGFloat(currentItem) - (itemEdgeOffset + minimumLineSpacing);
      
        previousOffset = updatedOffset;
        
//        print("itemEdgeOffset = (\(collectionView.frame.width) - \(itemSize.width) - \(minimumLineSpacing) * 2) / 2 = \(itemEdgeOffset)")
//
//        print("updatedOffset = (\(itemSize.width) + \(minimumLineSpacing)) * \(CGFloat(currentItem)) - (\(itemEdgeOffset) + \(minimumLineSpacing)) = \(updatedOffset)")
        delegate?.selectedHotel(index: currentItem)
        return CGPoint(x: updatedOffset, y: 0);//proposedContentOffset.y
    }
}
