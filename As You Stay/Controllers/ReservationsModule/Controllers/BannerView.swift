//
//  bannerView.swift
//  As You Stay
//
//  Created by Max Ward on 26/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class BannerView: UIView {

    @IBOutlet weak var checkInCalendarView: OneDayCalendarView!
    @IBOutlet weak var checkOutCalendarView: OneDayCalendarView!
    @IBOutlet weak var stayLabel: UILabel!
    
    static func configure(with receipt: ReceiptDRO) -> BannerView {
        let view: BannerView = .initFromNib()
       
        guard let startDay =  self.getDay(using: receipt.checkIn), let endDay = self.getDay(using: receipt.checkOut) else {
            let view: BannerView = .initFromNib()
            return view
        }
        
        let checkIn =  OneDayCalendarView.configure(month: startDay.monthName(.short), day: "\(startDay.day)", hour: getHours(using: startDay))
        view.checkInCalendarView.addSubview(checkIn)
        checkIn.translatesAutoresizingMaskIntoConstraints = false
        checkIn.leadingAnchor.constraint(equalTo: view.checkInCalendarView.leadingAnchor, constant: 0).isActive = true
        checkIn.trailingAnchor.constraint(equalTo: view.checkInCalendarView.trailingAnchor, constant: 0).isActive = true
        checkIn.bottomAnchor.constraint(equalTo: view.checkInCalendarView.bottomAnchor, constant: 0).isActive = true
        checkIn.topAnchor.constraint(equalTo: view.checkInCalendarView.topAnchor, constant: 0).isActive = true

        
        let checkOut =  OneDayCalendarView.configure(month: endDay.monthName(.short), day: "\(endDay.day)", hour: getHours(using: endDay))
        view.checkOutCalendarView.addSubview(checkOut)
        checkOut.translatesAutoresizingMaskIntoConstraints = false
        checkOut.leadingAnchor.constraint(equalTo: view.checkOutCalendarView.leadingAnchor, constant: 0).isActive = true
        checkOut.trailingAnchor.constraint(equalTo: view.checkOutCalendarView.trailingAnchor, constant: 0).isActive = true
        checkOut.bottomAnchor.constraint(equalTo: view.checkOutCalendarView.bottomAnchor, constant: 0).isActive = true
        checkOut.topAnchor.constraint(equalTo: view.checkOutCalendarView.topAnchor, constant: 0).isActive = true
        
        view.stayLabel.text = self.getNumberOfNights(using: startDay, checkout: endDay)
        return view
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        setupBackgroundLayer()
        drawDotLine()
    }
    
    fileprivate func setupBackgroundLayer() {
        // layer 1
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        gradientLayer.locations = [0,1]
        gradientLayer.colors = [ #colorLiteral(red: 0.04705882353, green: 0.7607843137, blue: 0.9215686275, alpha: 1).cgColor, UIColor.purple.cgColor]
        gradientLayer.startPoint = CGPoint(x:0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)

        self.layer.insertSublayer(gradientLayer, at: 0)
        
        // Layer 2
        
        let gradientLayer2 = CATextLayer()
        gradientLayer2.frame = CGRect(x: -50, y: -50, width: self.bounds.width + 200, height: self.bounds.height + 100)
        gradientLayer2.foregroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        gradientLayer2.transform = CATransform3DMakeRotation(0.2, 0, 0, 1)
        gradientLayer2.font =  UIFont(name: "Helvetica-Bold", size: 8)
        gradientLayer2.fontSize = 8
        gradientLayer2.isWrapped = true
        gradientLayer2.string = " ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY ASYOUSTAY"
        
        // Layer 3
        
        let gradientLayer3 = CAGradientLayer()
        gradientLayer3.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        gradientLayer3.locations = [0,1]
        gradientLayer3.colors = [UIColor.clear.cgColor, #colorLiteral(red: 0.1670824289, green: 0.1838721633, blue: 0.2178872228, alpha: 0.5).cgColor]
        gradientLayer3.startPoint = CGPoint(x:0.0, y: 0.0)
        gradientLayer3.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer3.addSublayer(gradientLayer2)
        gradientLayer3.masksToBounds = true

        self.layer.insertSublayer(gradientLayer3, at: 1)
    }
    
    fileprivate func drawDotLine() {
        let startPoint = CGPoint(x: self.bounds.minX + self.bounds.height * 0.7, y: self.bounds.midY * 0.8)
        let endPoint = CGPoint(x: self.bounds.maxX - self.bounds.height * 0.7, y: self.bounds.midY * 0.8)
        let controlPoint = CGPoint(x: self.bounds.midX, y: self.bounds.minY - 10)

        let path = CGMutablePath()
        path.move(to: startPoint)
        //path.addLine(to: endPoint)
        path.addQuadCurve(to: endPoint, control: controlPoint)

        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = #colorLiteral(red: 0.04705882353, green: 0.7607843137, blue: 0.9215686275, alpha: 1)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.fillRule = .evenOdd
        shapeLayer.lineDashPattern = [10, 5, 5, 5]
        shapeLayer.path = path

        self.layer.addSublayer(shapeLayer)
    }
    
    static func getDay(using day: String)-> Date? {
        let dateFormatter = DateFormatter()
//        2020-01-01T15:00:00
        let formmatedDay = day.replacingOccurrences(of: "T", with: " ")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return  dateFormatter.date(from: formmatedDay)
    }
    
    static func getNumberOfNights(using checkin: Date, checkout: Date) -> String {
        
        let calendar = Calendar.current
        let first = calendar.startOfDay(for: checkin)
        let last = calendar.startOfDay(for: checkout)
        
        let nights = calendar.dateComponents([.day], from: first, to: last)

        if let numbersNights = nights.day {
            if checkin == checkout {
                let hours  = Double(DatesManager.shared.checkoutHours - DatesManager.shared.checkinHours)
                return String(format: "%.2f", hours / 24) + " Night"
            }
            return "\(numbersNights) Nights"
        } else {
            return "0"
        }
    }
    static func getHours(using date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH a"
        return formatter.string(from: date)
    }
}
