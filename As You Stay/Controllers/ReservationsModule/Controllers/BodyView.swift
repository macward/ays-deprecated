//
//  BodyView.swift
//  As You Stay
//
//  Created by Max Ward on 30/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit


class BodyView: UIView {
    
    @IBOutlet weak var nameHotelLabel: UILabel!
    @IBOutlet weak var guestLabel: UILabel!
    @IBOutlet weak var creditCardLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var roomPriceLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    static func configure(using receipt: ReceiptDRO?) -> BodyView {
        let view: BodyView = initFromNib()
        guard let receipt = receipt else {return view }
        view.nameHotelLabel.text = receipt.hotelName
        view.guestLabel.text = "\(receipt.guestName)"
        view.creditCardLabel.text = "\(receipt.hotelAddress) "
        view.taxLabel.text = "\(receipt.taxes)"
        view.roomPriceLabel.text = "\(receipt.price)"
        view.totalLabel.text = "\(receipt.total)"
        return view
    }
}

