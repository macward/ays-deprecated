//
//  BookmarksViewController.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class BookmarksViewController: UIViewController {

    var coordinator: BookmarksCoordinator?
    var collectionView: UICollectionView!
    var viewModel = BookmarksViewModel()
    var favoritesList: [HotelObject]?
    var activityIndicator: ActivityIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator = ActivityIndicator(view: view)
        setupCollectionView()
        favoritesList = viewModel.list()
        title = "Boomark"
        self.view.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.137254902, blue: 0.1647058824, alpha: 1)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        favoritesList = viewModel.list()
        checkAvailability()
        collectionView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        activityIndicator.backgroundView.frame = self.view.bounds
        activityIndicator.view?.frame = self.view.bounds
        activityIndicator.animationView.center = self.view.center
    }
    
    fileprivate func setupCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)

        collectionView.backgroundColor = .clear
        self.view.addSubview(collectionView)
        
        collectionView.register(UINib(nibName: "BookmarksCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BookmarkCell")
        
        collectionView.isHidden = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo:self.view.topAnchor).isActive = true
    }
    
    func checkAvailability(){
        
//        if let list = favoritesList{
//            let viewModel = HotelDetailsViewModel()
//            guard let checkin  = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)?.getFormatToRequest() else { return }
//            guard let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)?.getFormatToRequest() else { return }
//
//            var count = 0
//            activityIndicator.startAnimating()
//            if list.count > 0 {
//                for hotel in list {
//
//                    viewModel.getRoomPricing(checkin_datetime: checkin, checkout_datetime: checkout, hotel_id: hotel.id, promo_code: "", ppn_bundle: hotel.ppn_bundle) { (result) in
//                        if result != nil {
//                            self.viewModel.updateAvailability(id: hotel.idAPi, availability: true)
//                        }
//                        count += 1
//                        if count == list.count {
//                            self.activityIndicator.stopAnimating()
//                            count = 0
//                        }
//                    }
//                }
//            } else {
//                self.activityIndicator.stopAnimating()
//            }
//        }
    }
}

// MARK: - UICollection view data source
extension BookmarksViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let favorities = favoritesList else { return 0 }
        return favorities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookmarkCell", for: indexPath) as! BookmarksCollectionViewCell
        
        if let list = favoritesList {
            cell.configure(with: list[indexPath.row])
        }
        
        return cell
    }
    
    @available(iOS 13.0, *)
    func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        if #available(iOS 13.0, *) {
            let configuration = UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { (action) -> UIMenu? in
                
                let shareAction = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up"), identifier: nil, discoverabilityTitle: nil, attributes: [], state: .off) { (action) in
                    if let cell = collectionView.cellForItem(at: indexPath) as? BookmarksCollectionViewCell {
                        self.showActivity(image: cell.image.image!, id: cell.hotelID)
                    }
                }
                
                let uncheckAction = UIAction(title: "Remove", image: UIImage(systemName: "bookmark"), identifier: nil, discoverabilityTitle: nil, attributes: [], state: .off) { (action) in
                    print("uncheck")
                    
                    let alertController = customAlert(title: "Warning", message: "Do you want to remove from favorites?", firstTitleAction: "Remove", secondTitleAction: "Cancel", firstActionHandler: { (UIAlertAction) in
                        
                        if let hotel = self.favoritesList?[indexPath.row] {
                            self.viewModel.destroy(id: hotel.id)
                            self.favoritesList = self.viewModel.list()
                            self.collectionView.reloadData()
                        }
                        
                    }) { (UIAlertAction) in
                        print("cancel remove hotel to the favorites")
                    }

                    self.present(alertController, animated: true, completion: nil)
                }
                
                let menu = UIMenu(title: "AS YOU STAY", image: #imageLiteral(resourceName: "icon-ays.pdf"), identifier: nil, options: [], children: [shareAction, uncheckAction])
                return menu
            }
            return configuration
        } else {
            // Fallback on earlier versions
        }
        return UIContextMenuConfiguration()
    }
}

// MARK: - UIActivityViewController
extension BookmarksViewController {
    
    fileprivate func showActivity(image: UIImage, id: String) {
        // Text to share with other apps
        let textToShare = String(describing: "As You Stay")


        guard let myAppURLToShare = URL(string: "https://asyoustay.com/hotels/\(id)") else {
            return
        }
        
        let items = [myAppURLToShare] as [Any]
        let avc = UIActivityViewController(activityItems: items, applicationActivities: nil)
        avc.navigationController?.title = "AS YOU STAY"
//        let view = UIImageView()
//        view.image = image
//        avc.view.addSubview(view)
//        
//        view.translatesAutoresizingMaskIntoConstraints = false
//        view.leadingAnchor.constraint(equalTo: avc.view.leadingAnchor).isActive = true
//        view.trailingAnchor.constraint(equalTo: avc.view.trailingAnchor).isActive = true
//        view.heightAnchor.constraint(equalToConstant: 200).isActive = true
////        view.topAnchor.constraint(equalTo:  avc.view.subviews.first!.bottomAnchor).isActive = false
//        view.bottomAnchor.constraint(equalTo: avc.view.subviews.last!.topAnchor).isActive = true
        //Apps to exclude sharing to
        avc.excludedActivityTypes = [
            UIActivity.ActivityType.airDrop,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList
        ]

        //Present the shareView on iPhone
        self.present(avc, animated: true, completion: nil)
    }
}

// MARK: - UICollection view delegate flowLayout
extension BookmarksViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         var hotelViewModel = HotelResultsViewModel()
         
         if let hotelID = favoritesList?[indexPath.row].id {
             activityIndicator.startAnimating()
             
             guard let checkin  = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)?.getFormatToRequest() else { return }
             guard let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)?.getFormatToRequest() else { return }
            
             let rooms = GuestSearchDTO(adults: 2, children: 0)
             
             hotelViewModel.getHotelDetails(hotel_id: hotelID,
                                            checkin_datetime: checkin,
                                            checkout_datetime: checkout,
                                            rooms: rooms) { [unowned self] (details, message) in
                 
                 guard let hotelDetails = details else {
                     self.coordinator?.errorAlert(ref: self, description: message ?? "Cant get hotel information")
                     self.activityIndicator.stopAnimating()
                     return
                 }
                 
                 self.activityIndicator.stopAnimating()
                 self.coordinator?.details(hotel: hotelDetails)
             }
         }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width - 20, height: 200)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
}
