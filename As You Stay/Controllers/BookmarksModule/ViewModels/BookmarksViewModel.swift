//
//  BookmarksViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 13/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit


class BookmarksViewModel {
    
    func store(hotelDetails: HotelDetails){
        
        let hotel = HotelObject()

        hotel.id = hotelDetails.id
        hotel.name = hotelDetails.name
        hotel.full_address = hotelDetails.full_address
        hotel.lat = (hotelDetails.lat as NSString).floatValue
        hotel.lng = (hotelDetails.lng as NSString).floatValue
        hotel.avg_market_price = 0
        hotel.avg_member_savings = 0
        hotel.member_savings_perc = 0
        hotel.avg_night_price = 0
        hotel.duration = 0
        hotel.review_rating = 1
        hotel.star_rating = 0
        hotel.distance = 0
        hotel.photo = hotelDetails.album[0].url
        BookmarksLocalService.store(favorite: hotel)
    }
    
    func list()-> [HotelObject]? {
        return BookmarksLocalService.list()
    }
    
    
    func isFavorites(id: String ) -> Bool {
        return BookmarksLocalService.exists(id: id)
    }
    
    func updateAvailability(id: String, availability: Bool){
        BookmarksLocalService.updateAvailability(id: id, availability: availability)
    }
    func destroy(id: String){
        
//        let deleted = BookmarksLocalService.getFavorite(id: id)
//        if let hotelDeleted = deleted {
//            for item in hotelDeleted.album {
//                AlbumLocalService.destroy(photo: item)
//            }
//            for item in hotelDeleted.amenities {
//                AmenitiesLocalService.destroy(amenity: item)
//            }
//        }
        BookmarksLocalService.destroy(id: id)
    }
    
    func switchBookmarkIcon(id: String) -> UIImage? {
        if isFavorites(id: id){
            return UIImage(named: "icon-bookmarks-fill")
        } else {
            return UIImage(named: "icon-bookmarks")
        }
    }
}
