//
//  TestingTableViewController.swift
//  As You Stay
//
//  Created by Max Ward on 26/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class TestingTableViewController: UITableViewController {
    
    var items: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        items.append("profile view")
        items.append("Credit Cards")
        items.append("Sign out")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let viewController = ProfileViewController()
            let viewModel = ProfileViewModel()
            viewController.viewModel = viewModel
            navigationController?.pushViewController(viewController, animated: true)
            
        case 1:
            showCreditCards()
        case 2:
           signout()
        default:
            print("")
        }
    }
}

extension TestingTableViewController {
    fileprivate func signout() {
        let alert = UIAlertController(title: "Destroy Session", message: "Are you sure?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Close", style: .cancel) { (status) in
            let authViewModel = AuthViewModel()
            authViewModel.remove()
            let navigationController = UINavigationController()
            let loginViewController = LoginViewController()
            loginViewController.authViewModel = AuthViewModel()
            navigationController.pushViewController(loginViewController, animated: false)
                       
            self.setRootViewController(navigationController)
        }
        alert.addAction(action)
        present(alert, animated: true)
    }
    func showCreditCards(){
        navigationController?.pushViewController(CreditCardsViewController(), animated: true)
    }
}
