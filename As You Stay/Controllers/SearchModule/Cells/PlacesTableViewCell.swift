//
//  PlacesTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 7/25/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import GooglePlaces

class PlacesTableViewCell: UITableViewCell {

    @IBOutlet weak var placeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
        
    func configure(with place: GMSAutocompletePrediction){
        placeLabel.text = place.attributedFullText.string
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
    }
    
}
