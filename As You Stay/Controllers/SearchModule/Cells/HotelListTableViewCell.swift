//
//  HotelListTableViewCell.swift
//  As You Stay
//
//  Created by Max Ward on 12/05/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit
import Kingfisher

class HotelListTableViewCell: UITableViewCell {

    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var hotelNameLable: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var wasPriceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var gradientView: UIView!
    
    let gradient = CAGradientLayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        gradientView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        gradientView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        gradientView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        gradientView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        gradientView.layer.addSublayer(gradient)
        layoutIfNeeded()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = gradientView.bounds
        gradient.locations = [0,1]
        gradient.colors = [UIColor.clear.cgColor, UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x:0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y:1.0)
//        layoutIfNeeded()
        
    }

    func configure(with hotel: HotelResponse) {
        
        hotelNameLable.text = hotel.name
        addressLabel.text = hotel.full_address
        priceLabel.text = "$\(hotel.avg_night_price.noZero)"
        wasPriceLabel.attributedText = "was$\(hotel.avg_market_price.noZero)".strikeThrough()
        hotelImage.contentMode = .scaleAspectFill
        if let url = URL(string: hotel.photo.url) {
            hotelImage.kf.setImage(with: .network(url))
        } else {
            hotelImage.backgroundColor = .lightGray
        }
    }
    
}
