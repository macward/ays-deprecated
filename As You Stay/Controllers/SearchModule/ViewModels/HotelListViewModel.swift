//
//  HotelListViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 12/05/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation
import UIKit


class HotelListViewModel {
    var headerData: HotelListDRO!
    var hotels: [HotelResponse] = []

}
