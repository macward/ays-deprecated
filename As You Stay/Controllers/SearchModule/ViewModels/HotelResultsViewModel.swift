//
//  ResultMapViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 25/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import GoogleMaps
import Alamofire
import AlamofireImage

class HotelResultsViewModel: NSObject {
    
    var hotelArray: [HotelResponse] = []
    var hotelMarkers: [GMSMarker] = []
    var detailSelectedHotel: HotelDetails?
    var selectedIndex: Int = 0
    var isSearching = DynamicValue<Bool>(false)
    var zoom: Float = 12.0
    var centerCell: ((HotelResponse, Int) -> Void)?

    lazy var searchDTO = SearchDTO()
    var currentPage: Int = 0
    var totalPages: Int = 0
    
    override init() {
        super.init()
    }
    
    func getNumberOfNights() -> String{
        let calendar = Calendar.current
        guard let checkin = DatesManager.shared.checkin, let checkout = DatesManager.shared.checkout else { return "0" }
        
        let first = calendar.startOfDay(for: checkin)
        let last = calendar.startOfDay(for: checkout)
        
        let nights = calendar.dateComponents([.day], from: first, to: last)

        if let numbersNights = nights.day {
            if checkin == checkout {
                let hours  = Double(DatesManager.shared.checkoutHours - DatesManager.shared.checkinHours)
                return String(format: "%.2f", hours / 24)
            }
            return "\(numbersNights)"
        } else {
            return "0"
        }
    }
    
    func addingDaysToDate(days: Int, currentDate: Date) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: currentDate)!
    }
    
    func removeAllMarkers(mapView: GMSMapView){
        mapView.clear()
    }
    
    func createHotelMarkerList(mapView: GMSMapView){
        
        removeAllMarkers(mapView: mapView)
        hotelMarkers.removeAll()
        
        if hotelArray.count > 0 {
            for (index, hotel) in hotelArray.enumerated() {
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(hotel.lat), longitude: CLLocationDegrees(hotel.lng))
                
                marker.iconView = makeMarkerView(for: hotel, isSelected: false)
                marker.map = mapView
                marker.hotel = hotel
                marker.index = index
                hotelMarkers += [marker]
            }
            hotelMarkers[selectedIndex].iconView = makeMarkerView(for: hotelArray[selectedIndex], isSelected: true)
            hotelMarkers[selectedIndex].zIndex = 1
            
            updateCameraToSelectedMarker(selected:  hotelMarkers[selectedIndex], map: mapView)
            
        } else {
            print("")
        }
    }
    
    func getImageHotel(url: String, completion :@escaping ((_ imageHotel : UIImage)->Void) ){
        let hotelService = HotelService()
        hotelService.getImageHotel(url: url) { (image) in
            completion(image)
        }
    }
    
    func selectMarker(_ marker: GMSMarker) {
        guard let newSelected = hotelMarkers.firstIndex(of: marker) else { return }
        let prevSelectedMarker = hotelMarkers[selectedIndex]
        let prevSelectedHotel = hotelArray[selectedIndex]
        prevSelectedMarker.iconView = makeMarkerView(for: prevSelectedHotel, isSelected: false)
        prevSelectedMarker.zIndex = 0
        
        selectedIndex = newSelected
        let hotel = hotelArray[newSelected]
        marker.iconView = makeMarkerView(for: hotel, isSelected: true)
        marker.zIndex = 1
    }
}

// MARK: - Make custom marker
extension HotelResultsViewModel {
    
    fileprivate func makeMarkerView(for hotel: HotelResponse, isSelected: Bool) -> UIView {
        let priceLabel = makeMarkerLabel(for: hotel)
        let priceLabelBackgroundView = embedLabelToView(priceLabel)
        
        priceLabelBackgroundView.backgroundColor = isSelected ? UIColor(red: 12/255, green: 194/255, blue: 235/255, alpha: 1) : UIColor(red: 32/255, green: 35/255, blue: 42/255, alpha: 1)
        
        let pinImage = isSelected ? UIImage(named: "locationPinSelected") : UIImage(named: "locationPinUnselected")
        let pinImageView = UIImageView(image: pinImage)
        
        pinImageView.translatesAutoresizingMaskIntoConstraints = false
        
        let maxWidth = max(pinImageView.bounds.width, priceLabelBackgroundView.bounds.width)
        let markerView = UIView()
        markerView.translatesAutoresizingMaskIntoConstraints = false
        markerView.addSubview(priceLabelBackgroundView)
        markerView.addSubview(pinImageView)
        let markerViewConstraints = [pinImageView.topAnchor.constraint(equalTo: priceLabel.bottomAnchor),
                                     pinImageView.centerXAnchor.constraint(equalTo: markerView.centerXAnchor),
                                     priceLabelBackgroundView.centerXAnchor.constraint(equalTo: markerView.centerXAnchor),
                                     priceLabelBackgroundView.topAnchor.constraint(equalTo: markerView.topAnchor),
                                     markerView.heightAnchor.constraint(equalToConstant: priceLabelBackgroundView.bounds.height + pinImageView.bounds.height),
                                     markerView.widthAnchor.constraint(equalToConstant: maxWidth)]
        NSLayoutConstraint.activate(markerViewConstraints)
        markerView.layoutIfNeeded()
        return markerView
    }
    
    fileprivate func makeMarkerLabel(for room: HotelResponse) -> UILabel {
        let priceLabel = UILabel()
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.font = UIFont.App.bold(size: 12)
        priceLabel.textAlignment = .center
        priceLabel.textColor = .white
        priceLabel.text = "$\(room.avg_night_price.noZero)"
        priceLabel.sizeToFit()
        return priceLabel
    }
    
    fileprivate func embedLabelToView(_ label: UILabel) -> UIView {
        let priceLabelBackgroundView = UIView()
        priceLabelBackgroundView.layer.cornerRadius = 2
        priceLabelBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        priceLabelBackgroundView.addSubview(label)
        let priceLabelConstraints = [label.centerXAnchor.constraint(equalTo: priceLabelBackgroundView.centerXAnchor),
                                     label.centerYAnchor.constraint(equalTo: priceLabelBackgroundView.centerYAnchor),
                                     priceLabelBackgroundView.heightAnchor.constraint(equalToConstant: label.bounds.height + 8.0),
                                     priceLabelBackgroundView.widthAnchor.constraint(equalToConstant: label.bounds.width + 8.0)]
        NSLayoutConstraint.activate(priceLabelConstraints)
        priceLabelBackgroundView.layoutIfNeeded()
        return priceLabelBackgroundView
    }
}

// MARK: - Map view delegate
extension HotelResultsViewModel: GMSMapViewDelegate {
    
    internal func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        selectMarker(marker)
        updateCameraToSelectedMarker(selected: marker, map: mapView)
        if let hotel = marker.hotel, let index = marker.index{
            centerCell?(hotel, index)
        }
        return true
    }
    
    internal func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
      zoom = mapView.camera.zoom
    }
    
    fileprivate func updateCameraToSelectedMarker(selected: GMSMarker, map: GMSMapView ){
        let target = CLLocationCoordinate2D(latitude: selected.position.latitude, longitude: selected.position.longitude)
        let camera = GMSCameraPosition.camera(withTarget: target, zoom: map.camera.zoom)
        map.animate(to: camera)
    }
}

// MARK: - Request Hotel details
extension HotelResultsViewModel {
    
    func getHotelDetails(hotel_id: String,
                         checkin_datetime:String,
                         checkout_datetime: String,
                         rooms:GuestSearchDTO,
                         completion: @escaping (HotelDetails?, String?)-> Void) {
           
       let hotelService = HotelService()
       hotelService.getHotelDetails(hotel_id: hotel_id,
                                    checkin_datetime: checkin_datetime,
                                    checkout_datetime: checkout_datetime,
                                    rooms: rooms.toDict()) { (result) in
           switch result {
               case .success(let value):
                if value.statusCode == 0 {
                    completion(value.data?.details, value.message)
                } else {
                    completion(nil, value.message)
                }
               
               case .failure(let err):
                completion(nil, err.localizedDescription)
           }
       }
   }
}

// MARK: - Request LocationsV2
extension HotelResultsViewModel {
    
    func searchHotelsByLocation(completion: @escaping (Bool, String?)-> Void){
        isSearching.value = true
        self.hotelArray.removeAll()
        let hotelService = HotelService()
        resetPages()
    
        hotelService.searchHotelByLocation(params: searchDTO.toDict2()) { (result) in
            switch result{
            case .success(let value):
                if value.statusCode == 0 {
                    if let data = value.data {
                        self.totalPages = data.pages
                        self.currentPage = 1
                        for hotel in data.hotels {
                            self.hotelArray.append(hotel)
                        }
                        completion(true, value.message)
                        self.isSearching.value = false
                    }
                    
                } else {
                    completion(false, value.message)
                }
                
            case .failure(let err):
                self.isSearching.value = false
                completion(false, err.localizedDescription)
            }
        }
    }
    
    func moreHotelsByLocation(completion: @escaping (Bool, String?)-> Void){
        isSearching.value = true
        let hotelService = HotelService()
        
        hotelService.searchHotelByLocation(params: searchDTO.toDict2()) { (result) in
            switch result{
            case .success(let value):
                if value.statusCode == 0 {
                    if let data = value.data {
                        for hotel in data.hotels {
                            self.hotelArray.append(hotel)
                        }
                        completion(true, value.message)
                        self.isSearching.value = false
                    }
                    
                } else {
                    completion(false, value.message)
                }
                
            case .failure(let err):
                self.isSearching.value = false
                completion(false, err.localizedDescription)
            }
        }
    }
    
    /* To Make searchDTO from LocationsV2 request*/
    
    func setDateTime(checkin: Date, checkout: Date) {
        searchDTO.checkinDatetime = checkin.getFormatToRequest()
        searchDTO.checkoutDatetime = checkout.getFormatToRequest()
    }
    
    func setAddress(address: GMSAddress) {
        searchDTO.city = address.locality
        searchDTO.country = address.country
        searchDTO.state = address.locality
        searchDTO.destination = address.locality
        self.setCoords(location: address.coordinate)
    }
    
    func setAddress(address: Address) {
        searchDTO.city = address.city
        searchDTO.country = address.country
        searchDTO.state = address.state
        searchDTO.destination = address.city
        self.setCoords(location: address.location)
    }
    
    func setCoords(location: CLLocationCoordinate2D) {
        searchDTO.lat = Float(location.latitude)
        searchDTO.lng = Float(location.longitude)
    }
    
    func setNextPage() {
        if currentPage < totalPages {
            currentPage += 1
            searchDTO.page = currentPage
        }
    }
    
    func resetPages() {
        currentPage = 0
        totalPages = 0
        searchDTO.page = 1
    }
    
    func checkMakeMoreHotels() -> Bool {
        if currentPage < totalPages {
            return true
        }
        return false
    }
}
