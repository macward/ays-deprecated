//
//  SearchLocationViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 08/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import CoreLocation

class SearchLocationViewModel {
    
    init() {        
    }
    
    func retrieveCurrentLocation(completion: @escaping((_ coords: CLLocationCoordinate2D) -> Void)) {
        ZibraLocationManager.shared.startLocation()
        
        ZibraLocationManager.shared.didUpdateLocation = { location in
            completion(location)
        }
    }
}



