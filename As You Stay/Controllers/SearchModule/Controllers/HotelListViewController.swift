//
//  HotelListViewController.swift
//  As You Stay
//
//  Created by Max Ward on 12/05/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class HotelListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var nightsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    var viewModel: HotelListViewModel!
    var delegate: SelectedHotelFromVerticalListDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureMapButton()
        configureHeaderData()
    }
    
    func configureMapButton() {
        mapButton.layer.cornerRadius = 0.5 * mapButton.bounds.size.width
        mapButton.clipsToBounds = true
    }
    
    func configureTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "HotelListTableViewCell", bundle: nil), forCellReuseIdentifier: "HotelListCell")
    }
    
    @IBAction func didTapMapButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func configureHeaderData() {
        cityLabel.text = viewModel.headerData.city
        dateLabel.text = viewModel.headerData.date
        nightsLabel.text = viewModel.headerData.nights
    }
}
extension HotelListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.hotels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "HotelListCell") as! HotelListTableViewCell
        cell.configure(with: viewModel.hotels[indexPath.section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        dismiss(animated: true) {
            self.delegate?.selectedHotel(self.viewModel.hotels[indexPath.section].id)
        }
    }
    
    
}
