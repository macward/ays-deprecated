//
//  GuestsViewController.swift
//  As You Stay
//
//  Created by Max Ward on 29/04/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class GuestsViewController: UIViewController {
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var adultsCountLabel: UILabel!
    @IBOutlet weak var childrenCountLabel: UILabel!
    @IBOutlet weak var moreAdultsButton: UIButton!
    @IBOutlet weak var moreChildrenButton: UIButton!
    @IBOutlet weak var lessAdultsButton: UIButton!
    @IBOutlet weak var lessChildrenButton: UIButton!
    
    var countAdults = 0
    var countChildren = 0
    var total = 0
    var delegate: DidSelectedGuestDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        adultsCountLabel.text = "\(countAdults)"
        childrenCountLabel.text = "\(countChildren)"
        total = countAdults + countChildren
        totalLabel.text = "TOTAL GUESTS: \(total)"
    }
    
    @IBAction func didTapMoreAdults( _ sender: Any) {
        if countAdults < 10 {
            countAdults += 1
            total += 1
            adultsCountLabel.text = "\(countAdults)"
            totalLabel.text = "TOTAL GUESTS: \(total)"
        }
        
    }
    
    @IBAction func didTapLessAdults( _ sender: Any) {
        if countAdults > 1 {
            countAdults -= 1
            total -= 1
            adultsCountLabel.text = "\(countAdults)"
            totalLabel.text = "TOTAL GUESTS: \(total)"
        }
    }
    
    @IBAction func didTapMoreChildren( _ sender: Any) {
        if countChildren < 10 {
            countChildren += 1
            total += 1
            childrenCountLabel.text = "\(countChildren)"
            totalLabel.text = "TOTAL GUESTS: \(total)"
        }
    }
    
    @IBAction func didTapLessChildren( _ sender: Any) {
        if countChildren > 0 {
            countChildren -= 1
            total -= 1
            childrenCountLabel.text = "\(countChildren)"
            totalLabel.text = "TOTAL GUESTS: \(total)"
        }
    }
    
    @IBAction func didTapOK( _ sender: Any) {
        delegate?.selectedGuests(adults: countAdults, children: countChildren)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapClose( _ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func configure() {
        
        moreAdultsButton.layer.cornerRadius =  0.5 * moreAdultsButton.bounds.size.width
        
        moreChildrenButton.layer.cornerRadius =  0.5 * moreChildrenButton.bounds.size.width
        
        lessAdultsButton.backgroundColor = .clear
        lessAdultsButton.layer.cornerRadius =  0.5 * lessAdultsButton.bounds.size.width
        lessAdultsButton.layer.borderWidth = 1
        lessAdultsButton.layer.borderColor = UIColor(red: 12/255, green: 194/255, blue: 235/255, alpha: 1).cgColor
        lessChildrenButton.backgroundColor = .clear
        lessChildrenButton.layer.cornerRadius =  0.5 * lessChildrenButton.bounds.size.width
        lessChildrenButton.layer.borderWidth = 1
        lessChildrenButton.layer.borderColor = UIColor(red: 12/255, green: 194/255, blue: 235/255, alpha: 1).cgColor
    }
}
