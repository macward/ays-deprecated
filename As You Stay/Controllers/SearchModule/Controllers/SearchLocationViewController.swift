//
//  SearchLocationViewController.swift
//  As You Stay
//
//  Created by Max Ward on 7/25/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

struct Address {
    let location: CLLocationCoordinate2D
//    let southWestLocation: CLLocationCoordinate2D
//    let northEastLocation: CLLocationCoordinate2D
    let name: String
    let city: String?
    let state: String?
    let country: String?
}

class SearchLocationViewController: UIViewController {
    
    @IBOutlet weak var locationSearchBar: UISearchBar!
    @IBOutlet weak var locationsTableView: UITableView!
    @IBOutlet weak var currentLocationLabel: UILabel!
    
    fileprivate let placesClient = GMSPlacesClient()
    var activityIndicator: ActivityIndicator!
    fileprivate let filter: GMSAutocompleteFilter = {
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        return filter
    }()
    
    var selectedLocationDelegate: SelectedLocationDelegate?
    
    var viewModel = SearchLocationViewModel()
    
    fileprivate var resultsData: [GMSAutocompletePrediction] = [] {
        didSet {
            locationsTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        shouldDismissKeyboard()
        
        view.backgroundColor = UIColor(red: 43/256, green: 47/256, blue: 58/256, alpha: 1)
        
        locationSearchBar.backgroundImage = UIImage()
        locationSearchBar.barTintColor = UIColor(red: 43/256, green: 47/256, blue: 58/256, alpha: 1)
        locationSearchBar.setTextFieldBackgroundColor(color: UIColor(red: 43/256, green: 47/256, blue: 58/256, alpha: 1))
        locationSearchBar.setTextFieldText(color: UIColor.white, fontSize: 14.0, bold: false)
        let textFieldInsideSearchBar = locationSearchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        
        if let icon = UIImage(named: "icon-search"){
            locationSearchBar.setIcon(image: icon)
        }
        locationSearchBar.setMagnifyingGlassColorTo(color: .lightGray )
        
        locationsTableView.backgroundColor = UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1)
        locationsTableView.separatorStyle = .none
        locationSearchBar.delegate = self
        locationsTableView.delegate = self
        locationsTableView.dataSource = self
                
        locationsTableView.register(PlacesTableViewCell.self, forCellReuseIdentifier: "PlacesCell")
        
        let currentLocationTap = UITapGestureRecognizer(target: self, action: #selector(didtapCurrentLocation(sender:)))
        currentLocationLabel.isUserInteractionEnabled = true
        currentLocationLabel.font = UIFont.Text.big()
        currentLocationLabel.addGestureRecognizer(currentLocationTap)
        
        let closeButton = UIBarButtonItem(image: UIImage(named: "icon-cross-close"), style: .plain, target: self, action: #selector(didTapCloseButton(_:)))
        closeButton.tintColor = UIColor(hexString: "0CC2EB")
        navigationItem.rightBarButtonItem = closeButton
        
        title = "Select Location"
        
        locationSearchBar.becomeFirstResponder()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locationsTableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        currentLocationLabel.font = UIFont.Text.big()
    }
    
    @objc func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func didtapCurrentLocation(sender: UITapGestureRecognizer) {
        activityIndicator = ActivityIndicator(view: view)
        activityIndicator.startAnimating()
        
        let locStatus = CLLocationManager.authorizationStatus()
        switch locStatus {
            case .notDetermined:
                let locationManager = CLLocationManager()
//              locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.distanceFilter = 200
                locationManager.requestWhenInUseAuthorization()
            return
            case .denied, .restricted:
                let alert = UIAlertController(title: "Location Services are disabled", message: "Please enable Location Services in your Settings", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
                present(alert, animated: true, completion: nil)
                self.activityIndicator.stopAnimating()
            return
            case .authorizedAlways, .authorizedWhenInUse:
            break
        @unknown default:
            fatalError()
        }
        
        viewModel.retrieveCurrentLocation { (location) in
            print(location)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.activityIndicator.stopAnimating()
                self.getCurrentLocation(coordinates: location)
            }
        }
    }
}

extension SearchLocationViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        locationsTableView.reloadData()
        self.placeAutocomplete(searchText)
    }
}

extension SearchLocationViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1)
        cell.textLabel?.textColor = .white
        cell.textLabel?.font = UIFont.Text.big()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesCell") as! PlacesTableViewCell
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.white
        cell.selectedBackgroundView = backgroundView
        cell.textLabel?.textColor = .white
        cell.textLabel?.text = self.addressStringForAutocompletePrediction(resultsData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section:Int) -> String?{
        return "POPULAR CITIES"
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor(red: 32/256, green: 35/256, blue: 42/256, alpha: 1)
        return header
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let placeID = resultsData[indexPath.row].placeID
        getPlaceDetails(placeID)
        selectedLocationDelegate?.didSelectLocation(location: addressStringForAutocompletePrediction(resultsData[indexPath.row]))
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        tableView.cellForRow(at: indexPath)?.selectedBackgroundView?.backgroundColor = .clear
        tableView.cellForRow(at: indexPath)?.selectionStyle = .none
        return true
    }
}

extension SearchLocationViewController {
    
    fileprivate func placeAutocomplete(_ text: String) {
        placesClient.autocompleteQuery(text, bounds: nil, filter: filter) { autocompleteResults, error in
            guard let autocompleteResults = autocompleteResults else {
                print("No Autocomplete results with error: \(String(describing: error?.localizedDescription))")
                return
            }
            self.resultsData = autocompleteResults
        }
    }
    
    fileprivate func addressWithoutLastComponentFromAddress(_ address: String) -> String {
        var addressComponents = address.components(separatedBy: ",")
        
        if (addressComponents.count > 1) {
            addressComponents.removeLast()
        }
        return addressComponents.joined(separator: ",")
    }
    
    fileprivate func addressStringForAutocompletePrediction(_ autocompletePrediction: GMSAutocompletePrediction) -> String {
        let address =  autocompletePrediction.attributedFullText.string
        return address
    }

    fileprivate func shortAddressStringForAutocompletePrediction(_ autocompletePrediction: GMSAutocompletePrediction) -> String {
        let address =  autocompletePrediction.attributedFullText.string
        let addressWithState = addressWithoutLastComponentFromAddress(address)
        let addressNoState = addressWithoutLastComponentFromAddress(addressWithState)
        return addressNoState
    }
    
    fileprivate func getPlaceDetails(_ placeID: String) {
        
        placesClient.lookUpPlaceID(placeID) { place, error in
            guard let place = place else {
                print("lookup place id error: \(String(describing: error?.localizedDescription))")
                return
            }
            guard let southWest = place.viewport?.southWest else {
                print("lookup place id error: \(String(describing: error?.localizedDescription))")
                return
            }
            guard let northEast = place.viewport?.northEast else {
                print("lookup place id error: \(String(describing: error?.localizedDescription))")
                return
            }
            self.getAddress(coordinates: place.coordinate, southWest: southWest, northEast: northEast)
        }
    }
    
    func getAddress(coordinates: CLLocationCoordinate2D, southWest: CLLocationCoordinate2D, northEast: CLLocationCoordinate2D) {

        GMSGeocoder().reverseGeocodeCoordinate(coordinates) { response, error in
            
            guard let result = response?.firstResult() else {
                print("No results from geocode request with error: \(String(describing: error?.localizedDescription))")
                return
            }
            
            if let line1 = result.lines?.first, let line2 = result.lines?.last {
                
                let address = line1.isEmpty ? self.addressWithoutLastComponentFromAddress(line2) : self.addressWithoutLastComponentFromAddress(line1 + ", " + line2)
                
                let selectedAddress = Address(location: result.coordinate,
                                             // southWestLocation: southWest,
                                              //northEastLocation: northEast,
                                              name: address,
                                              city: result.locality,
                                              state: result.administrativeArea,
                                              country: result.country)
                
                self.selectedLocationDelegate?.didSelectLocation(address: selectedAddress)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    
    func getCurrentLocation(coordinates: CLLocationCoordinate2D){
        GMSGeocoder().reverseGeocodeCoordinate(coordinates) { response, error in
            
            guard let result = response?.firstResult() else {
                print("No results from geocode request with error: \(String(describing: error?.localizedDescription))")
                return
            }
            
            if let line1 = result.lines?.first, let line2 = result.lines?.last {
                
                let address = line1.isEmpty ? self.addressWithoutLastComponentFromAddress(line2) : self.addressWithoutLastComponentFromAddress(line1 + ", " + line2)
                
                let selectedAddress = Address(location: result.coordinate,
                                              //southWestLocation: result.coordinate,
                                              //northEastLocation: result.coordinate,
                                              name: address,
                                              city: result.locality,
                                              state: result.administrativeArea,
                                              country: result.country)
                
                self.selectedLocationDelegate?.didSelectLocation(address: selectedAddress)
                self.selectedLocationDelegate?.didSelectLocation(location: selectedAddress.city ?? "")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
