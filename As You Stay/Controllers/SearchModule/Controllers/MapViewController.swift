//
//  MapViewController.swift
//  As You Stay
//
//  Created by Max Ward on 13/08/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapViewController: UIViewController {

    var coordinator: HotelCoordinator?
    var viewModel = HotelResultsViewModel()
    
    @IBOutlet weak var checkinView: UIView!
    @IBOutlet weak var checkoutView: UIView!
    @IBOutlet weak var checkinLabel: UILabel!
    @IBOutlet weak var checkoutLabel: UILabel!
    @IBOutlet weak var nightsLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var hotelListButton: UIButton!
    
    @IBOutlet weak var guestsLabel: UILabel!
    
    var collectionView: UICollectionView!
    var activityIndicator: ActivityIndicator!
    var hasLocation: Bool = false
    var selectedCity = ""
    
    lazy var mapContainerView: UIView = {
        let mView = UIView(frame: view.bounds)
        return mView
    }()
    
    @IBOutlet weak var infoView: UIView! {
        didSet {
            self.infoView.layer.cornerRadius = 5
            self.infoView.roundCorners(corners: [.topLeft, .topRight], radius: 5)
            self.infoView.addShadow()
        }
    }
    
    var mapView = GMSMapView()
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupNavigationBar()
        setupHotelListButton()
    
        locationLabel.text = "--"
        checkAvaibleLocation()
        
        viewModel.searchDTO.rooms = GuestSearchDTO(adults: 2, children: 0)
        
        DatesManager.shared.checkin = viewModel.addingDaysToDate(days: 2, currentDate: Date())
        DatesManager.shared.checkout = viewModel.addingDaysToDate(days: 4, currentDate: Date())
        DatesManager.shared.checkinHours = 15
        DatesManager.shared.checkoutHours = 11
        
        
        nightsLabel.text = "\(viewModel.getNumberOfNights()) Nights"
        //NO BORRAR
//        guestsLabel.text = "\(viewModel.searchDTO.rooms.adults + viewModel.searchDTO.rooms.children )"
        
        let checkin = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)
        let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)
        checkinLabel.text = checkin?.getMapFormatDate()
        checkoutLabel.text = checkout?.getMapFormatDate()
        
        viewModel.setDateTime(checkin: checkin!, checkout: checkout!)
        viewModel.centerCell = self.centerCell
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
                
        setupMapView()
        
        let checkinViewTap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
        checkinView.isUserInteractionEnabled = true
        checkinView.addGestureRecognizer(checkinViewTap)
        
        let checkoutViewTap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
        checkoutView.isUserInteractionEnabled = true
        checkoutView.addGestureRecognizer(checkoutViewTap)
        
        setupActivityIndicator()
        setupCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupNavigationBar()
    }
    
    func checkAvaibleLocation() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    let defaultAddress = Address(location: CLLocationCoordinate2D(latitude: 40.7127753, longitude: -74.0059728),
                       name: "240 Broadway, New York, NY 10007, EE. UU., 240 Broadway, New York, NY 10007", city: "New York", state: "New York", country: "Estados Unidos")
                    viewModel.setAddress(address: defaultAddress)
                    locationLabel.text = defaultAddress.city!
                    selectedCity = defaultAddress.city!
                    DispatchQueue.main.async {
                        self.makeHotelRequest()
                    }
            
                case .authorizedAlways, .authorizedWhenInUse:
                    locationManager.delegate = self
                    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                    locationManager.startUpdatingLocation()
                @unknown default:
                break
            }
            } else {
                print("Location services are not enabled")
        }
    }
    
    override func viewDidLayoutSubviews() {
        mapView.frame = view.bounds
        activityIndicator.backgroundView.frame = self.view.bounds
        activityIndicator.view?.frame = self.view.bounds
        activityIndicator.animationView.center = self.view.center
    }
    
    func setupMapView(){
        let camera = GMSCameraPosition.camera(withLatitude: 40.73, longitude: -73.93, zoom: 14)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.setMinZoom(12, maxZoom: 20)
        mapView.frame = view.bounds
        mapContainerView.addSubview(mapView)
        mapView.delegate = viewModel
        view.insertSubview(mapContainerView, at: 0)
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func setupActivityIndicator () {
        activityIndicator = ActivityIndicator(view: view)
    }
    
    func setupCollectionView() {
        let layout = WLCollectionViewLayout()
        layout.delegate = self
        layout.minimumLineSpacing = 10
        layout.itemSize = CGSize(width: self.view.bounds.width - 100, height: 140)
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        self.view.addSubview(collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 40)

        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -16).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        collectionView.register(UINib(nibName: "HotelsCollectionViewCellNew", bundle: nil), forCellWithReuseIdentifier: "HotelCellNew")
    }
    func pushHotelDetailViewController(_ id: String) {
        guard let checkin  = DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)?.getFormatToRequest() else { return }
        guard let checkout = DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)?.getFormatToRequest() else { return }
                
        activityIndicator.startAnimating()
        let rooms = viewModel.searchDTO.rooms ?? GuestSearchDTO(adults: 2, children: 0)
        viewModel.getHotelDetails(hotel_id: id, checkin_datetime: checkin, checkout_datetime: checkout, rooms: rooms) { [unowned self] (detail, message) in
            guard let hotelDetails = detail else {
                self.coordinator?.errorAlert(ref: self, description: "Cant get hotel information")
                self.activityIndicator.stopAnimating()
                return
            }
                    
            self.activityIndicator.stopAnimating()
            self.coordinator?.details(hotel: hotelDetails, guests: rooms )
        }
    }
    
    func makeHotelRequest() {
        if activityIndicator.isActive() { return }
        activityIndicator.startAnimating()
        viewModel.selectedIndex = 0
        viewModel.searchHotelsByLocation { (status, message) in
            if status {
                self.viewModel.createHotelMarkerList(mapView: self.mapView)
                if self.viewModel.hotelArray.count > 0 {
                    self.collectionView.reloadData()
                } else {
                    self.present(simpleAlert(title: "Warning", message: "No hotels found"), animated: true, completion: nil)
                }
            } else {
                self.present(simpleAlert(title: "Warning", message: message ?? "No hotels found"), animated: true, completion: nil)
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    func makeMoreHotelRequest() {
        activityIndicator.startAnimating()
        viewModel.moreHotelsByLocation { (status, message) in
            if status {
                self.viewModel.createHotelMarkerList(mapView: self.mapView)
                if self.viewModel.hotelArray.count > 0 {
                    self.collectionView.reloadData()
                } else {
                    self.present(simpleAlert(title: "Warning", message: "No more hotels found"), animated: true, completion: nil)
                }
            } else {
                self.present(simpleAlert(title: "Warning", message: message ?? "No more hotels found"), animated: true, completion: nil)
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    @IBAction func didTapSearchLocation(_ sender: Any) {
        let viewController = SearchLocationViewController(nibName: nil, bundle: nil)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        viewController.selectedLocationDelegate = self
        viewController.modalPresentationStyle = .overCurrentContext
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func didTapHotelListButton(_ sender: Any) {
        let date = "\( DatesManager.shared.checkin?.combineDateWithHours(hours: DatesManager.shared.checkinHours)?.getMapFormatDate() ?? "") \(DatesManager.shared.checkout?.combineDateWithHours(hours: DatesManager.shared.checkoutHours)?.getMapFormatDate() ?? "")"
            
            
        let vc = HotelListViewController()
        vc.viewModel = HotelListViewModel()
        vc.delegate = self
        vc.viewModel.headerData = HotelListDRO(city: selectedCity,
                                             nights: "\(viewModel.getNumberOfNights()) Nights",
                                             date: date)
        vc.viewModel.hotels = viewModel.hotelArray
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func didTapNightButton(_ sender: Any) {
        showCalendarViewController()
       
    }
    
    @IBAction func didTapGuests(_ sender: Any) {
        let viewController = GuestsViewController()
        viewController.delegate = self
        viewController.countAdults = viewModel.searchDTO.rooms.adults
        viewController.countChildren = viewModel.searchDTO.rooms.children
        self.present(viewController, animated: true)
    }
    
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        showCalendarViewController()
    }
    
    func showCalendarViewController() {
        
        let calendarViewController = CalendarViewController()
        calendarViewController.didSelectedDateDelegate = self
        calendarViewController.firstDate = DatesManager.shared.checkin
        calendarViewController.lastDate = DatesManager.shared.checkout
        calendarViewController.checkinHours  = DatesManager.shared.checkinHours
        calendarViewController.checkoutHours = DatesManager.shared.checkoutHours
        
        present(calendarViewController, animated: true)
    }
    
    func setupHotelListButton() {
        hotelListButton.layer.cornerRadius = 0.5 * hotelListButton.bounds.size.width
        hotelListButton.clipsToBounds = true
    }
}

extension MapViewController: SelectedLocationDelegate {
    
    func didSelectLocation(address: Address) {
        viewModel.setAddress(address: address)
        DispatchQueue.main.async {
            self.makeHotelRequest()
            self.collectionView.reloadData()
        }
    }
    
    func didSelectLocation(location: String) {
        locationLabel.text = location
        selectedCity = location
    }
}

// MARK: - UICollectionViewDataSource
extension MapViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.hotelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HotelCellNew", for: indexPath) as! HotelsCollectionViewCellNew
        let hotelRow = viewModel.hotelArray[indexPath.row]
        cell.setupRowHotel(hotel: hotelRow)
        return cell
    }
}

// MARK: - Calendar delegate
extension MapViewController: DidSelectedDateDelegate {
    
    func didSelectedDate(checkin: Date, checkout: Date, checkinHours: Int, checkoutHours: Int) {
        
        DatesManager.shared.checkin = checkin
        DatesManager.shared.checkout = checkout
        DatesManager.shared.checkinHours = checkinHours
        DatesManager.shared.checkoutHours = checkoutHours
        
        viewModel.removeAllMarkers(mapView: mapView)
        
        let checkinDate = checkin.combineDateWithHours(hours: checkinHours)
        let checkoutDate = checkout.combineDateWithHours(hours:checkoutHours)
        checkinLabel.text = checkinDate?.getMapFormatDate()
        checkoutLabel.text = checkoutDate?.getMapFormatDate()
        
        nightsLabel.text = "\(viewModel.getNumberOfNights()) Nights"
        
        viewModel.setDateTime(checkin: checkinDate!, checkout: checkoutDate!)
        
        makeHotelRequest()
    }
}

// MARK: - Location Manager delegate
extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let currentLocation = locations.last?.coordinate else { return }
        
        locationManager.stopUpdatingLocation()
        if !hasLocation {
            hasLocation = true
            self.viewModel.setCoords(location: currentLocation)
            updateLocationUI(location: currentLocation)
        }
        
        //makeHotelRequest()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        } else {
            makeHotelRequest()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func updateLocationUI (location: CLLocationCoordinate2D) {
        let geodecoder = GMSGeocoder()
        hasLocation = true
        geodecoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)) { (geocodeResponse, err) in
            guard let address = geocodeResponse?.firstResult() else { return }
            self.viewModel.setAddress(address: address)
            self.locationLabel.text = address.locality
            self.selectedCity = address.locality ?? ""
            self.makeHotelRequest()
        }
    }
}

// MARK: - UICollectionView Delegate FlowLayout
extension MapViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.pushHotelDetailViewController(self.viewModel.hotelArray[indexPath.row].id)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if viewModel.hotelArray.count - 2 == indexPath.row && viewModel.checkMakeMoreHotels() {
            viewModel.setNextPage()
            makeMoreHotelRequest()
        }
    }
}

// MARK: - Center
extension MapViewController {
    
    func centerMapLocation(lat: Float, lng: Float){
        let lat = CLLocationDegrees(lat)
        let lng = CLLocationDegrees(lng)
        let googleMapCoord: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let camera = GMSCameraPosition.camera(withTarget: googleMapCoord, zoom: viewModel.zoom)
        UIView.animate(withDuration: 0.3) {
            self.mapView.camera = camera
            self.mapView.animate(to: camera)
        }
    }
    
    func centerCell(_ hotel: HotelResponse, _ index: Int){
        collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: true)
    }
}

//  MARK: - Center Marker
extension MapViewController: DidSelectedHotel {
    
    func selectedHotel(index: Int) {
        let hotelRow = self.viewModel.hotelArray[index]
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            self.viewModel.selectMarker(self.viewModel.hotelMarkers[index])
            self.centerMapLocation(lat: hotelRow.lat, lng: hotelRow.lng)
        }
    }
}
extension MapViewController: DidSelectedGuestDelegate {
    func selectedGuests(adults: Int, children: Int) {
        viewModel.searchDTO.rooms.adults = adults
        viewModel.searchDTO.rooms.children = children
        //NO BORRAR
//        guestsLabel.text = "\(adults + children)"
        makeHotelRequest()
    }
}
extension MapViewController: SelectedHotelFromVerticalListDelegate {
    func selectedHotel(_ id: String) {
        self.pushHotelDetailViewController(id)
    }
}
