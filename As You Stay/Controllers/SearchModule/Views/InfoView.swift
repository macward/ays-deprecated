//
//  LocationView.swift
//  As You Stay
//
//  Created by Max Ward on 13/08/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class InfoView: UIView {
    
    var didTapLocationTrigger: (() -> Void)?
    var didTapCalendarTrigger: (() -> Void)?
    
    @IBOutlet weak var nightsLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cityContainerView: UIView!
    @IBOutlet weak var nightsContainerView: UIView!
    @IBOutlet weak var checkOutLabel: UILabel!
    @IBOutlet weak var checkInLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.isUserInteractionEnabled = true
        self.layer.cornerRadius = 5
        clipsToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func didTapNightsButton(_ sender: Any) {
        self.didTapCalendarTrigger?()
    }
    
    @IBAction func didTapLocationButton(_ sender: Any) {
        self.didTapLocationTrigger?()
    }
    
    func setCity(name: String) {
        self.cityLabel.text = name
    }
}
