//
//  CodeableViewController.swift
//  EMGP
//
//  Created by Max Ward on 11/05/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

class CodeableViewController: UIViewController {
    
    var bgColor: UIColor = UIColor.white
    
    func backgroundView(withColor color: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        return view
    }
    
    override func loadView() {
        super.loadView()
        view = self.backgroundView(withColor: bgColor)
    }
}

