//
//  NibLoadableProtocol.swift
//  EMGP
//
//  Created by Max Ward on 13/05/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

protocol NibLoadableProtocol {
    var nibName: String { get }
}

extension NibLoadableProtocol {
    
    public func loadFromNib() -> Self {
        let nib = UINib(nibName: nibName, bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! Self
        return view
    }
}
