//
//  Protocols.swift
//  OneWay-Architecture
//
//  Created by Max Ward on 22/03/2018.
//  Copyright © 2018 Mac Ward. All rights reserved.
//

import Foundation
import UIKit

class GenericDataSource<T> : NSObject {
  var data: DynamicValue<[T]> = DynamicValue([])
}

import RealmSwift
/// Represents a type that can be persisted using Realm.
public protocol Persistable {
  associatedtype ManagedObject: Object
  init(managedObject: ManagedObject)
  func managedObject() -> ManagedObject
}
