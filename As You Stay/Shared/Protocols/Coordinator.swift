//
//  Coordinator.swift
//  EMGP
//
//  Created by Max Ward on 10/05/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

protocol Coordinator: AnyObject {
    
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    var parentViewController: UIViewController? { get set }
    var parentCoordinator: MainCoordinator? { get set}
    
    func start()

}

extension Coordinator {
    func errorAlert(ref viewController: UIViewController) {
        let alert = simpleAlert(title: "Error", message: "Cant get hotel information")
        viewController.present(alert, animated: true)
    }
    func errorAlert(ref viewController: UIViewController, description: String) {
        let alert = simpleAlert(title: "Error", message: description)
        viewController.present(alert, animated: true)
    }
}
