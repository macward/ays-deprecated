//
//  AppProtocols.swift
//  As You Stay
//
//  Created by Max Ward on 7/18/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

protocol AuthViewModelProtocol {
    func validate(email: String) -> Bool
    func validate(password: String) -> Bool
    func signin(username: String, password: String,completion :@escaping (Bool, String?) -> Void)
    func signup(firstName: String, lastName: String, password: String, email: String, completion :@escaping (Bool, String?) -> Void)
}
protocol DidGoogleSigin {
    func didSigin(success: Bool)
}

protocol SelectedHotelFromVerticalListDelegate {
    func selectedHotel(_ id: String)
}

protocol SelectedLocationDelegate {
    func didSelectLocation(location: String) -> Void
    func didSelectLocation(address: Address)
}

protocol DidSelectedGuestDelegate {
    func selectedGuests(adults: Int, children: Int)
}

protocol completeRegistrationDelegate {
    func completedRegistretion()
}

protocol CompletionSigninDelegate {
    func completedSignin()
}

protocol BookingTotalDiscountsViewModelProtocol {
    var bookingService : BookingService { get }
    var bookingDRO : BookingDRO { get set }
    func validatePromoCodde(promoCode: String, completion: @escaping (String?, Validation?) -> Void)
}

protocol AcceptReceiptDelegate {
    func didTapAccept()
}

protocol DidSelectedDateDelegate {
    func didSelectedDate(checkin: Date, checkout: Date, checkinHours: Int, checkoutHours: Int)
}

protocol DidSelectedCreditCard {
    func didSelectedCreditCard( creditCard: CreditCardObject) -> Void
}

protocol DidAddCreditCardDelegate {
    func addedNewCard()
}

protocol UpdatedProfileDelegate {
    func updatedProfile()
}

protocol BookingTotalDiscountsProtocol{
    var viewModel : bookingTotalDiscountsViewModel { get }
    var delegate: BookingTotalDiscountsDelegate? { get }
}

protocol BookingTotalDiscountsDelegate{
    func setPromocode() -> Void
    func setLoyaltyAmmount() -> Void
}

protocol SubmitBookingDelegate {
    func didTapSubmitButton()
}
