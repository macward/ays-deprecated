//
//  UIViewLoader.swift
//  EMGP
//
//  Created by Max Ward on 13/05/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

protocol UIViewLoader {}

extension UIView : UIViewLoader {}

extension UIViewLoader where Self : UIView {
    
    static func loadNib() -> Self {
        let nibName = "\(self)".split{$0 == "."}.map(String.init).last!
        let nib = UINib(nibName: nibName, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! Self
    }
    
}
