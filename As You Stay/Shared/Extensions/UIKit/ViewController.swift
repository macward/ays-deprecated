//
//  ViewController.swift
//  EMGP
//
//  Created by Max Ward on 11/06/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func shouldDismissKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func hideBackButton() {
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    /// Add accessibility label
    func addAccessibilityLabelToTabBarItem() {
        if tabBarController != nil {
            if tabBarController!.tabBarItem != nil {
                tabBarController!.tabBarItem.accessibilityLabel = navigationItem.title
            }
        }
    }
}
