//
//  UIImage.swift
//  As You Stay
//
//  Created by Max Ward on 26/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {

    convenience init?(barcode: String) {
        let data = barcode.data(using: .ascii)
        guard let filter = CIFilter(name: "CICode128BarcodeGenerator") else { return nil }
        filter.setValue(data, forKey: "inputMessage")
        filter.setDefaults()
        //Margin
        filter.setValue(7.00, forKey: "inputQuietSpace")
        filter.setValue(data, forKey: "inputMessage")
        //Scaling
        let transform = CGAffineTransform(scaleX: 3, y: 3) 
        guard let output = filter.outputImage?.transformed(by: transform) else { return nil }
        
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(output, from: output.extent)!
        let rawImage:UIImage = UIImage.init(cgImage: cgImage)

        //Refinement code to allow conversion to NSData or share UIImage. Code here:
        //http://stackoverflow.com/questions/2240395/uiimage-created-from-cgimageref-fails-with-uiimagepngrepresentation
        let cgimage: CGImage = (rawImage.cgImage)!
        let cropZone = CGRect(x: 0, y: 0, width: Int(rawImage.size.width), height: Int(rawImage.size.height))
        let cWidth: size_t  = size_t(cropZone.size.width)
        let cHeight: size_t  = size_t(cropZone.size.height)
        let bitsPerComponent: size_t = cgimage.bitsPerComponent
        //THE OPERATIONS ORDER COULD BE FLIPPED, ALTHOUGH, IT DOESN'T AFFECT THE RESULT
        let bytesPerRow = (cgimage.bytesPerRow) / (cgimage.width  * cWidth)

        let context2: CGContext = CGContext(data: nil, width: cWidth, height: cHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: cgimage.bitmapInfo.rawValue)!

        context2.draw(cgimage, in: cropZone)

        let result: CGImage  = context2.makeImage()!

        self.init(cgImage: result)
    }
}
