//
//  ZibraLocationManager.swift
//  As You Stay
//
//  Created by Max Ward on 08/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import CoreLocation

class ZibraLocationManager: NSObject {
    
    static let shared = ZibraLocationManager()
    
    fileprivate var locationManager = CLLocationManager()
    fileprivate var currentLocation = DynamicValue<CLLocationCoordinate2D?>(nil)
    var didUpdateLocation: ((_ location: CLLocationCoordinate2D) -> Void)?
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        currentLocation.addAndNotify(observer: self) {
            guard let location = self.currentLocation.value else { return }
            self.didUpdateLocation?(location)
        }
    }
    
    func startLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func stopLocation() {
        locationManager.stopUpdatingLocation()
    }
    
}

extension ZibraLocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last?.coordinate else { return }
        
        currentLocation.value = location
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.stopLocation()
        }
    }
    
}
