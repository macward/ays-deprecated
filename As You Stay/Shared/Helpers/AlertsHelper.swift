//
//  AlertsHelper.swift
//  As You Stay
//
//  Created by Max Ward on 7/15/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

func simpleAlert(title: String, message: String) -> UIAlertController {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
    alert.addAction(action)
    return alert
}


func customAlert(title: String,
                 message: String,
                 firstTitleAction: String,
                 secondTitleAction: String,
                 firstActionHandler: @escaping (UIAlertAction) -> (),
                 secondActionHandler: @escaping (UIAlertAction) -> ()) -> UIAlertController{
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let firstAction = UIAlertAction(title: firstTitleAction, style: .default, handler: firstActionHandler)
    let secondAction = UIAlertAction(title: secondTitleAction, style: .default, handler: secondActionHandler)
    
    
    alertController.addAction(firstAction)
    alertController.addAction(secondAction)
    
    return alertController
    
}

extension UIAlertController {
    
    //Set background color of UIAlertController
    func setBackgroundColor(color: UIColor) {
        if let bgView = self.view.subviews.first, let groupView = bgView.subviews.first, let contentView = groupView.subviews.first {
            contentView.backgroundColor = color
        }
    }
    
    //Set title font and title color
    func setTitlet(font: UIFont?, color: UIColor?) {
        
        guard let title = self.title else { return }
        let attributeString = NSMutableAttributedString(string: title)//1
        if let titleFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : titleFont],//2
                                          range: NSMakeRange(0, title.utf8.count))
        }
        
        if let titleColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor],//3
                                          range: NSMakeRange(0, title.utf8.count))
        }
        self.setValue(attributeString, forKey: "attributedTitle")//4
    }
    
    //Set message font and message color
    func setMessage(font: UIFont?, color: UIColor?) {
        
        guard let message = self.message else { return }
        let attributeString = NSMutableAttributedString(string: message)
        if let messageFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : messageFont],
                                          range: NSMakeRange(0, message.utf8.count))
        }
        
        if let messageColorColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : messageColorColor],
                                          range: NSMakeRange(0, message.utf8.count))
        }
        self.setValue(attributeString, forKey: "attributedMessage")
    }
    
    //Set tint color of UIAlertController
    func setTint(color: UIColor) {
        self.view.tintColor = color
    }
}
