//
//  UIHelper.swift
//  As You Stay
//
//  Created by Max Ward on 26/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import AssistantKit

class UIHelper {
    
    static func multiplier() -> CGFloat {
        
        switch Device.screen.family.rawValue {
        case "old":
            return 0.25
        case "small":
            return 0.45
        case "medium":
            switch Device.screen.rawValue {
            case 6.5:
                return 1
            case 6.1:
                return 0.93
            case 5.8:
                return 0.80
            default:
                return 0.60
            }
        default:
            return 1
        }
    }
    
    static func multiplierFrom8() -> CGFloat {
        
        switch Device.screen.family.rawValue {
        case "old":
            return 1
        case "small":
            return 1.102
        case "medium":
            switch Device.screen.rawValue {
            case 6.5:
                return 1.102
            case 6.1:
                return 1.102
            case 5.8:
                return 1.102
            default:
                return 1
            }
        default:
            return 1
        }
    }
}
