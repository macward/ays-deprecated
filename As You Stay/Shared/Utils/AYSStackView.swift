//
//  AYSStackView.swift
//  As You Stay
//
//  Created by Max Ward on 13/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation
import UIKit

class AYSStackView : UIStackView {
    
    @IBInspectable var backgrounColor: UIColor = UIColor.clear {
        didSet {
            _bkgColor = backgrounColor
        }
    }
    
    private var _bkgColor: UIColor?
    
    override public var backgroundColor: UIColor? {
        get { return _bkgColor }
        set {
            _bkgColor = newValue
            setNeedsLayout()
        }
    }
    
    private lazy var backgroundLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        self.layer.insertSublayer(layer, at: 0)
        return layer
    }()
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        backgroundLayer.path = UIBezierPath(rect: self.bounds).cgPath
        backgroundLayer.fillColor = self.backgroundColor?.cgColor
    }
}
