//
//  DataProvider.swift
//  RouteNavigator
//
//  Created by Max Ward on 24/05/2018.
//  Copyright © 2018 RouteNavigator. All rights reserved.
//

import Realm
import RealmSwift

struct DataProvider {
    
    func isRealmAccessible() -> Bool {
        do {
            _ = try Realm()
            
        } catch {
            return false
        }
        return true
    }
    
    func configureRealm() {
        
        let config = Realm.Configuration(
            schemaVersion: 1,
            //swiftlint:disable:next unused_closure_parameter
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
            }
           ,deleteRealmIfMigrationNeeded: true
        )
        
        Realm.Configuration.defaultConfiguration = config
        _ = try! Realm()
        
    }
    
    private var realm: Realm {
        return try! Realm()
    }

}

extension DataProvider {
    
    func objects<T: Object>(_ type: T.Type, predicate: NSPredicate? = nil) -> Results<T>? {
        if !isRealmAccessible() { return nil }
        
        return predicate == nil ? realm.objects(type) : realm.objects(type).filter(predicate!)
    }
    
    func object<T: Object>(_ type: T.Type, key: String) -> T? {
        if !isRealmAccessible() { return nil }
        
        return realm.object(ofType: type, forPrimaryKey: key)
    }
    
    func object<T: Object>(_ type: T.Type, id: Int) -> T? {
        if !isRealmAccessible() { return nil }
        
        return realm.object(ofType: type, forPrimaryKey: id)
    }
    
    func add<T: Object>(_ data: [T], update: Bool = true) {
        if !isRealmAccessible() { return }
        
        if realm.isInWriteTransaction {
            realm.add(data)
        } else {
            try? realm.write {
                realm.add(data)
            }
        }
    }
    
    func add<T: Object>(_ data: T, update: Bool = true) {
        add([data], update: update)
    }
    
    func runTransaction(action: () -> Void) {
        if !isRealmAccessible() { return }

        try? realm.write {
            action()
        }
    }
    
    func delete<T: Object>(_ data: [T]) {
        try? realm.write { realm.delete(data) }
    }
    
    func delete<T: Object>(_ data: T) {
        delete([data])
    }
    
    func clearAllData() {
        if !isRealmAccessible() { return }
        try? realm.write { realm.deleteAll() }
    }
    
}
