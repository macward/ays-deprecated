//
//  ThemeProtocol.swift
//  EMGP
//
//  Created by Max Ward on 27/06/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

enum FontType {
    case Primary
}

extension FontType {
    var fontName: String {
        switch self {
        case .Primary:
            return "HelveticaNeue"
        }
    }
    
    var fontSize: CGFloat {
        switch self {
        case .Primary:
            return 16
        }
    }
}

protocol NavigationThemeComponent {
    var tintColor: UIColor { get }
    var barTintColor: UIColor { get }
    var textAttributes: [NSAttributedString.Key : Any] { get }
}

protocol TabbarThemeComponent {
    var tintColor: UIColor { get }
    var barTintColor: UIColor { get }
    var unselectedItemTintColor: UIColor { get }
    var shadowImage: UIImage { get }
    var backgroundImage: UIImage { get }
}

protocol TextFieldThemeComponent {
    var borderColor: UIColor { get }
    var borderWidth: CGFloat { get }
    var backgroundColor: UIColor { get }
    var backgroundColorDisabled: UIColor { get }
    var backgroundColorSelected: UIColor { get }
    var textColor: UIColor { get }
//    var fontType: FontType { get }
}

protocol ButtonThemeComponent {
    var borderColor: CGColor { get }
    var borderWidth: CGFloat { get }
    var backgroundColor: UIColor { get }
    var backgroundColorSelected: UIColor { get }
    var backgroundColorDisabled: UIColor { get }
    var textColor: UIColor { get }
    var textColorDisabled: UIColor { get }
//    var fontType: FontType { get }
}

protocol ColorSchemeThemeComponent {
    var primary: UIColor { get }
    var primaryInverted: UIColor { get }
    var secondary: UIColor { get }
    var primaryDetails: UIColor { get }
    var secondaryDetails: UIColor { get }
    var background: UIColor { get }
    var primaryText: UIColor { get }
    var secondaryText: UIColor { get }
    var success: UIColor { get }
    var warning: UIColor { get }
    var error: UIColor { get }
}

protocol TextColorThemeComponent {
    var textPrimary: UIColor { get }
    var textSecondary: UIColor { get }
    var textSuccess: UIColor { get }
    var textWarning: UIColor { get }
    var textInfo: UIColor { get }
    var textLight: UIColor { get }
    var textDark: UIColor { get }
    var textMuted: UIColor { get }
    var textWhite: UIColor { get }
}

protocol TableViewThemeComponent {
    
}

protocol TableViewCellThemeComponent {
    
}

protocol Theme {
    
    var color: ColorSchemeThemeComponent { get }
    var textColor: TextColorThemeComponent { get }
    var backgroundColor: UIColor { get }
    
    var navigation: NavigationThemeComponent { get }
    var tabBar: TabbarThemeComponent { get }
    var tableView: TableViewThemeComponent { get }
    var tableViewCell: TableViewCellThemeComponent { get }
    
    var primaryTextField: TextFieldThemeComponent { get }
    var primaryButton: ButtonThemeComponent { get }
    
}

extension Theme {
    
    static func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size.width, height: size.height))
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func windowSize() -> CGSize? {
        return UIApplication.shared.windows.first?.frame.size
    }
}

extension UIImage {
    static func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size.width, height: size.height))
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

