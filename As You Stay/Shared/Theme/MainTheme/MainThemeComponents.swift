//
//  MainThemeComponents.swift
//  As You Stay
//
//  Created by Max Ward on 23/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

struct ThemeTextColors: TextColorThemeComponent {
    
    var textPrimary: UIColor = UIColor.white
    
    var textSecondary: UIColor = UIColor.white
    
    var textSuccess: UIColor = UIColor.white
    
    var textWarning: UIColor = UIColor.white
    
    var textInfo: UIColor = UIColor.white
    
    var textLight: UIColor = UIColor.white
    
    var textDark: UIColor = UIColor.white
    
    var textMuted: UIColor = UIColor.white
    
    var textWhite: UIColor = UIColor.white
    
    
}

struct ThemeNavigation: NavigationThemeComponent {
    
    var tintColor: UIColor = UIColor.white
    
    var barTintColor: UIColor = UIColor(red:0.1, green:0.11, blue:0.15, alpha:1)
    
    var textAttributes: [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor:UIColor.white]
    
}

struct ThemeTabBar: TabbarThemeComponent {
    
    var tintColor: UIColor = UIColor.white
    
    var barTintColor: UIColor = UIColor(red:0.1, green:0.11, blue:0.15, alpha:1)
    
    var unselectedItemTintColor: UIColor = UIColor(red:0.36, green:0.37, blue:0.53, alpha:1)
    
    var shadowImage: UIImage = UIImage.imageWithColor(color: UIColor(red:0.1, green:0.11, blue:0.15, alpha:1), size: CGSize(width: 1, height: 1))
    
    var backgroundImage: UIImage = UIImage.imageWithColor(color: UIColor(red:0.1, green:0.11, blue:0.15, alpha:1), size: CGSize(width: 1, height: 1))
    
}

struct ThemeTextField: TextFieldThemeComponent {
    
    var borderColor: UIColor = UIColor.red
    
    var borderWidth: CGFloat = 2
    
    var backgroundColor: UIColor = UIColor.yellow
    
    var backgroundColorDisabled: UIColor = UIColor.gray
    
    var backgroundColorSelected: UIColor = UIColor.red
    
    var textColor: UIColor = UIColor.black
    
}

struct ThemePrimaryButton: ButtonThemeComponent {
    
    var borderColor: CGColor = UIColor.black.cgColor
    
    var borderWidth: CGFloat = 2.0
    
    var backgroundColor: UIColor = UIColor.red
    
    var backgroundColorSelected: UIColor = UIColor.white
    
    var backgroundColorDisabled: UIColor = UIColor.white
    
    var textColor: UIColor = UIColor.white
    
    var textColorDisabled: UIColor = UIColor.white
    
}


struct ThemeColors: ColorSchemeThemeComponent {
    
    var primary: UIColor = #colorLiteral(red: 0.5568627451, green: 0.1411764706, blue: 0.6666666667, alpha: 1)
    
    var primaryInverted: UIColor = UIColor(hexString: "297FCA")
    
    var secondary: UIColor = UIColor(hexString: "297FCA")
    
    var primaryDetails: UIColor = UIColor(hexString: "297FCA")
    
    var secondaryDetails: UIColor = UIColor(hexString: "297FCA")
    
    var background: UIColor = UIColor(hexString: "297FCA")
    
    var primaryText: UIColor = UIColor(hexString: "297FCA")
    
    var secondaryText: UIColor = UIColor(hexString: "297FCA")
    
    var success: UIColor = UIColor(hexString: "297FCA")
    
    var warning: UIColor = UIColor(hexString: "297FCA")
    
    var error: UIColor = UIColor(hexString: "297FCA")
    
}

struct ThemeTableView: TableViewThemeComponent {
    
}

struct ThemeTableViewCell: TableViewCellThemeComponent {
    
}
