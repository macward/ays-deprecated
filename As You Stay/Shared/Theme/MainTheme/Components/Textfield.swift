//
//  MainThemeTestfield.swift
//  EMGP
//
//  Created by Max Ward on 28/06/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

extension UITextField {
    
    func applyTheme(_ theme: TextFieldThemeComponent) {
        layer.borderWidth = theme.borderWidth
        layer.borderColor = theme.borderColor.cgColor
        layer.masksToBounds = true
    }
    
}

//
//class MTTextField: UITextField {
//
//    let padding = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
//
//    fileprivate let theme = ThemeManager.shared.currentTheme()
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        self.setup()
//    }
//
//    override func textRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    fileprivate func setup() {
//        guard let theme = self.theme else { return }
//        layer.borderWidth = theme.textfield.borderSize
//        layer.borderColor = theme.textfield.borderColor.cgColor
//        layer.masksToBounds = true
////        layer.cornerRadius = theme.textfield.border
//    }
//
//}
