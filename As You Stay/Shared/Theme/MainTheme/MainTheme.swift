//
//  MainTheme.swift
//  EMGP
//
//  Created by Max Ward on 27/06/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

class MainTheme: Theme {
    
    var backgroundColor: UIColor = UIColor(hexString: "20232A", alpha: 1)
    
    var color: ColorSchemeThemeComponent { return ThemeColors() }
    
    var primaryButton: ButtonThemeComponent { return ThemePrimaryButton() }
    
    var textColor: TextColorThemeComponent { return ThemeTextColors() }
    
    var navigation: NavigationThemeComponent {  return ThemeNavigation() }
    
    var tabBar: TabbarThemeComponent { return ThemeTabBar() }
    
    var tableView: TableViewThemeComponent { return ThemeTableView() }
    
    var tableViewCell: TableViewCellThemeComponent { return ThemeTableViewCell() }
    
    var primaryTextField: TextFieldThemeComponent { return ThemeTextField() }
    
}
