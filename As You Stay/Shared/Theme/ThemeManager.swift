//
//  ThemeManager.swift
//  EMGP
//
//  Created by Max Ward on 27/06/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

class ThemeManager {
    
    fileprivate var theme: Theme!
    
    static var shared = ThemeManager()
    
    func currentTheme() -> Theme {
        return self.theme
    }
    
    func applyTheme(theme: Theme) {
        
        self.theme = theme
        
        let navigation = UINavigationBar.appearance()
        navigation.barTintColor = theme.navigation.barTintColor
        navigation.tintColor = theme.navigation.tintColor
        navigation.titleTextAttributes = theme.navigation.textAttributes
        
        let tabbar = UITabBar.appearance()
        tabbar.barTintColor = theme.tabBar.barTintColor
        tabbar.tintColor = theme.tabBar.tintColor
        tabbar.unselectedItemTintColor = theme.tabBar.unselectedItemTintColor
        tabbar.backgroundImage = theme.tabBar.backgroundImage
        tabbar.shadowImage = theme.tabBar.shadowImage
        
        
    }
    
}
