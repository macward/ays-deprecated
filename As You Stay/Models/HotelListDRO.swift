//
//  HotelListDRO.swift
//  As You Stay
//
//  Created by Max Ward on 15/05/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation


struct HotelListDRO {
    var city: String
    var nights: String
    var date: String
}
