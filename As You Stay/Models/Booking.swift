//
//  Booking.swift
//  As You Stay
//
//  Created by Max Ward on 11/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import RealmSwift

struct BookingListResponse: Codable {
    
    var statusCode: Int
    var message: String
//    var data: BookingList
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message = "message"
//        case data = "data"
    }
}

struct BookingList: Codable {
    var list: [BookingElement]
    
    enum CodingKeys: String, CodingKey {
        case list = "list"
    }
}

struct BookingElement: Codable {
    
    var status: String
    var hotelConfirmationCode: String
    var hotel: BookingHotel
    var identification: Int
    var priceUsd: Float
    var guest: BookingGuest
    var note: String
    var ts: String
    var bookingId: String
    var checkinDateTime: String
    var checkoutDateTime: String
    var defaultRoomImage: AlbumElement
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case hotelConfirmationCode = "hotel_confirmation_code"
        case hotel = "hotel"
        case identification = "id"
        case priceUsd = "price_usd"
        case guest = "guest"
        case note = "note"
        case ts = "ts"
        case bookingId = "booking_id"
        case checkinDateTime = "check_in_datetime"
        case checkoutDateTime = "check_out_datetime"
        case defaultRoomImage = "default_room_image"
    }
}

struct BookingHotel: Codable {
    
    var taRating: Float
    var distance: Float
    var memberSavings: Float
    var photo: AlbumElement
    var hotelId: String
    var fullPrice: Float
    var duration: Int
    var marketPrice: Float
    var tag: String?
    var fullAddress: String
    var checkinStr: String
    var lat: Float
    var lng: Float
    var total: Float
    var checkinDesviation: Int
    var identification: String
    var totalSavingsPerc: Float
    var checkoutPrice: Float
    var roomPrice: Float
    var totalSavingsUsd: Float
    var starRating: Int
    var hotelPhone: String
    var name: String
    var discount: Float
    var checkinPrice: Float
    var savings: Float
    var checkoutStr: String
    var p: Int
    var timeString: String
    var location: String
    var checkoutDeviation: Float
    var priceLabel: String
    
    enum CodingKeys: String, CodingKey {
        
        case taRating = "ta_rating"
        case distance = "distance"
        case memberSavings = "member_savings"
        case photo = "photo"
        case hotelId = "hotel_id"
        case fullPrice = "full_price"
        case duration = "duration"
        case marketPrice = "market_price"
        case tag = "tag"
        case fullAddress = "full_addreess"
        case checkinStr = "checkin_str"
        case lat = "lat"
        case lng = "lng"
        case total = "total"
        case checkinDesviation = "checkin_desviation"
        case identification = "id"
        case totalSavingsPerc = "total_saving_perc"
        case checkoutPrice = "check_out_price"
        case roomPrice = "room_price"
        case totalSavingsUsd = "total_savings_usd"
        case starRating = "star_rating"
        case hotelPhone = "hotel_phone"
        case name = "name"
        case discount = "discount"
        case checkinPrice = "check_in_price"
        case savings = "savings"
        case checkoutStr = "checkout_str"
        case p = "p"
        case timeString = "time_string"
        case location = "location"
        case checkoutDeviation = "check_out_desviation"
        case priceLabel = "price_label"
    }
}



struct BookingGuest: Codable {
    
    var firstName: String
    var lastName: String
    var email: String
    
    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case email = "email"
    }
}

class BookingHotelObject: Object {
   
    @objc dynamic var id: Int = 0
    @objc dynamic var taRating: Float = 0
    @objc dynamic var distance: Float = 0
    @objc dynamic var memberSavings: Float = 0
    @objc dynamic var photo: AlbumElementObject? = nil
    @objc dynamic var hotelId: String = ""
    @objc dynamic var fullPrice: Float = 0
    @objc dynamic var duration: Int = 0
    @objc dynamic var marketPrice: Float = 0
    @objc dynamic var tag: String? = nil
    @objc dynamic var fullAddress: String = ""
    @objc dynamic var checkinStr: String = ""
    @objc dynamic var lat: Float = 0
    @objc dynamic var lng: Float = 0
    @objc dynamic var total: Float = 0
    @objc dynamic var checkinDesviation: Int = 0
    @objc dynamic var identification: String = ""
    @objc dynamic var totalSavingsPerc: Float = 0
    @objc dynamic var checkoutPrice: Float = 0
    @objc dynamic var roomPrice: Float = 0
    @objc dynamic var totalSavingsUsd: Float = 0
    @objc dynamic var starRating: Int = 0
    @objc dynamic var hotelPhone: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var discount: Float = 0
    @objc dynamic var checkinPrice: Float = 0
    @objc dynamic var savings: Float = 0
    @objc dynamic var checkoutStr: String = ""
    @objc dynamic var p: Int = 0
    @objc dynamic var timeString: String = ""
    @objc dynamic var location: String = ""
    @objc dynamic var checkoutDeviation: Float = 0
    @objc dynamic var priceLabel: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class BookingGuestObject: Object {
    
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var email: String = ""
}

class BookingObject: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var status: String = ""
    @objc dynamic var hotelConfirmationCode: String = ""
    @objc dynamic var hotel: BookingHotelObject? = nil
    @objc dynamic var identification: Int = 0
    @objc dynamic var priceUsd: Float = 0
    @objc dynamic var guest: BookingGuestObject? = nil
    @objc dynamic var note: String = ""
    @objc dynamic var ts: String = ""
    @objc dynamic var bookingId: String = ""
    @objc dynamic var checkinDateTime: String = ""
    @objc dynamic var checkoutDateTime: String = ""
    @objc dynamic var defaultRoomImage: AlbumElementObject? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
