//
//  ApiTexts.swift
//  As You Stay
//
//  Created by Max Ward on 22/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation

class APITexts {
    
    var shareTextSMS: String = ""
    var shareTextFacebook: String = ""
    var shareEmailSubject: String = ""
    var shareEmailBody: String = ""
    var shareFacebookTitle : String = ""
    var shareImageUrl: String = ""
    var shareUrl: String = ""
    var supportPhone : String = ""
    var referralHeader: String = ""
    
    static let sharedInstace = APITexts()
    
    class func loadKeys(_ keys: Keys){
        APITexts.sharedInstace.referralHeader = keys.referralHeader
        APITexts.sharedInstace.shareTextSMS = keys.referralSMSText
        APITexts.sharedInstace.shareTextFacebook = keys.referralFacebookText
        APITexts.sharedInstace.shareEmailBody = keys.referralEmailBody
        APITexts.sharedInstace.shareEmailSubject = keys.referralEmailSubject
        APITexts.sharedInstace.shareUrl = keys.referralShareURL
        APITexts.sharedInstace.shareFacebookTitle = keys.referralFacebookTitle
        APITexts.sharedInstace.shareImageUrl = keys.referralImage
        APITexts.sharedInstace.supportPhone = keys.supportPhone
    }
}
