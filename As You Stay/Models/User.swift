//
//  User.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import RealmSwift

struct Account {
    var totalHours: Int
    var balance: Double
}

struct User {
    var id: Int
    var firstName: String
    var lastName: String
    var email: String
    var phone: String
}

class UserObject: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var phone: String = ""
    override static func primaryKey() -> String? {
        return "id"
    }
}


struct ForgotPasswordResponse: Codable {
    
    var statusCode: Int
    var message: String
    var data: ForgotPasswordData
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message = "message"
        case data = "data"
    }
}
struct ForgotPasswordData: Codable {}
