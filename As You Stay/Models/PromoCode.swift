//
//  PromoCode.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

enum PromoType: Int {
    case promo_TYPE_NOT_A_PROMO = -1
    case promo_TYPE_REGULAR = 0
    case promo_TYPE_REFERRAL = 1
    case promo_CODE_TYPE_COMPANY = 2
    
}

enum DiscountType: Int {
    case discount_TYPE_USD = 0
    case discount_TYPE_PERCENTAGE = 1
}

struct  PromoCode {
    var code: String
    var type: PromoType
    var discountType: DiscountType
    var formattedString: String
    var discountAmount: Double
    var message: String
}
