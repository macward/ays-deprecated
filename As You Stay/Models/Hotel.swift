//
//  Hotel.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import RealmSwift

struct SearchHotelResponse: Codable {
    var status_code: Int
    var message: String
    var data: HotelData
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case status_code = "status_code"
        case message = "message"
    }
}

struct HotelData: Codable {
    var id: Int
    var total_hotels: Int
    var durationInHours: Int
    var nights: Int
    var viewPort: String?
    var center: CenterMapCoord
    
    var hotels:[HotelResponse]
    var pages: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case total_hotels = "total_hotels"
        case durationInHours = "duration_in_hours"
        case nights = "nights"
        case viewPort = "view_port"
        case center = "center"
        case pages = "pages"
        case hotels = "hotels"
     
    }
}
struct AlbumElement : Codable {
    var url: String// "https://mobileimg.priceline.com/htlimg/18150804/18150804/thumbnail-300.jpg",
    var cover: Bool//true,
    var id: Int//1,
    var description: String//"Rodeway Inn Meadowlands"

    enum CodingKeys: String, CodingKey {
        case url = "url"
        case cover = "cover"
        case id = "id"
        case description = "description"
    }

}

class AlbumElementObject: Object {

    @objc dynamic var id: Int = 0
    @objc dynamic var url: String = ""
    @objc dynamic var cover: Bool = false
    @objc dynamic var idApi: Int = 0
    @objc dynamic var descriptions: String = ""

    override static func primaryKey() -> String? {
       return "id"
    }
}



struct HotelResponse: Codable {
    
    var id: String
    var name: String
    var photo: AlbumElement
    var full_address: String
    var distance: Double
    var lat: Float
    var lng: Float
    var duration: Int
    var review_rating: Double
    var star_rating: Double
    var avg_market_price: Double
    var avg_member_savings: Double
    var member_savings_perc: Double
    var avg_night_price: Double
    
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case full_address = "full_address"
        case lat = "lat"
        case lng = "lng"
        case avg_market_price = "avg_market_price"
        case avg_member_savings = "avg_member_savings"
        case member_savings_perc = "member_savings_perc"
        case avg_night_price = "avg_night_price"
        case duration = "duration"
        case review_rating = "review_rating"
        case star_rating = "star_rating"
        case distance = "distance"
        case photo = "photo"

    }
}

class HotelObject: Object {

    @objc dynamic var id: String = ""
    @objc dynamic var availability: Bool = false
    @objc dynamic var name: String = ""
    @objc dynamic var full_address: String = ""
    @objc dynamic var lat: Float = 0
    @objc dynamic var lng: Float = 0
    @objc dynamic var avg_market_price: Double = 0
    @objc dynamic var avg_member_savings: Double = 0
    @objc dynamic var member_savings_perc: Double = 0
    @objc dynamic var avg_night_price: Double = 0
    @objc dynamic var duration: Int = 0
    @objc dynamic var review_rating: Double = 0
    @objc dynamic var star_rating: Double = 0
    @objc dynamic var distance: Double = 0
    @objc dynamic var photo: String = ""
    
    override static func primaryKey() -> String? {
       return "id"
    }
}

struct CenterMapCoord: Codable {
    var lat: Float
    var lng: Float
}
