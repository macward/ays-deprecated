//
//  GenericApiResponse.swift
//  As You Stay
//
//  Created by Max Ward on 28/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation

struct GenericApiResponse <T: Codable>: Codable {
    
    var statusCode: Int
    var message: String?
    var data: T?
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message = "message"
        case data = "data"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.statusCode = try values.decode(Int.self, forKey: .statusCode)
        if let rMessage = try? values.decode(String.self, forKey: .message) {
            self.message = rMessage
        } else {
            self.message = ""
        }
        self.data = nil
        if let rData = try? values.decode(T.self, forKey: .data) {
            self.data = rData
        }
    }
}
