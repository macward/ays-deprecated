//
//  Room.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import RealmSwift

//enum RoomType: String {
//    case individual
//    case doble
//    case triple
//    case quad
//    case queen
//    case king
//    case twin
//}
//
//struct Room {
//    var id: Int
//    var hotelId: Int
//    var album: [Photo]
//    var capacity: Int
//    var taRating: Int
//    var descriptionRoom: String
//    var price : Double
//    var roomType: String
//}
//
//class RoomObject: Object {
//     @objc dynamic var id: Int = 0
//     @objc dynamic var hotelId: Int = 0
//     @objc dynamic var album: [Photo]? = nil
//     @objc dynamic var capacity: Int = 0
//     @objc dynamic var taRating: Int = 0
//     @objc dynamic var descriptionRoom: String = ""
//     @objc dynamic var price : Double = 0.0
//     @objc dynamic var roomType: String = ""
//    
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//}
