//
//  BookingDRO.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation


struct BookingDRO {
    
    var hotelName: String = ""
    var hotelId: String  = ""
    var guestFirstName: String = ""
    var guestLastName: String = ""
    var selectedRoom: PricingV3RoomType? = nil
    var ppnBundle: String = ""
    var promoCode: String = ""
    var creditCardId: String = ""
    var nightAmount: Float  = 0
    var loyaltyCredit: Float = 0.0
    var guests: GuestSearchDTO? = nil
}
