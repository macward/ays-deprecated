//
//  Session.swift
//  As You Stay
//
//  Created by Max Ward on 7/17/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import RealmSwift

//struct Session: Codable {
//
//    var status_code: Int
//    var message: String?
//    var data: SessionData
//
//    enum CodingKeys: String, CodingKey {
//        case data = "data"
//        case status_code = "status_code"
//        case message = "message"
//    }
//}

struct SessionData: Codable {
    var token: String?
    var id: Int

    enum CodingKeys: String, CodingKey {
        case token = "token"
        case id = "id"
    }
}

enum SessionTypes : String{
    case facebook = "facebook"
    case google = "google"
    case email = "mail"
}

class SessionObject : Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var userId: String = ""
    @objc dynamic var date: Date = Date()
    @objc dynamic var token: String = ""
    @objc dynamic var sessionType: String = SessionTypes.email.rawValue
    override static func primaryKey() -> String? {
        return "id"
    }
}
