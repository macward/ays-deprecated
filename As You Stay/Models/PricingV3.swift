//
//  PricingV3.swift
//  As You Stay
//
//  Created by Max Ward on 09/03/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation

// MARK: - PricingV3Data
struct PricingV3Data: Codable {
    var nightsAmount: Float
    var duration: Float
    var address: String
    var taxesAndFeesInfo: [String]
    var roomTypes: [PricingV3RoomType]

    enum CodingKeys: String, CodingKey {
        case nightsAmount = "nights_amount"
        case duration = "duration"
        case address = "address"
        case taxesAndFeesInfo = "taxes_and_fees_info"
        case roomTypes = "room_types"
    }
}

// MARK: - PricingV3RoomType
struct PricingV3RoomType: Codable {
    var avgMarketPrice: Float
    var avgMemberSavings: Float
    var avgNightPrice: Float
    var avgEarlyCheckinLateCheckoutFee: Float
    var memberSavingsPerc: Float
    var marketPrice: Float
    var memberSavings: Float
    var earlyCheckinLateCheckoutFees: Float
    var bookingSubtotal: Float
    var savingsDeviation: Float
    var feesDeviation: Float
    var promoDiscounts: Float
    var taxesTotal: Float
    var propertyFee: Float
    var bookingTotal: Float
    var roomType: String
    var refundable: Bool
    var bookingPolicy: String
    var importantInfo: [String]
//    var propertyFeeDetails: JSONNull?
    var ppnBundle: String

    enum CodingKeys: String, CodingKey {
        case avgMarketPrice = "avg_market_price"
        case avgMemberSavings = "avg_member_savings"
        case avgNightPrice = "avg_night_price"
        case avgEarlyCheckinLateCheckoutFee = "avg_early_checkin_late_checkout_fee"
        case memberSavingsPerc = "member_savings_perc"
        case marketPrice = "market_price"
        case memberSavings = "member_savings"
        case earlyCheckinLateCheckoutFees = "early_checkin_late_checkout_fees"
        case bookingSubtotal = "booking_subtotal"
        case savingsDeviation = "savings_deviation"
        case feesDeviation = "fees_deviation"
        case promoDiscounts = "promo_discounts"
        case taxesTotal = "taxes_total"
        case propertyFee = "property_fee"
        case bookingTotal = "booking_total"
        case roomType = "room_type"
        case refundable = "refundable"
        case bookingPolicy = "booking_policy"
        case importantInfo = "important_info"
//        case propertyFeeDetails = "property_fee_details"
        case ppnBundle = "ppn_bundle"
    }
}
