//
//  BookingReceipt.swift
//  As You Stay
//
//  Created by Max Ward on 26/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

struct BookingReceiptResponse: Codable {
    var statusCode: Int
    var message: String
    var data: ReceiptObject
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message = "message"
        case data = "data"
    }
}

struct ReceiptObject: Codable {
    var hotel: HotelReceipt
    var guest: GuestReceipt
    var booking: BookingReceipt
    var pricing: PricingReceipt
    
    enum CodingKeys: String, CodingKey {
        case hotel, guest, booking, pricing
    }
}

struct HotelReceipt: Codable{
    var phone: String
    var notes: String
    var name: String
    var address: String
    var confirmationCode: String
    
    enum CodingKeys: String, CodingKey {
        case phone, notes, name, address
        case confirmationCode = "confirmation_code"
    }
    
}

struct GuestReceipt: Codable {
    var phone: String
    var name: String
    var email: String
    
    enum CodingKeys: String, CodingKey {
        case phone, name, email
    }
}

struct BookingReceipt: Codable {
    var id: String
    var checkIn: String
    var checkOut: String
    var durationHrs: Int
    var statusLabel1: String
    var statusLabel2: String
    var status: Int
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case status = "status"
        case checkIn = "check_in"
        case durationHrs = "duration_hrs"
        case statusLabel1 = "status_label_1"
        case statusLabel2 = "status_label_2"
        case checkOut = "check_out"
    }
}
struct  PricingReceipt: Codable {
   
    var roomPrice: Float
    var memberSavings: Float
    var checkInDeviation: Float
//    var checkInPrice: Float
    var checkOutDeviation: Float
//    var checkOutPrice: Float
    var tax: Float
    var credit: Float
    var promoAmount: Int
    var promoCode: String
    var hotelFees: Float
    var total: Float
    var ccLabel: String
    var ccType: String
    var ccLast4: String

    enum CodingKeys: String, CodingKey {
        
        case roomPrice = "room_price"
        case memberSavings = "member_savings"
        case checkInDeviation = "checkin_deviation"
//        case checkInPrice = "check_in_price"
        case checkOutDeviation = "checkout_deviation"
//        case checkOutPrice = "check_out_price"
        case promoAmount = "promo_amount"
        case ccLast4 = "cc_last4"
        case hotelFees = "hotel_fees"
        case promoCode = "promo_code"
        case credit = "credit"
        case total = "total"
        case tax = "tax"
        case ccType = "cc_type"
        case ccLabel = "cc_label"
    }
}

