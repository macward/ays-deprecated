//
//  HotelDetails.swift
//  As You Stay
//
//  Created by Max Ward on 01/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import RealmSwift

struct HotelDetailsResponse: Codable {
    
    var status_code: Int
    var message: String
    var data: HotelDetailsData?
    enum CodingKeys: String, CodingKey {
        case status_code = "status_code"
        case message = "message"
        case data = "data"
    }
}

struct HotelDetailsData: Codable {
    
    var details: HotelDetails
    
    enum CodingKeys: String, CodingKey {
        case details = "details"
    }
}

struct HotelDetails: Codable {
    
    var id: String
    var name: String
    var description: String
//    var room_type: String
    var album: [AlbumElement]
    var display_address: String
    var full_address: String
    var amenities: [Amenities]
    var lat: String
    var lng: String
    var policy: [String]
    var star_rating: Float
    var ppn_bundle: String?
//    var reviews: Reviews
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case full_address = "full_address"
        case display_address = "display_address"
        case lat = "lat"
        case lng = "lng"
        case description = "description"
//        case room_type = "room_type"
        case ppn_bundle = "ppn_bundle"
        case star_rating = "star_rating"
        case album = "album"
        case amenities = "amenities"
        case policy = "policy"
        //case reviews = "reviews"
    }
    
}

struct Amenities: Codable {
    
    var price_usd: Double
    var name: String
    var is_display_free: Bool
    var is_cost_extra: Bool
    var is_available: Bool
    var icon: String
    
    enum CodingKeys: String, CodingKey {
        case price_usd = "price_usd"
        case name = "name"
        case is_display_free = "is_display_free"
        case is_cost_extra = "is_cost_extra"
        case is_available = "is_available"
        case icon = "icon"
    }
}

struct Reviews: Codable {
    var reviewData: [ReviewData]
    var reviews_count: String
    var reviews_rating: Double
    
    enum CodingKeys: String, CodingKey {
        case reviewData = "reviews"
        case reviews_count = "reviews_count"
        case reviews_rating = "reviews_rating"
        
    }
}

struct ReviewData: Codable {
    var date: String // "2019-07-31",
    var positive: String // "Staff was professional and caring.",
    var user_name: String // "Anonymous",
    var negative: String //"No",
    var rating: String //"9"
    
    enum CodingKeys: String, CodingKey {
        case date = "date"
        case positive = "positive"
        case user_name = "user_name"
        case negative = "negative"
        case rating = "rating"
    }
}
