//
//  CreditCard.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation
import RealmSwift

struct CreditCard: Codable {
    
    var last4: String
    var zipcode: String
    var isPrimary: Bool
    var expYy: String
    var type: String
    var expMm: String
    var holder: String
    var identification: String
    var label: String
    
    enum CodingKeys: String, CodingKey {
        
        case last4 = "last4"
        case zipcode = "zipcode"
        case isPrimary = "is_primary"
        case expYy = "exp_yy"
        case type = "type"
        case expMm = "exp_mm"
        case holder = "holder"
        case identification = "id"
        case label = "label"
    }
}

class CreditCardObject: Object {
   
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var last4: String = ""
    @objc dynamic var zipcode: String = ""
    @objc dynamic var isPrimary: Bool = false
    @objc dynamic var expYy: String = ""
    @objc dynamic var type: String = ""
    @objc dynamic var expMm: String = ""
    @objc dynamic var holder: String = ""
    @objc dynamic var identification: String = ""
    @objc dynamic var label: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
