//
//  BookingReceiptsList.swift
//  As You Stay
//
//  Created by Max Ward on 03/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation


struct BookingReceiptUpcomingReservationsResponse: Codable {
    
    var statusCode: Int
    var message: String
    var data: ReceiptObjectList
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message = "message"
        case data = "data"
    }
}

struct ReceiptObjectList: Codable {
    var bookings: [ReceiptListItem]
    enum CodingKeys: String, CodingKey {
        case bookings = "bookings"
    }
}

struct ReceiptListItem: Codable {
    
    var hotel: HotelReceiptListItem
    var guest: GuestReceiptListItem
    var booking: BookingReceiptListItem
    var pricing: PricingReceiptListItem
}

struct HotelReceiptListItem: Codable {
    var name: String
    var address: String
    var phone: String
    var confirmationCode: String
    
    enum CodingKeys: String, CodingKey {
        case name, address, phone
        case confirmationCode = "confirmation_code"
    }
    
}

struct GuestReceiptListItem: Codable {
    var firstName: String
    var lastName: String
    var email: String
    
    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case email
    }
}

struct BookingReceiptListItem: Codable {
    
    var id: String
    var checkIn: String
    var checkOut: String
    var duration: Int
    var status: Int
    
    enum CodingKeys: String, CodingKey {
        case id, duration, status
        case checkIn = "check_in"
        case checkOut = "check_out"
    }
}

struct PricingReceiptListItem: Codable {
    
    var marketPrice: Float
    var memberSavings: Float
    var earlyCheckinLateCheckoutFees: Int
    var bookingSubtotal: Float
    var promoDiscounts: Float
    var taxesTotal: Float
    var propertyFee: Float
    var bookingTotal: Float
    
    enum CodingKeys: String, CodingKey {
        case marketPrice = "market_price"
        case memberSavings = "member_savings"
        case earlyCheckinLateCheckoutFees = "early_checkin_late_checkout_fees"
        case bookingSubtotal = "booking_subtotal"
        case promoDiscounts = "promo_discounts"
        case taxesTotal = "taxes_total"
        case propertyFee = "property_fee"
        case bookingTotal = "booking_total"
    }
}
