
//
//  CreditCardDTO.swift
//  As You Stay
//
//  Created by Max Ward on 06/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation


struct CreditCardDTO: Codable {
    var cvv: String
    var isPrimary: Bool
    var expMM: String
    var zipCode: String
    var label: String
    var last4: String
    var token: String
    var expYY: String
    var deviceData: String
    var cardHolder: String
    
    enum CodingKeys: String, CodingKey {
        case cvv, label, last4, token
        case isPrimary = "is_primary"
        case expMM = "exp_mm"
        case zipCode = "zip_code"
        case expYY = "exp_yy"
        case deviceData = "device_data"
        case cardHolder =  "card_holder"
    }
}

extension CreditCardDTO {
   func toDict() -> [String: Any] {
    
    let params: [String: Any] = [
        "cvv": self.cvv,
        "is_primary": self.isPrimary,
        "exp_mm": self.expMM,
        "zipcode": self.zipCode,
        "label": self.label,
        "last4": self.last4,
        "token": self.token,
        "exp_yy": self.expYY,
        "device_data": self.deviceData,
        "card_holder": self.cardHolder]
    return params
    }
}

struct CreditCardResponse: Codable {
    var statusCode: Int
    var message: String
    var data: CreditCardData
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message, data
    }
}
struct CreditCardData: Codable {
    
}
