//
//  Booking.swift
//  As You Stay
//
//  Created by Max Ward on 7/23/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

struct BookingDTO {
    var hotelId: String
    var checkInDate: String
    var checkOutDate: String
    var guestFirstName: String
    var guestLastName: String
    var creditCard: String
//    var guests: Int = 2
    var promoCode: String
    var redeemCredits: Double
    var deviceData: String
    var ppnBundle: String
    var rooms: GuestSearchDTO
}

struct BookingResponse: Codable{
    var statusCode: Int
    var message: String
    var data: BookingData
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message = "message"
        case data = "data"
    }
}

struct BookingData: Codable {
    var bookingId: String
    var totalHours: Int
    
    enum CodingKeys: String, CodingKey {
        case bookingId = "booking_id"
        case totalHours =  "total_hours"
    }
}

extension BookingDTO {
    func toDict() -> [String: Any] {
        let params: [String: Any] = [
            "promo_code": self.promoCode,
            "checkin_datetime": self.checkInDate,
            "checkout_datetime": self.checkOutDate,
            "hotel_id": self.hotelId,
            "rooms": [self.rooms.toDict()],
            "guest_first_name": self.guestFirstName,
            "guest_last_name": self.guestLastName,
            "credit_card": self.creditCard,
            "redeem_credits": self.redeemCredits,
            "device_data": self.deviceData,
            "ppn_bundle": self.ppnBundle]
        return params
    }
}
