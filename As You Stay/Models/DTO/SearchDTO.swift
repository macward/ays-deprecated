//
//  SearchDTO.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class SearchDTO {
    
    var city: String!
    var state: String!
    var country: String!
    var checkinDatetime: String!
    var checkoutDatetime: String!
    var guests: Int = 2
    var lat: Float!
    var lng: Float!
//    var neLat: Float!
//    var neLng: Float!
//    var swLat: Float!
//    var swLng: Float!
    var destination: String!
    var rooms: GuestSearchDTO!
    var sortBy: String!
    var page: Int = 1
    
    init() {}
}

extension SearchDTO {
    func toDict() -> [String: Any] {
        let params: [String: Any] = [
            "city": self.city ?? "",
            "state": "",
            "country": self.country ?? "",
            "checkin_datetime": self.checkinDatetime ?? "",
            "checkout_datetime": self.checkoutDatetime ?? "",
            "guests": 2,
            "lat": self.lat ?? "",
            "lng": self.lng ?? "",
            "destination": "",
            "rooms": 1,
            "sort_by": "best_desc",
            "page": self.page]
        return params
    }
    
    func toDict2() ->[String: Any] {
//        let room: [String: Any] = [
//            "adults": 2,
//            "children": 0
//        ]
        let params: [String: Any] = [
//            "ne_lat": self.neLat ?? "",
//            "ne_lng": self.neLng ?? "",
//            "sw_lat": self.swLat ?? "",
//            "sw_lng": self.swLng ?? "",
            "lat": self.lat ?? "",
            "lng": self.lng ?? "",
            "checkin_datetime": self.checkinDatetime ?? "",
            "checkout_datetime": self.checkoutDatetime ?? "",
            "destination": "",
            "rooms": [self.rooms.toDict()] ,//[room],
            "sort_by": "best_desc",
            "page": self.page
        ]
        return params
    }
}
struct GuestSearchDTO {
    var adults: Int
    var children: Int
}
extension GuestSearchDTO {
    func toDict() -> [String: Any] {
        let params: [String: Any] = [
            "adults": self.adults,
            "children": self.children
        ]
        return params
    }
}
