//
//  SignupDTO.swift
//  As You Stay
//
//  Created by Max Ward on 13/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation

struct SignupResponse: Codable {
    var status_code: Int
    var message: String?
    var data: SignupData
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case status_code = "status_code"
        case message = "message"
    }
}

struct SignupData: Codable {
    var token: String?
    var id: Int?

    enum CodingKeys: String, CodingKey {
        case token = "token"
        case id = "id"
    }
}
