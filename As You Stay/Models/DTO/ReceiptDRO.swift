//
//  ReceiptDRO.swift
//  As You Stay
//
//  Created by Max Ward on 03/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation
/**
 "hotel": {
     "name": "DoubleTree Hotel Jersey City",
     "address": "455 Washington Boulevard, Jersey City, New Jersey, United States",
     "phone": "201-499-2400",
     "confirmation_code": "CONF0"
 },
 "hotel": {
     "name": "InterContinental NEW YORK BARCLAY - Newly Renovated",
     "address": "111 East 48th Street, New York City, New York, United States",
     "notes": "Check these notes\rWow!",
     "phone": "212-755-5900",
     "confirmation_code": "CONF0"
 },
 
 
 "guest": {
     "first_name": "Chen",
     "last_name": "Demos",
     "email": "testapp@asyoustay.com"
 },
 "booking": {
     "id": "AYS-EK74FE7L",
     "check_in": "2020-01-01T15:00:00",
     "check_out": "2020-01-03T11:00:00",
     "duration": 1,
     "status": 2
 },
 "booking": {
     "id": "AYS-3L7UJ6XH",
     "check_in": "2019-12-28 15:00:00",
     "check_out": "2019-12-30 11:00:00",
     "duration_hrs": 1,
     "status_label_1": "Your room is booked!",
     "status_label_2":"Confirmation: #AYS-3L7UJ6XH",
     "status": 2
 },
 
 
 "pricing": {
     "room_price": 888.0,
     "member_savings": 38.0,
     "check_in_deviation": 0.0,
     "check_in_price": 0.0,
     "check_out_deviation": 0.0,
     "check_out_price": 0.0,
     "tax": 139.56,
     "credit": 0.0,
     "promo_amount": 0.0,
     "promo_code": "",
     "hotel_fees": 68.86,
     "total": 989.56,
     "cc_label": "Business",
     "cc_type": "MasterCard",
     "cc_last4": "4444"
 }
 "pricing": {
     "market_price": 314.0,
     "member_savings": 50.0,
     "early_checkin_late_checkout_fees": 0,
     "booking_subtotal": 264.0,
     "promo_discounts": 0.0,
     "taxes_total": 42.26,
     "property_fee": 0.0,
     "booking_total": 306.26
 }
 */
struct ReceiptDRO {
    //hotel
    var hotelName: String
    var hotelAddress: String
    var hotelPhone: String
    //guest
    var guestName: String
    var guestEmail: String
    //booking
    var bookingID: String
    var checkIn: String
    var checkOut: String
    //pricing
    var price: Float
    var promoCode: String
    var memberSavings: Float
    var total: Float
    var taxes: Float
    var hotelFees: Float
}
