//
//  Profile.swift
//  As You Stay
//
//  Created by Max Ward on 05/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//
import Foundation
import RealmSwift

struct ProfileResponse: Codable {
    
    var statusCode: Int
    var message: String
    var data: DetailProfile
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message = "message"
        case data = "data"
    }
}

struct DetailProfile: Codable {
    var detailProfileObject: DetailProfileObject
    var payments: [CreditCard]
    var keys: Keys
    
    enum CodingKeys: String, CodingKey {
        case detailProfileObject = "details"
        case payments = "payments"
        case keys = "keys"
    }
}

// MARK: - Keys
struct Keys: Codable {
    let googleAPIKey, supportPhone: String
    let bookingLeadTimeHrs, bookingMaxLeadTime: Int
    let pickerSubtext: String
    let bookingStayDurationDefault: Int
    let referralSMSText, referralEmailBody, referralEmailSubject: String
    let referralShareURL: String
    let referralFacebookText, referralFacebookTitle, referralImage, bookingNoHotelsMessageHeader: String
    let referralHeader, contactHeader: String

    enum CodingKeys: String, CodingKey {
        case googleAPIKey = "GOOGLE_API_KEY"
        case supportPhone = "SUPPORT_PHONE"
        case bookingLeadTimeHrs = "BOOKING_LEAD_TIME_HRS"
        case bookingMaxLeadTime = "BOOKING_MAX_LEAD_TIME"
        case pickerSubtext = "PICKER_SUBTEXT"
        case bookingStayDurationDefault = "BOOKING_STAY_DURATION_DEFAULT"
        case referralSMSText = "REFERRAL_SMS_TEXT"
        case referralEmailBody = "REFERRAL_EMAIL_BODY"
        case referralEmailSubject = "REFERRAL_EMAIL_SUBJECT"
        case referralShareURL = "REFERRAL_SHARE_URL"
        case referralFacebookText = "REFERRAL_FACEBOOK_TEXT"
        case referralFacebookTitle = "REFERRAL_FACEBOOK_TITLE"
        case referralImage = "REFERRAL_IMAGE"
        case bookingNoHotelsMessageHeader = "BOOKING_NO_HOTELS_MESSAGE_HEADER"
        case referralHeader = "REFERRAL_HEADER"
        case contactHeader = "CONTACT_HEADER"
    }
}
    
struct DetailProfileObject: Codable{
    
    var firstName: String
    var lastName: String
    var phone: String
    var country: String?
    var states: String?
    var city: String?
    var formattedAddress: String?
    var dateOfBirth: String?
    var accountBalance: Float
    var phoneVerified: Bool
    var referralCounter: Float
    var referralCode: String
    var cityOpeningSubscribe: Bool
    var braintreeSession: String
    var totalHrs: Int
    var email: String
    
    enum CodingKeys: String, CodingKey {
        
        case firstName = "first_name"
        case lastName = "last_name"
        case phone = "phone"
        case country = "country"
        case states = "states"
        case city = "city"
        case formattedAddress = "formatted_address"
        case dateOfBirth = "date_of_birth"
        case accountBalance = "account_balance"
        case phoneVerified = "phone_verified"
        case referralCounter = "referral_counter"
        case referralCode = "referral_code"
        case cityOpeningSubscribe = "city_opening_subscribe"
        case braintreeSession = "braintree_session"
        case totalHrs = "total_hrs"
        case email = "email"
        
    }
    
}

class ProfileObject: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var states: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var formattedAddress: String = ""
    @objc dynamic var dateOfBirth: String = ""
    @objc dynamic var accountBalance: Float = 0.0
    @objc dynamic var phoneVerified: Bool = false
    @objc dynamic var referralCounter: Float = 0.0
    @objc dynamic var referralCode: String = ""
    @objc dynamic var cityOpeningSubscribe: Bool = false
    @objc dynamic var braintreeSession: String = ""
    @objc dynamic var totalHrs: Int = 0
    @objc dynamic var email: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

struct ProfileDefaultResponse: Codable {
    var statusCode: Int
    var message: String
    var data: ProfileDefaultData

    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message, data
    }
}

struct ProfileDefaultData: Codable {
    
}


