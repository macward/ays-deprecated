//
//  ReservationsViewModel.swift
//  As You Stay
//
//  Created by Max Ward on 27/12/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class ReservationsViewModel {

//    var receipts: [ReceiptObject] = []
    var list: [ReceiptDRO] = []
    
//    func getBooking(using index: Int) -> BookingReceipt {
//        return receipts[index].booking
//    }
//
//    func getGuest(using index: Int) -> GuestReceipt {
//        return receipts[index].guest
//    }
//
//    func getHotel(using index: Int) -> HotelReceipt {
//        return receipts[index].hotel
//    }
//
//    func getPricing(using index: Int) -> PricingReceipt {
//        return receipts[index].pricing
//    }
    
    func loadOneReceipt(completion: @escaping () ->Void) {
        let bookingService = BookingService()
        
        bookingService.getReceipt(id: "AYS-FUHRF65Q") { (result) in
            switch result {
            case .success(let receipt):
//                self.receipts.append(receipt.data)
                completion()
            case .failure(let err):
                completion()
                print(err)
            }
        }
    }
    
    func getReceipts(completion: @escaping (String?) ->Void) {
        let bookingService = BookingService()
        
        bookingService.getUpcomingReservations { (result) in
            switch result {
            case .success(let reservations):
                if reservations.statusCode == 0 {
                    if let reservations = reservations.data {
                        self.list.removeAll()
                        for item in reservations.bookings {
                            let dro = ReceiptDRO(hotelName: item.hotel.name, hotelAddress: item.hotel.address, hotelPhone: item.hotel.phone, guestName: item.guest.firstName, guestEmail: item.guest.email, bookingID: item.booking.id, checkIn: item.booking.checkIn, checkOut: item.booking.checkOut, price: item.pricing.marketPrice, promoCode:
                                "", memberSavings: item.pricing.memberSavings, total: item.pricing.bookingTotal, taxes: item.pricing.taxesTotal, hotelFees: item.pricing.propertyFee)
                            self.list.append(dro)
                            self.list = self.list.reversed()
                        }
                        completion(nil)
                    }
                } else {
                    completion(reservations.message)
                }
                
                
            case .failure(let err):
                completion(err.localizedDescription)
            }
        }
        
    }
}
