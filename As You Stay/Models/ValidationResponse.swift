//
//  ValidationResponse.swift
//  As You Stay
//
//  Created by Max Ward on 03/01/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import Foundation

struct ValidationResponse: Codable {
    var statusCode: Int
    var message: String
    var data: Validation
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message = "message"
        case data = "data"
    }
}

//{
//    "status_code": 0,
//    "message": "",
//    "data": {
//        "promo": "TESTLEO",
//        "promo_type": 0,
//        "discount": 100.0,
//        "discount_type": 0,
//        "message": null,
//        "formatted": "$100.0"
//    }
//}

struct Validation: Codable {

    var code: String = ""
    var type : Int = 0
    var discountType : Int = 0
    var formattedString : String = ""
    var discountAmount: Float = 0.0
//    var message: String = ""
    
    enum CodingKeys: String, CodingKey {
        case code = "promo"
        case type  = "promo_type"
        case discountType = "discount_type"
        case formattedString = "formatted"
        case discountAmount = "discount"
//        case message = "message"
    }
}
