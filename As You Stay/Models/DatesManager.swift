//
//  DatesManager.swift
//  As You Stay
//
//  Created by Max Ward on 9/3/19.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import Foundation

class DatesManager {
    static let shared = DatesManager()
    var checkin: Date?
    var checkout: Date?
    var checkinHours = 0
    var checkoutHours = 0
    init(){}
}
