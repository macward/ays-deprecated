//
//  ProfileCoordinator.swift
//  As You Stay
//
//  Created by Max Ward on 06/05/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit


class ProfileCoordinator: Coordinator {
    
    var parentCoordinator: MainCoordinator?
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var parentViewController: UIViewController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        if SessionService.exists() {
            let viewController = ProfileViewController()
            viewController.viewModel = ProfileViewModel()
            viewController.coordinator = self
            navigationController.pushViewController(viewController, animated: true)
        } else {
            let viewController = RequestLoginViewController()
            viewController.profileCoordinator = self
            viewController.signinFrom = .profile
            navigationController.pushViewController(viewController, animated: true)
        }
    }
    
    func restart(){
        // si no estoy logueado tiene que haber un controller
        if navigationController.viewControllers.count == 1 {
            navigationController.viewControllers.remove(at: 0)
        }
        
        start()
    }
}

