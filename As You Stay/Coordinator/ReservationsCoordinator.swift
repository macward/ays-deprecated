//
//  ReservationsCoordinator.swift
//  As You Stay
//
//  Created by Max Ward on 04/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class ReservationsCoordinator: Coordinator {
    
    var parentCoordinator: MainCoordinator?
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var parentViewController: UIViewController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        if SessionService.exists() {
            let viewController = ReservationsViewController()
            viewController.oneReceipt = false
            viewController.coordinator = self
            navigationController.pushViewController(viewController, animated: true)
        } else {
            let viewController = RequestLoginViewController()
            viewController.coordinator = self
//            viewController.isReservatoin = true
            viewController.signinFrom = .reservations
            navigationController.pushViewController(viewController, animated: true)
        }
    }
    
    func restart(){
        // si no estoy logueado tiene que haber un controller
        if navigationController.viewControllers.count == 1 {
            navigationController.viewControllers.remove(at: 0)
        }
        
        start()
    }
}
