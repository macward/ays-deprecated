//
//  BookmarksCoordinator.swift
//  As You Stay
//
//  Created by Max Ward on 04/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class BookmarksCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var parentViewController: UIViewController?
    weak var parentCoordinator: MainCoordinator?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = BookmarksViewController()
        viewController.coordinator = self
        navigationController.pushViewController(viewController, animated: false)
    }
    
    func details(hotel: HotelDetails) {
        let child = HotelCoordinator(navigationController: navigationController)
        childCoordinators.append(child)
        
        let detailsHotelViewController = HotelDetailViewController()
        detailsHotelViewController.coordinator = child
        detailsHotelViewController.coordinator.parentViewController = detailsHotelViewController

        detailsHotelViewController.viewModel.hotelDetail = hotel
        detailsHotelViewController.hidesBottomBarWhenPushed = false
        navigationController.pushViewController(detailsHotelViewController, animated: true)
    }
}
