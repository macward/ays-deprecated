//
//  HotelCoordinator.swift
//  As You Stay
//
//  Created by Max Ward on 01/11/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit

class HotelCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    
    weak var parentViewController: UIViewController?
    weak var parentCoordinator: MainCoordinator?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let mapViewController = MapViewController()
        mapViewController.coordinator = self
        navigationController.pushViewController(mapViewController, animated: false)
    }
    
    func details(hotel: HotelDetails, guests: GuestSearchDTO?) {
        let detailsHotelViewController = HotelDetailViewController()
        detailsHotelViewController.coordinator = self
        detailsHotelViewController.coordinator.parentViewController = detailsHotelViewController

        detailsHotelViewController.viewModel.hotelDetail = hotel
        detailsHotelViewController.viewModel.selectedGuest = guests
        detailsHotelViewController.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(detailsHotelViewController, animated: true)
    }
    
    func booking(dro: BookingDRO){
        if SessionService.exists() {
            let bookingViewController = BookingViewController()
            bookingViewController.coordinator = self
            bookingViewController.viewModel.bookingDRO = dro
            var views = navigationController.viewControllers
            if views.count == 4 {
                navigationController.popViewController(animated: false)
            }
            navigationController.pushViewController(bookingViewController, animated: true)
        } else {
            let requestViewController = RequestLoginViewController()
            requestViewController.hotelCoordinator = self
//            requestViewController.isReservatoin = false
            requestViewController.signinFrom = .rooms
            requestViewController.viewModel.bookingDRO = dro
            navigationController.pushViewController(requestViewController, animated: true)
        }
    }
    
    func preview() {
        let previewCoordinator = AmenitiesViewController()
        previewCoordinator.coordinator = self
        navigationController.pushViewController(previewCoordinator, animated: true)
    }
    
    func preview(ref viewController: UIViewController) {
        let previewCoordinator = AmenitiesViewController()
        previewCoordinator.coordinator = self
        parentViewController?.present(previewCoordinator, animated: true, completion: nil)
    }
    
    func moveToInitial() {
        navigationController.popToRootViewController(animated: false)
    }
    
    func creditCards(ref viewController: BookingViewController, cardList: [CreditCardObject]) {
        let creditCardsViewController = CreditCardsViewController()
        let nav = UINavigationController(rootViewController: creditCardsViewController)
        //creditCardsViewController.cardList = cardList
        creditCardsViewController.coordinator = self
        creditCardsViewController.delegate = viewController
        viewController.present(nav, animated: true, completion: nil)
    }

    func roomsV3(data: PricingV3Data?, hotel: HotelDetails?, guests: GuestSearchDTO?) {
        
        let roomsTypeViewController = RoomsViewController()
        roomsTypeViewController.roomTypes = data?.roomTypes ?? []
        if let profile = ProfileLocalService.getProfile() {
            roomsTypeViewController.dro.guestLastName = profile.lastName
            roomsTypeViewController.dro.guestFirstName = profile.firstName
        } else {
            roomsTypeViewController.dro.guestLastName = ""
            roomsTypeViewController.dro.guestFirstName = "without user  "
        }
        roomsTypeViewController.dro.hotelName = hotel?.name ?? "Hotel"
//        roomsTypeViewController.dro.ppnBundle = hotel?.ppn_bundle ?? ""
        roomsTypeViewController.dro.hotelId = hotel?.id ?? ""
        roomsTypeViewController.dro.nightAmount = (data?.nightsAmount ?? 0) / (data?.duration ?? 1)
        roomsTypeViewController.important = data?.roomTypes.first?.importantInfo ?? []
        roomsTypeViewController.coordinator = self
        roomsTypeViewController.viewModel.selectedGuests = guests 
        navigationController.pushViewController(roomsTypeViewController, animated: true)
    }
    
    func receipt(_ receipt: ReceiptObject) {
        let viewModel = ReceiptViewModel()
        viewModel.receipt = receipt
        let viewController = ReceiptViewController()
        viewController.viewModel = viewModel
        viewController.coordinator = self
        navigationController.pushViewController(viewController, animated: true)
    }
}

