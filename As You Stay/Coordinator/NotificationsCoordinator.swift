//
//  NotificationsCoordinator.swift
//  As You Stay
//
//  Created by Max Ward on 14/03/2020.
//  Copyright © 2020 zibracoders. All rights reserved.
//

import UIKit

class NotificationsCoordinator: Coordinator {
    
    var parentCoordinator: MainCoordinator?
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var parentViewController: UIViewController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = NotificationsViewController()
        viewController.coordinator = self
        navigationController.pushViewController(viewController, animated: true)
    }
}
