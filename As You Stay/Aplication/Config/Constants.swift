//
//  ApiConstants.swift
//  EMGP
//
//  Created by Max Ward on 14/05/2019.
//  Copyright © 2019 Mac Ward. All rights reserved.
//

import UIKit

struct K {
    
    struct PROVIDERS {
        struct GOOGLE {
            static let API_KEY = "AIzaSyCpZkBbrtBZtAJ6POjju7ipmN1RudNn-iQ"
            static let CLIENT_ID = "450146047999-tfu44scct33c87knj5e51f6l1d2n98ia.apps.googleusercontent.com"
        }
    }
    
    struct Target {
        #if DEVELOPMENT
        static let baseURL = "http://34.205.19.215/"
        #else
        static let baseURL = "https://asyoustay.com/"
        #endif
    }
    
    struct Server {
        static let baseURL = "\(Target.baseURL)services/client"
    }
    
    struct NotificationKeys {
        static let refreshMainTabBar = "ays.signin.refreshMainTabBar"
    }
    
    struct API {
        
        struct Authentication {
            static let login = "\(Target.baseURL)services/client/account/login/"
            static let tokenValidation = "\(Target.baseURL)services/client/account/validate/"
            static let signup = "\(Target.baseURL)services/client/account/signup/"
            static let restorePassword = "\(Target.baseURL)services/client/account/forgetPassword"
            static let googleTokenVerification = "\(Target.baseURL)services/client/account/google/connect/"
            
        }
        
        struct Profile {
            static let me = "\(Target.baseURL)services/client/account/getProfile/"
            static let update = "\(Target.baseURL)services/client/account/update/"
            static let resetPassword = "\(Target.baseURL)services/client/account/signup/"
        }
        
        struct Hotels {
            static let search = "\(Target.baseURL)services/client/search/locationV2/"
            static let details = "\(Target.baseURL)services/client/search/getHotelDetailsV2/"
            static let inRange = "\(Target.baseURL)services/client/search/getHotelsInRangeV2/"
        }
        
        struct Destinations {
            static let list = "\(Target.baseURL)services/client/search/getDestinations/"
            static let details = "\(Target.baseURL)services/client/search/getDestinationDetails/"
        }
        
        struct Booking {
            static let estimate = "\(Target.baseURL)services/client/bookings/estimate/"
            static let pricing = "\(Target.baseURL)services/client/bookings/getPricingV2/"
            static let submit = "\(Target.baseURL)services/client/bookings/submitBookingV2/"
            static let receipt = "\(Target.baseURL)services/client/bookings/getReceipt"
            static let upcomingReservations = "\(Target.baseURL)services/client/bookings/getUpcomingReservations"
            static let list = "\(Target.baseURL)services/client/bookings/getList"
        }
    }
}

enum ApiRequestError: Error {
    case CannotFetch(String)
    case CannotCreate(String)
    case CannotUpdate(String)
    case CannotDelete(String)
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}

func getApplicationVersion() -> String! {
    let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    return version
}

func getApplicationBuild() -> String! {
    let version = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
    return version
}

func getDeviceUdid() -> String! {
    return UIDevice.current.identifierForVendor!.uuidString
}
