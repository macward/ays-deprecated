//
//  AppDelegate.swift
//  As You Stay
//
//  Created by Max Ward on 15/07/2019.
//  Copyright © 2019 zibracoders. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleMaps
import GooglePlaces
import Firebase
import Fabric
import Crashlytics
import OneSignal
import Intercom
import AdSupport

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var coordinator: MainCoordinator?
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //START OneSignal initialization code
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]

        OneSignal.initWithLaunchOptions(launchOptions,
        appId: "",
        handleNotificationAction: nil,
        settings: onesignalInitSettings)

        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;

        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
        print("User accepted notifications: \(accepted)")
        })
        //END OneSignal initializataion code
        
        
        GIDSignIn.sharedInstance().clientID = K.PROVIDERS.GOOGLE.CLIENT_ID
        GMSServices.provideAPIKey(K.PROVIDERS.GOOGLE.API_KEY)
        GMSPlacesClient.provideAPIKey(K.PROVIDERS.GOOGLE.API_KEY)
        
        ThemeManager.shared.applyTheme(theme: MainTheme())
        
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        
        let _ = DataProvider().configureRealm()
        
        print("Document directory \(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0])")
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = MainTabBarViewController()
        window?.makeKeyAndVisible()
        
        Intercom.setApiKey("fszdj8jw", forAppId: "ios_sdk-1a7905ce1a149e7ddf846026cc505c772718b7ad")
        
        if SessionService.exists() {
            if let profile = ProfileLocalService.getProfile() {
            Intercom.enableLogging()
                Intercom.registerUser(withEmail: profile.email)
                let userAttributes = ICMUserAttributes()
                userAttributes.name = "\(profile.firstName) \(profile.lastName)"
                userAttributes.email = profile.email
                userAttributes.userId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
                Intercom.updateUser(userAttributes)
            }
        }
        
        return true
    }
    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        let appId: String = Settings.appID
//        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" {
//            return ApplicationDelegate.shared.application(app, open: url, options: options)
//        }
//        if url.scheme != nil && url.scheme!.hasPrefix("com.googleusercontent.apps.\(appId)") && url.host ==  "authorize" {
//            return GIDSignIn.sharedInstance().handle(url as URL?,
//                                                             sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
//                                                             annotation: options[UIApplication.OpenURLOptionsKey.annotation])
//        } else {
//            return false
//        }
        
//    }
    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    
}
